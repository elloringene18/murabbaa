<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('title_ar')->nullable();
            $table->string('slug')->unique()->index();
            $table->string('thumbnail')->nullable();
            $table->string('photo')->nullable();
            $table->longText('content')->nullable();
            $table->longText('content_ar')->nullable();
            $table->date('date')->nullable();
            $table->string('location')->nullable();
            $table->bigInteger('media_type_id')->unsigned()->index();
            $table->foreign('media_type_id')->references('id')->on('media_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
