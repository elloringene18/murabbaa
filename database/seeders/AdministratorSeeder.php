<?php

namespace Database\Seeders;

use App\Models\Administrator;
use Faker\Generator;
use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::create([
            'name' => 'Hind Almarzooqi',
            'name_ar' => 'هند المرزوقي ',
            'position' => '',
            'position_ar' => '',
        ]);

        Administrator::create([
            'name' => 'Roqaia Radwan',
            'name_ar' => 'رقية رضوان
',
            'position' => '',
            'position_ar' => '',
        ]);

        Administrator::create([
            'name' => 'Mohammed Alblooshi ',
            'name_ar' => 'محمد البلوشي
',
            'position' => 'Technical',
            'position_ar' => 'Technical',
        ]);


//        for($x=0;$x<10;$x++)
//        {
//            $name = $this->faker->name;
//            $title = $this->faker->jobTitle;
//
//            Administrator::create([
//                'name' => $name,
//                'name_ar' => $name.' AR',
//                'position' => $title,
//                'position_ar' => $title.' AR',
//            ]);
//        }
    }
}
