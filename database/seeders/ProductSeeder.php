<?php

namespace Database\Seeders;

use App\Models\Artist;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Models\ProductArtist;

class ProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker)
    {
        $this->model = $model;
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'year' => '2020',
                'title' => 'Domino 1',
                'title_ar' => 'Domino 1',
                'photo' => 'uploads/products/amar-1.jpg',
                'photo_full' => 'uploads/products/amar-1-full.jpg',
                'description' => '<p>Documentary photography of Men Playing Domino in Diba while they are having their afternoon tea. They gather everyday after Asr prayer to play domino as they are friends and have tea.</p>',
                'description_ar' => '<p>Documentary photography of Men Playing Domino in Diba while they are having their afternoon tea. They gather everyday after Asr prayer to play domino as they are friends and have tea.</p>',
                'category_id' => 2,
                'for_sale' => 1,
            ]
        ];

        $this->storeToArists($data,1);

        $data = [
            [
                'year' => '2020',
                'title' => 'Budour Art 1',
                'title_ar' => 'Budour Art 1',
                'photo' => 'uploads/products/bodour-1.jpg',
                'photo_full' => 'uploads/products/bodour-1-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 2',
                'title_ar' => 'Budour Art 2',
                'photo' => 'uploads/products/bodour-2.jpg',
                'photo_full' => 'uploads/products/bodour-2-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 3',
                'title_ar' => 'Budour Art 3',
                'photo' => 'uploads/products/bodour-3.jpg',
                'photo_full' => 'uploads/products/bodour-3-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 4',
                'title_ar' => 'Budour Art 4',
                'photo' => 'uploads/products/bodour-4.jpg',
                'photo_full' => 'uploads/products/bodour-4-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 5',
                'title_ar' => 'Budour Art 5',
                'photo' => 'uploads/products/bodour-5.jpg',
                'photo_full' => 'uploads/products/bodour-5-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 6',
                'title_ar' => 'Budour Art 6',
                'photo' => 'uploads/products/bodour-6.jpg',
                'photo_full' => 'uploads/products/bodour-6-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 7',
                'title_ar' => 'Budour Art 7',
                'photo' => 'uploads/products/bodour-7.jpg',
                'photo_full' => 'uploads/products/bodour-7-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 8',
                'title_ar' => 'Budour Art 8',
                'photo' => 'uploads/products/bodour-8.jpg',
                'photo_full' => 'uploads/products/bodour-8-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Budour Art 9',
                'title_ar' => 'Budour Art 9',
                'photo' => 'uploads/products/bodour-9.jpg',
                'photo_full' => 'uploads/products/bodour-9-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
        ];

        $this->storeToArists($data,2);
        $data = [
            [
                'year' => '2020',
                'title' => 'The Platform',
                'title_ar' => 'The Platform',
                'description' => '<p>The title and design is inspired by the Spanish movie The Platform and phrase use is : 
Perfection is unattainable, but if we seek perfection we can catch excellence</p>',
                'description_ar' => '<p>The title and design is inspired by the Spanish movie The Platform and phrase use is : 
Perfection is unattainable, but if we seek perfection we can catch excellence</p>',
                'photo' => 'uploads/products/diaa-1.jpg',
                'photo_full' => 'uploads/products/diaa-1-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'The Change',
                'title_ar' => 'The Change',
                'description' => '<p>Quote used : Change never happens spontaneously</p>',
                'description_ar' => '<p>Quote used : Change never happens spontaneously</p>',
                'photo' => 'uploads/products/diaa-2.jpg',
                'photo_full' => 'uploads/products/diaa-2-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'The Autumn of Love',
                'title_ar' => 'The Autumn of Love',
                'description' => '<p>The Falling letters of love or the autumn of love using the letters of this verse of a poem for Al Abbas bin Al Ahnaf : ان اﻟﻣﺣﺑﯾن ﻗوم ﺑﯾن أﻋﯾﻧﮭم وﺳم ﻣن اﻟﺣب ﻻ ﯾﺧﻔﻰ ﻋﻠﻰ أﺣد</p>',
                'description_ar' => '<p>The Falling letters of love or the autumn of love using the letters of this verse of a poem for Al Abbas bin Al Ahnaf : ان اﻟﻣﺣﺑﯾن ﻗوم ﺑﯾن أﻋﯾﻧﮭم وﺳم ﻣن اﻟﺣب ﻻ ﯾﺧﻔﻰ ﻋﻠﻰ أﺣد</p>',
                'photo' => 'uploads/products/diaa-3.jpg',
                'photo_full' => 'uploads/products/diaa-3-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Kindness is the key',
                'title_ar' => 'Kindness is the key',
                'description' => '<p>A 3D illusion artwork highlighting the importance of Kindness in our lives using the verse of a poem : ﻧﺑﻠﻎ ﻓﻲ اﻟﻘﻠوب ﻣﻘﺎﻣﺎ اﻟﺣﯾﺎة ﻓﺈﻧﻣﺎ ﺑﺎﻟﻠطف اﺻﻧﻊ ﺟﻣﯾﻼ ﻓﻲ</p>',
                'description_ar' => '<p>A 3D illusion artwork highlighting the importance of Kindness in our lives using the verse of a poem : ﻧﺑﻠﻎ ﻓﻲ اﻟﻘﻠوب ﻣﻘﺎﻣﺎ اﻟﺣﯾﺎة ﻓﺈﻧﻣﺎ ﺑﺎﻟﻠطف اﺻﻧﻊ ﺟﻣﯾﻼ ﻓﻲ</p>',
                'photo' => 'uploads/products/diaa-4.jpg',
                'photo_full' => 'uploads/products/diaa-4-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Conversation of the eyes',
                'title_ar' => 'Conversation of the eyes',
                'description' => '<p>Using the verse: وﻧﺷﻛو ﺑﺎﻟﻌﯾون إذا اﻟﺗﻘﯾﻧﺎ ﻓﺄﻓﮭﻣﮫ وﯾﻌﻠم ﻣﺎ أردت أﻗول ﺑﻣﻘﻠﺗﻲ أﻧﻲ ﻣت ﺷوﻗﺎ ﻓﯾوﺣﻲ طرﻓﮫ أﻧﻲ ﻗد ﻋﻠﻣت</p>',
                'description_ar' => '<p>Using the verse: وﻧﺷﻛو ﺑﺎﻟﻌﯾون إذا اﻟﺗﻘﯾﻧﺎ ﻓﺄﻓﮭﻣﮫ وﯾﻌﻠم ﻣﺎ أردت أﻗول ﺑﻣﻘﻠﺗﻲ أﻧﻲ ﻣت ﺷوﻗﺎ ﻓﯾوﺣﻲ طرﻓﮫ أﻧﻲ ﻗد ﻋﻠﻣت</p>',
                'photo' => 'uploads/products/diaa-5.jpg',
                'photo_full' => 'uploads/products/diaa-5-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => '“And you are of a great moral character”',
                'title_ar' => '“And you are of a great moral character”',
                'description' => '<p>A verse in Quran describing our prophet Muhammad PPUH  ﺧﻠﻖ ﻋظﯾم وإﻧك ﻟﻌﻠﻰ</p>',
                'description_ar' => '<p>A verse in Quran describing our prophet Muhammad PPUH  ﺧﻠﻖ ﻋظﯾم وإﻧك ﻟﻌﻠﻰ</p>',
                'photo' => 'uploads/products/diaa-6.jpg',
                'photo_full' => 'uploads/products/diaa-6-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Pride In Battle',
                'title_ar' => 'Pride In Battle',
                'description' => '<p>Featuring 3 of the top players at Man City FC using the official motto of the football club : Pride in Battle as the letters shaping the faces of the players </p>',
                'description_ar' => '<p>Featuring 3 of the top players at Man City FC using the official motto of the football club : Pride in Battle as the letters shaping the faces of the players </p>',
                'photo' => 'uploads/products/diaa-7.jpg',
                'photo_full' => 'uploads/products/diaa-7-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
        ];

        $this->storeToArists($data,3);
        $data = [
            [
                'year' => '2020',
                'title' => 'Nadia Art 1',
                'title_ar' => 'Nadia Art 1',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-1.jpg',
                'photo_full' => 'uploads/products/nadia-1-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 2',
                'title_ar' => 'Nadia Art 2',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-2.jpg',
                'photo_full' => 'uploads/products/nadia-2-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 3',
                'title_ar' => 'Nadia Art 3',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-3.jpg',
                'photo_full' => 'uploads/products/nadia-3-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 4',
                'title_ar' => 'Nadia Art 4',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-4.jpg',
                'photo_full' => 'uploads/products/nadia-4-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 5',
                'title_ar' => 'Nadia Art 5',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-5.jpg',
                'photo_full' => 'uploads/products/nadia-5-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 6',
                'title_ar' => 'Nadia Art 6',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-6.jpg',
                'photo_full' => 'uploads/products/nadia-6-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Nadia Art 7',
                'title_ar' => 'Nadia Art 7',
                'description' => '<p></p>',
                'description_ar' => '<p></p>',
                'photo' => 'uploads/products/nadia-7.jpg',
                'photo_full' => 'uploads/products/nadia-7-full.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
        ];

        $this->storeToArists($data,4);

        $data = [
            [
                'year' => '2020',
                'title' => 'Where is your head at',
                'title_ar' => 'Where is your head at',
                'description' => '<p>This piece was inspired by the pandemic’s impact on our mental health. As individuals our anxiety has been umcomfortable to say the least, but it has helped us come together and bond on a global scale.</p>',
                'description_ar' => '<p>This piece was inspired by the pandemic’s impact on our mental health. As individuals our anxiety has been umcomfortable to say the least, but it has helped us come together and bond on a global scale.</p>',
                'photo' => 'uploads/products/anna-1-260.jpg',
                'photo_full' => 'uploads/products/anna-1.jpg',
                'category_id' => 4,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Contained',
                'title_ar' => 'Contained',
                'description' => '<p>Without any expectation of the turn-out, the idea of this piece was to go with the ﬂow and let the lines form organically, only to realise, the lines started closing in on themselves far from the edge of the board, hence the title “Contained” a reﬂection of the state of mind in the midst of the lockdown.</p>',
                'description_ar' => '<p>Without any expectation of the turn-out, the idea of this piece was to go with the ﬂow and let the lines form organically, only to realise, the lines started closing in on themselves far from the edge of the board, hence the title “Contained” a reﬂection of the state of mind in the midst of the lockdown.</p>',
                'photo' => 'uploads/products/anna-2-260.jpg',
                'photo_full' => 'uploads/products/anna-2.jpg',
                'category_id' => 4,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'My Gentle Creature',
                'title_ar' => 'My Gentle Creature',
                'description' => '<p>This is one of my free ﬂow illustrations. Like many of my organic line combinations, the illus-tration comes to life as if creeping in but slowly, without anyone expecting it, and that’s why I name this one The Gentle Creature.</p>',
                'description_ar' => '<p>This is one of my free ﬂow illustrations. Like many of my organic line combinations, the illus-tration comes to life as if creeping in but slowly, without anyone expecting it, and that’s why I name this one The Gentle Creature.</p>',
                'photo' => 'uploads/products/anna-3-260.jpg',
                'photo_full' => 'uploads/products/anna-3.jpg',
                'category_id' => 13,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Urban in Colors',
                'title_ar' => 'Urban in Colors',
                'description' => '<p>Urban in Color were Illustrated digitally as part of an abstract city scape series, and unlike showing iconic architectural shapes i used colors represemting different times to bring them to life.</p>',
                'description_ar' => '<p>Urban in Color were Illustrated digitally as part of an abstract city scape series, and unlike showing iconic architectural shapes i used colors represemting different times to bring them to life.</p>',
                'photo' => 'uploads/products/anna-4-260.jpg',
                'photo_full' => 'uploads/products/anna-4.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => '43 Hugs',
                'title_ar' => '43 Hugs',
                'description' => '<p>A free ﬂow illustration using acrylic, to represent “hugs” given to someone, like an abundance of love that surrounds one person.</p>',
                'description_ar' => '<p>A free ﬂow illustration using acrylic, to represent “hugs” given to someone, like an abundance of love that surrounds one person.</p>',
                'photo' => 'uploads/products/anna-5-260.jpg',
                'photo_full' => 'uploads/products/anna-5.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Sisters with heavy hearts',
                'title_ar' => 'Sisters with heavy hearts',
                'description' => '<p>The Sisters represents ï¬rst an foremost the bond between women, a dedication to any woman that familiarizse with another, in hardships and challenges. Women, mothers, sisters, daughters, friends, are able to carry the world on their shoulders, and heavy hearts as a result. </p>',
                'description_ar' => '<p>The Sisters represents ï¬rst an foremost the bond between women, a dedication to any woman that familiarizse with another, in hardships and challenges. Women, mothers, sisters, daughters, friends, are able to carry the world on their shoulders, and heavy hearts as a result. </p>',
                'photo' => 'uploads/products/anna-6-260.jpg',
                'photo_full' => 'uploads/products/anna-6.jpg',
                'category_id' => 2,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Lebanon Tomorrow',
                'title_ar' => 'Lebanon Tomorrow',
                'description' => '<p>The print is made to order, and forever contributes to the Lebanese Red Cross.</p>',
                'description_ar' => '<p>The print is made to order, and forever contributes to the Lebanese Red Cross.</p>',
                'photo' => 'uploads/products/anna-7-260.jpg',
                'photo_full' => 'uploads/products/anna-7.jpg',
                'category_id' => 13,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'Changes',
                'title_ar' => 'Changes',
                'description' => '<p>This piece represents our way of life, a wheel turning consistently, suddenly disrupted by the pandemic making its way through all the cracks like the force of a wave.</p>',
                'description_ar' => '<p>This piece represents our way of life, a wheel turning consistently, suddenly disrupted by the pandemic making its way through all the cracks like the force of a wave.</p>',
                'photo' => 'uploads/products/anna-8-260.jpg',
                'photo_full' => 'uploads/products/anna-8.jpg',
                'category_id' => 4,
                'for_sale' => 1,
            ],
            [
                'year' => '2020',
                'title' => 'The Sea & Me Mandala',
                'title_ar' => 'The Sea & Me Mandala',
                'description' => '<p>The Sea & Me Mandala was inspired by my love for the Ocean as it is a big part of my family’s life, all together it documents some of my favorite things related to the sea. The Mandala is available as home accessories and as a beach blanket.</p>',
                'description_ar' => '<p>The Sea & Me Mandala was inspired by my love for the Ocean as it is a big part of my family’s life, all together it documents some of my favorite things related to the sea. The Mandala is available as home accessories and as a beach blanket.</p>',
                'photo' => 'uploads/products/anna-9-260.jpg',
                'photo_full' => 'uploads/products/anna-9.jpg',
                'category_id' => 11,
                'for_sale' => 1,
            ],
        ];

        $this->storeToArists($data,5);

//        $data = [];
//        for($x=0;$x<300;$x++){
//            $data[] = ['title' => $this->faker->text(rand(10,30))];
//        }
//
//        foreach ($data as $item)
//        {
//            $product = [];
//            $product['year'] = rand(1900,2020);
//            $product['title'] = $item['title'];
//            $product['title_ar'] = $item['title'].' AR';
//            $product['slug'] = $this->generateSlug(\Illuminate\Support\Str::slug($item['title']));
//            $product['description'] = $this->faker->paragraph();
//            $product['description_ar'] = $this->faker->paragraph() .' AR';
//            $product['photo'] = 'uploads/products/placeholder-260-'.rand(1,3).'.png';
//            $product['category_id'] = Category::whereDoesntHave('children')->inRandomOrder()->first()->id;
//            $product['for_sale'] = rand(1,10) > 3 ? 1 : 0;
//
//            $newproduct = Product::create($product);
//
//            if($newproduct){
//
//                if($newproduct->category->parent)
//                    $attributes = $newproduct->category->parent->attributes;
//                else
//                    $attributes = $newproduct->category->attributes;
//
//                foreach ($attributes as $attribute){
//                    $value = $attribute->values()->inRandomOrder()->first();
//                    ProductAttribute::create(['attribute_value_id'=>$value->id,'product_id'=>$newproduct->id]);
//                }
//
//                // Assign Product to Artist
//                $artist = Artist::inRandomOrder()->first();
//                ProductArtist::create(['product_id'=>$newproduct->id,'artist_id'=>$artist->id]);
//
//            }
//        }
    }

    function storeToArists($data,$artist_id){
        foreach ($data as $item)
        {
            $item['slug'] = $this->generateSlug(\Illuminate\Support\Str::slug($item['title']));

            $newproduct = Product::create($item);

            if($newproduct){

                if($newproduct->category->parent)
                    $attributes = $newproduct->category->parent->attributes;
                else
                    $attributes = $newproduct->category->attributes;

                foreach ($attributes as $attribute){
                    $value = $attribute->values()->inRandomOrder()->first();
                    ProductAttribute::create(['attribute_value_id'=>$value->id,'product_id'=>$newproduct->id]);
                }
                ProductArtist::create(['product_id'=>$newproduct->id,'artist_id'=>$artist_id]);

            }
        }
    }
}
