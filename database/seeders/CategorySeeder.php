<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryParent;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategorySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'name' => 'Painting',
                'name_ar' => 'الرسم',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Landscape',
                        'name_ar' => 'المناظر الطبيعية',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Portraiture ',
                        'name_ar' => 'رسم البورتريه',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Abstract',
                        'name_ar' => 'الرسم التجريدي',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Surrealism ',
                        'name_ar' => 'الرسم السريالي',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Realism',
                        'name_ar' => 'الرسم الواقعي',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Still Life',
                        'name_ar' => 'رسم الطبيعة الصامتة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Mixed Media',
                        'name_ar' => 'الوسائط المختلطة',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [
                'name' => 'Illustration' ,
                'name_ar' => 'الرسم التوضيحي',
                'attributes' => [
                    [ 'name' => 'Subject' ],
                    [ 'name' => 'Style' ],
                ],
                'children' => [
                    [
                        'name' => 'Conceptual',
                        'name_ar' => 'المفاهيمي',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Fashion',
                        'name_ar' => 'الموضة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Comics and Graphic Novels',
                        'name_ar' => 'القصص والروايات المصورة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Digital',
                        'name_ar' => 'الرقمي',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Editorial',
                        'name_ar' => 'الرسم التحريري',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [
                'name' => 'Sculpture' ,
                'name_ar' => 'النحت' ,
                'attributes' => [
                    [ 'name' => 'Material' ],
                    [ 'name' => 'Subject' ],
                    [ 'name' => 'Size' ],
                ]
            ],
            [ 'name' => 'Printmaking' ,
                'name_ar' => 'الطباعة الفنية',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Relief',
                        'name_ar' => 'البارزة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Intaglio',
                        'name_ar' => 'الغائرة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Lithography',
                        'name_ar' => 'المستوية',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Screen Printing',
                        'name_ar' => 'الحريرية',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Children Literature' ,
                'name_ar' => 'أدب الطفل ',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Authors',
                        'name_ar' => 'المؤلفون',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Illustrators',
                        'name_ar' => 'الرسامون',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Photography' ,
                'name_ar' => 'التصوير الفوتوغرافي',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Landscape ',
                        'name_ar' => 'المناظر الطبيعية ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Photojournalism ',
                        'name_ar' => 'الصحافة المصورة ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Portraiture ',
                        'name_ar' => 'تصوير البورتريه ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Fashion ',
                        'name_ar' => 'الموضة ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Still Life ',
                        'name_ar' => 'الطبيعة الصامتة ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Architectural ',
                        'name_ar' => 'التصوير المعماري ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Conceptual ',
                        'name_ar' => 'التصوير المفاهيمي ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Mixed Media',
                        'name_ar' => 'الوسائط المختلطة',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Literature' ,
                'name_ar' => 'الآداب',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Novelists ',
                        'name_ar' => 'الروائيون',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Short Story Authors ',
                        'name_ar' => 'مؤلفو القصص القصيرة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Playwrights ',
                        'name_ar' => 'كتاب المسرح',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Poets',
                        'name_ar' => 'الشعراء',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Filmmaking' ,
                'name_ar' => 'صناعة الأفلام',
                'attributes' => [
                    [ 'name' => 'Type' ],
                    [ 'name' => 'Material' ],
                ],
                'children' => [
                    [
                        'name' => 'Feature Films',
                        'name_ar' => 'الأفلام الروائية',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Short Films',
                        'name_ar' => 'الأفلام القصيرة',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Documentaries',
                        'name_ar' => 'الأفلام الوثائقية',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Animation' ,
                'name_ar' => 'الرسوم المتحركة',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => '2D Animation ',
                        'name_ar' => 'الرسوم ثنائية الأبعاد',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => '3D Animation ',
                        'name_ar' => 'الرسوم ثلاثية الأبعاد',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Jewelry Design' ,
                'name_ar' => 'تصميم المجوهرات',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ]
            ],
            [ 'name' => 'Performance Arts' ,
                'name_ar' => 'فنون الأداء',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Actors',
                        'name_ar' => 'الممثلين',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Standup Comedians',
                        'name_ar' => 'الأداء الكوميدي ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Singers',
                        'name_ar' => 'المطربين ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Mime',
                        'name_ar' => 'فن الإيماء ',
                        'attributes' => [
                        ],
                    ],
                    [
                        'name' => 'Puppetry',
                        'name_ar' => 'فن الدمى',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Special Art Forms' ,
                'name_ar' => 'فنون خاصة',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ],
                'children' => [
                    [
                        'name' => 'Sand Painting',
                        'name_ar' => 'الرسم بالرمل',
                        'attributes' => [
                        ],
                    ],
                ]
            ],
            [ 'name' => 'Calligraphy' ,
                'name_ar' => 'الخط العربي',
                'attributes' => [
                    [ 'name' => 'Color' ],
                    [ 'name' => 'Art Style' ],
                    [ 'name' => 'Size' ],
                ]
            ]
        ];

        foreach ($data as $item)
        {
            $category = [];
            $category['name'] = $item['name'];
            $category['name_ar'] = $item['name_ar'];


            $category['slug'] = $this->generateSlug($item['name']);

            if(Category::where('name',$item['name'])->first())
                $category = Category::where('name',$item['name'])->first();
            else
                $category = Category::create($category);

                if($category){
    //                foreach ($item['attributes'] as $attr){
    //
    //                    $attribute = [];
    //                    $attribute['name'] = $attr['name'];
    //                    $attribute['name_ar'] = $attr['name'].' AR';
    //                    $attribute['slug'] = \Illuminate\Support\Str::slug($attr['name']);
    //
    ////                    $output->writeln($attribute);
    //
    //                    $category->attributes()->create($attribute);
    //                }

                    if(isset($item['children'])){
                        foreach ($item['children'] as $child){

                            $subcategory = [];
                            $subcategory['name'] = $child['name'];
                            $subcategory['name_ar'] = $child['name_ar'];


                            if(!Category::where('name',$child['name'])->first()) {
                                $subcategory['slug'] = $this->generateSlug($category->slug . '-' . $child['name']);
                                $subcategory = Category::create($subcategory);
                                CategoryParent::create(['category_id' => $subcategory->id, 'parent_category_id' => $category->id]);
                            }

    //                        foreach ($child['attributes'] as $attr){
    //
    //                            $attribute = [];
    //                            $attribute['name'] = $attr['name'];
    //                            $attribute['name_ar'] = $attr['name'].' AR';
    //                            $attribute['slug'] = \Illuminate\Support\Str::slug($attr['name']);
    //
    //    //                    $output->writeln($attribute);
    //
    //                            $subcategory->attributes()->create($attribute);
    //                        }
                        }
                    }
                }
        }
    }
}
