<?php

namespace Database\Seeders;

use App\Models\PageContent;
use Illuminate\Database\Seeder;

class ArtFestivalPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'page','section','title','title_ar','content','content_ar','type'

        $data = [
            [
                'page' => 'al-murabba-festival',
                'section' => 'top-content',
                'content' => '
                <h1>AL MURABBAA ART FESTIVAL</h1>
                <p>We exist to promote a dialogue between generations that enriches peoples’ lives through Arts & Culture, curating a space where everyone feels welcome.</p>
                <br/>
                <p><strong>We create a sense of belonging</strong></p>

                <p>True to the Ajman brand, we believe in making everyone feel like they belong with us. We break down the walls of knowledge to make it accessible to and enjoyable for all.</p>

                <p><strong>We make art accessible</strong></p>

                <p>We believe that knowledge can change peoples’ lives, but to do so we need to make sure we turn the most complex of ideas into simple stories.</p>

                <p><strong>We cultivate confidence</strong></p>

                <p>We give our guests the feeling that they will always leave our experience feeling better about themselves and empowered to keep on asking why.</p>

                <br/>
                <br/>
                <h1>What to look forward to</h1>
                <p>Discover a range of vibrant art, activities and exhibitions during our 10-day festival.</p>
                    ',
                'content_ar' => '
                    <h1>مهرجان المربعة للفنون<span></span></h1>
                <p>مهمتنا في الفنون والثقافة هي تعزيز الحوار بين الأجيال لإثراء حياة الناس مما يخلق مساحةً ترحب بالجميع. </p>

                <br/>
                <p><strong>	نخلق الشعور بالانتماء</strong></p>

                <p>تماشياً مع رؤية إمارة عجمان، نسعى جاهدين لجعل الجميع يشعرون بالانتماء إلينا، فنحن نكسر حواجز المعرفة لنجعلها ممتعةٌ للجميع وفي متناولهم.  </p>

                <p><strong>	نجعل الفن في متناول الجميع</strong></p>

                <p>نؤمن أن المعرفة بوسعها تغيير حياة الناس ولكن تحقيقاً لذلك علينا التأكد من تبسيط الأفكار المعقدة وتحويلها إلى قصص سهلة. </p>

                <p><strong>	نزرع الثقة بالنفس</strong></p>

                <p>نزرع في ضيوفنا شعور الرضى عن النفس وحب الاستطلاع عند خوضهم تجربتنا.</p>

                <br/>
                <br/>
                    <h1>ما الذي ينتظرك؟<span></span></h1>
                    <p>اكتشف مجموعة من الفنون والأنشطة والمعارض النابضة بالحياة من خلال مهرجاننا على مدى عشرة أيام. </p>
',
                'type' => 'html',
            ],
            [
                'page' => 'al-murabba-festival',
                'section' => 'pdf-file',
                'content' => 'uploads/art-festival/AlMurabbaa_1July2021.pdf',
                'content_ar' => 'uploads/art-festival/AlMurabbaa_1July2021.pdf',
                'type' => 'file',
            ],

        ];

        foreach ($data as $item){
            PageContent::create($item);
        }
    }
}

