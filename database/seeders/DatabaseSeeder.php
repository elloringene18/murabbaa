<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CategorySeeder::class);
        $this->call(CategoryAttributeSeeder::class);
//        $this->call(ArtistSeeder::class);
//        $this->call(ProductSeeder::class);
        $this->call(NewArtistProductSeeder::class);
        $this->call(EventTypeSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(AdministratorSeeder::class);
        $this->call(SponsorSeeder::class);
//        $this->call(MonthArtistSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(HomeCategorySeeder::class);
        $this->call(PageContentSeeder::class);
    }
}
