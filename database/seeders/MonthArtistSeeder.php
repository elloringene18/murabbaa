<?php

namespace Database\Seeders;

use App\Models\Artist;
use App\Models\MonthArtist;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class MonthArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_from = Carbon::now();
        $date_to = Carbon::now();
//        $artist = Artist::inRandomOrder()->first();

        $month = MonthArtist::create([
            'artist_id' => 2,
            'date_from' => $date_from->startOfMonth(),
            'date_to' => $date_to->endOfMonth(),
            'product_id' => 10,
        ]);

        $products = Artist::find(2)->products()->get()->pluck('id');
        $month->products()->sync($products);

    }
}
