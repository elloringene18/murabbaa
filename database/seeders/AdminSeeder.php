<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'name' => 'Admin',
            'email' => 'webmaster@murabbaa.com',
            'password' => \Illuminate\Support\Facades\Hash::make('Ajman@Murabbaa#2021!')
        ];

        \App\Models\User::create($data);
    }
}
