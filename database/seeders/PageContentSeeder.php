<?php

namespace Database\Seeders;

use App\Models\PageContent;
use Illuminate\Database\Seeder;

class PageContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'page','section','title','title_ar','content','content_ar','type'

        $data = [
            [
                'page' => 'about',
                'section' => 'welcome-title',
                'content' => 'Welcome to Almurabbaa',
                'content_ar' => 'مرحبا بك في منصة مواهب',
                'type' => 'text',
            ],
            [
                'page' => 'about',
                'section' => 'welcome-content',
                'content' => '<p>Welcome to Almurabbaa, a UAE initiative in collaboration with the Ministry of Culture, that gives local and regional artists a platform to exhibit and share their art with the world.</p>
                        <p>This carefully curated portal supports artists across all mediums and styles. So, come in, look around and be inspired.</p>',
                'content_ar' => '<p>أهلاً بكم في مواهب، مبادرة إماراتية بالتعاون مع وزارة الثقافة تهدف إلى توفير منصّة واحدة ومتكاملة للفنّانين في الوطن والمنطقة ليتشاركوا من خلالها أعمالهم وإبداعاتهم مع العالم. </p>
                        <p>تعمل هذه البوّابة المصمّمة بإتقان على تمكين الفنّانين ودعمهم مهما اختلفت أساليبهم والوسائل المستخدمة للتواصل والتعبير الفنّي، فندعوكم إلى زيارتنا، والتعرّف على منصّتنا والفنّانين وأعمالهم وتستلهموا منها.</p>',
                'type' => 'html',
            ],
            [
                'page' => 'about',
                'section' => 'mission-title',
                'content' => 'Mision & Vision',
                'content_ar' => 'الرؤية و الرسالة',
                'type' => 'text',
            ],
            [
                'page' => 'about',
                'section' => 'mission-photo',
                'content' => 'img/user-placeholder.jpg',
                'content_ar' => 'img/user-placeholder.jpg',
                'type' => 'image',
            ],
            [
                'page' => 'about',
                'section' => 'mission-content',
                'content' => '
                    <p><strong>Vision: </strong></p>
                    <p>“Empower our recognized artists and nurture our talents in the making through an innovative regional digital space that supports Creativity, Arts and cultural society”</p>
                    <p>&nbsp;</p>
                    <p><strong>Mission: </strong></p>
                    <p>As we take pride in our culture and are passionate about our artists and young talents, we are committed to: </p>
                    <ul>
                    <li>Bring inspiration and innovation to all the talents in the UAE</li>
                    <li>feature different UAE national and resident talents to boost the local art scene</li>
                    <li>Connect the community to create a more immersive experience</li>
                    <li>Provide a digital platform to showcase, promote, spread awareness and commercialize artistic talent</li>
                    <li>Bring in and highlight talents&rsquo; best practices</li>
                    <li>Serve and Give Back to our Society</li>
                    </ul>
                ',
                'content_ar' => '
                <p><strong>رؤيتنا: </strong></p>
                <p>تمكين الفنّانين المعروفين ورعاية المواهب الصاعدة ودعمهم من خلال توفيرمنصة إقليمية مبتكرة تسهم في تنمية الإبداع والفنون وتلاحم الفئات الفنية لإثراء مجتمعنا الثقافي.  </p>
                <p>&nbsp;</p>
                <p><strong>مهمتنا: </strong></p>
                <p>نفخر بثقافتنا ونهتم بشغف بفنّانينا ومواهبنا الشابة، وبالتالي نلتزم بالإهداف التالية: </p>
                <ul>
                <li>إلهام كافة الموهوبين في الدولة وتشجيعهم على الإبداع</li>
                <li>الترويج لكافة الموهوبين من الإماراتيين والوافدين سعياً منا إلى تعزيز صناعة الفنون المحلية</li>
                <li>تعزيز التواصل لتقديم تجربة غامرة وأكثر تفاعلية</li>
                <li>توفير منصة رقمية تجمع الفنّانين تحت سقف واحد لنشر الوعي عن المواهب الفنّية وعرض أعمالهم، والتروّيج لهم ودعمهم إقتصادياً وتجارياً </li>
                <li>تسليط الضوء على أفضل الممارسات التي ينتهجها الفنانون</li>
                <li>العطاء وخدمة مجتمعنا </li>
                </ul>
',
                'type' => 'html',
            ],
            [
                'page' => 'about',
                'section' => 'values-content',
                'content' => '
                    <p><strong>Values:</strong></p>
                    <p>&nbsp;</p>
                    <p><strong>DISCOVERY:</strong></p>
                    <p>We make sure to DISCOVER, embrace and see more than what meets the eyes</p>
                    <p>&nbsp;</p>
                    <p><strong>COLLABORATION:</strong></p>
                    <p>We COLLABORATE with our artists to bring to the public their work. Masterpieces and success stories</p>
                    <p>&nbsp;</p>
                    <p><strong>CONNECTION:</strong></p>
                    <p>We CONNECT with the Community and share their successes</p>
                    <p>&nbsp;</p>
                    <p><strong>PASSION:</strong></p>
                    <p>We are PASSIONATE about heritage culture, and all types of artistic talents</p>
                    <p>&nbsp;</p>
                    <p><strong>IDENTITY:</strong></p>
                    <p>We highlight our IDENTITY that embraces our history and paves our future through the discipline of art and unique experiences.</p>
                    <p>&nbsp;</p>
                    <p><strong>VALUING ARTISTS</strong></p>
                    <p>We recognize, appreciate, encourage , celebrate and VALUE our talents and provide them with means that contribute to their success.</p>
                    <p>&nbsp;</p>
                    <p><strong>INNOVATION:</strong></p>
                    <p>We foster INNOVATION, and draw the future of the generations to come in an artistic, distinguished, and creative manner</p>
                ',
                'content_ar' => '
                    <p><strong>قيمنا:</strong></p>
                    <p>&nbsp;</p>
                    <p><strong>الاكتشاف:</strong></p>
                    <p>نتأكد من اكتشاف وإحتضان أكثر ما تراه العين </p>
                    <p>&nbsp;</p>
                    <p><strong>التعاون:</strong></p>
                    <p>نتعاون مع فنانينا لنقدم إلى الجمهور أعمالهم، وروائعهم وقصص نجاحاتهم.</p>
                    <p>&nbsp;</p>
                    <p><strong>التواصل:</strong></p>
                    <p>نتواصل مع مجتمعنا الثقافي الفني ونشارك نجاحاتهم</p>
                    <p>&nbsp;</p>
                    <p><strong>الشغف:</strong></p>
                    <p>نجسد شغفنا حول تراثنا الثقافي وكافة الفئات من المواهب الفنية </p>
                    <p>&nbsp;</p>
                    <p><strong>هويتنا:</strong></p>
                    <p>نسلط الضوء على هويتنا التي تحتضن  تاريخنا وترسم مستقبلنا من خلال تجارب فنية فريدة </p>
                    <p>&nbsp;</p>
                    <p><strong>تقدير الفنانين:</strong></p>
                    <p>نقدّر الفنانين وندعمهم ونشجعهم ونحتفل بهم ونقدم لهم ما يلزمهم لتحقيق نجاحاتهم.</p>
                    <p>&nbsp;</p>
                    <p><strong>الإبتكار:</strong></p>
                    <p>ندعم الإبتكار ونرسم مستقبل الأجيال القادمة بأسلوب فني ومتميز ومبتكر   </p>
    ',
                'type' => 'html',
            ],
            [
                'page' => 'about',
                'section' => 'administration-title',
                'content' => 'Almurabbaa’s Team Members',
                'content_ar' => 'أعضاء الفريق',
                'type' => 'text',
            ],
            [
                'page' => 'about',
                'section' => 'administration-content',
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
                        </p>',
                'content_ar' => '<p>
                            لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريدأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس
                        </p>',
                'type' => 'html',
            ],
            [
                'page' => 'about',
                'section' => 'administration-carousel-title',
                'content' => 'Almurabbaa’s Team Members',
                'content_ar' => 'أعضاء الفريق',
                'type' => 'text',
            ],
            [
                'page' => 'about',
                'section' => 'partner-title',
                'content' => 'Strategic Partners',
                'content_ar' => 'الشركاء الاستراتيجين',
                'type' => 'text',
            ],
//            [
//                'page' => 'about',
//                'section' => 'partner-content',
//                'content' => '<p>Thanks to all our partners. Lorem ipsum sit amet lorem ipsum.</p>',
//                'content_ar' => '<p>لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس </p>',
//                'type' => 'html',
//            ],
            [
                'page' => 'footer',
                'section' => 'copyright',
                'content' => '© 2021 Almurabbaa - All Rights Reserved',
                'content_ar' => '© 2021 Almurabbaa - جميع الحقوق محفوظة',
                'type' => 'text',
            ],
            [
                'page' => 'footer',
                'section' => 'facebook',
                'content' => 'http://www.facebook.com',
                'content_ar' => 'http://www.facebook.com',
                'type' => 'text',
            ],
            [
                'page' => 'footer',
                'section' => 'twitter',
                'content' => 'http://www.twitter.com',
                'content_ar' => 'http://www.twitter.com',
                'type' => 'text',
            ],
            [
                'page' => 'footer',
                'section' => 'instagram',
                'content' => 'http://www.instagram.com',
                'content_ar' => 'http://www.instagram.com',
                'type' => 'text',
            ],
            [
                'page' => 'footer',
                'section' => 'snippets',
                'content' => '',
                'content_ar' => '',
                'type' => 'code',
            ],
            [
                'page' => 'header',
                'section' => 'snippets',
                'content' => '
                  <meta name="description" content="">
                  <meta property="og:title" content="">
                  <meta property="og:type" content="">
                  <meta property="og:url" content="">
                  <meta property="og:image" content="">
                ',
                'content_ar' => '
                  <meta name="description" content="">
                  <meta property="og:title" content="">
                  <meta property="og:type" content="">
                  <meta property="og:url" content="">
                  <meta property="og:image" content="">
                  ',
                'type' => 'code',
            ],
            [
                'page' => 'contact',
                'section' => 'contact-email',
                'content' => 'artists@ajmantourism.ae',
                'content_ar' => 'artists@ajmantourism.ae',
                'type' => 'text',
            ],

        ];

        foreach ($data as $item){
            PageContent::create($item);
        }
    }
}
