<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FestivalItem;

class ArtFestivalItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Main stage',
                'title_ar' => 'المسرح الرئيسي',
                'content' => '
                Concerts, fashion shows, speakers, readings and movies will be hosted and screened here throughout the entire 10 days. From traditional artists to more modern bands, the stage will showcase the diversity of the UAE artists scene. 
                ',
                'content_ar' => '
                سيستضيف هذا المسرح حفلات موسيقية وعروض أزياء وخطابات وقراءات للنصوص الأدبية وسيعرضهم على مدى العشرة أيام، كما سسيعكس ثراء المشهد الفني الإماراتي بدءاً من الفنانين الكلاسيكيين ووصلاً إلى الفرق الموسيقية المعاصرة.
                ',
                'thumbnail' => 'img/festival/01.png',
            ],
            [
                'title' => 'Installation – The Art Room',
                'title_ar' => 'المنشأة – غرفة الفن
                ',
                'content' => '
                Our “Art Room” is a chance for everyone to create a collective art piece. Step in and let your creativity contribute to the masterpiece. 
                ',
                'content_ar' => '
                تتيح "غرفة الفن" الفرصة للجميع للتعاون على خلق قطعة فنية. انضم إلينا وأطلق العنان لإبداعك للمساهمة في تكوين التحفة الفنية.
                ',
                'thumbnail' => 'img/festival/02.png',
            ],
            [
                'title' => 'Workshops & programmes',
                'title_ar' => 'البرامج وورش العمل',
                'content' => '
                Daily workshops and programmes from local artists, creative entrepreneurs and local art market leaders will be hosted to cultivate conversations and collaboration. Sessions will include workshops on art history, design masterclasses, contemporary art and more.
                ',
                'content_ar' => '
                ستستضاف ورش عمل وبرامج يومية لفنانين محليين ورواد أعمال محليين مبدعين في المجال الفني لخلق محادثات وبث روح التعاون.
                ',
                'thumbnail' => 'img/festival/03.png',
            ],
            [
                'title' => 'Food art',
                'title_ar' => 'فن الطبخ',
                'content' => '
                Our local community of food & beverage suppliers will offer the cuisine and culture of Ajman. From traditional Emirati hospitality to modern street food, the food art area has something for everyone.
                ',
                'content_ar' => '
                سيعرف مجتمعنا المحلي من موردي المأكولات والمشروبات الجمهور على مطبخ عجمان وثقافته. منطقة فنون الطبخ لديها ما يناسب الجميع انطلاقاً من الضيافة الإماراتية ووصولاً إلى طام الشارع الحديث.
                ',
                'thumbnail' => 'img/festival/04.png',
            ],
        ];

        foreach ($data as $item)
            FestivalItem::create($item);
    }
}
