<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryParent;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Output\ConsoleOutput;

class NewCategorySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'name' => 'Art',
                'name_ar' => '',
                'children' => [
                    [
                        'name' => 'Original', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Digital Art', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Sculpture', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Photography', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Calligraphy', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Installation', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Illustration – Comix', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Animation', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Live performance', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Pop up studio', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Street Art- Mural', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Printing making', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Special Art form', 'name_ar' => ''
                    ],
                ]
            ],
            //
            [
                'name' => 'Design',
                'name_ar' => '',
                'children' => [
                    [
                        'name' => 'Product design', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Fashion', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Architecture', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Art & Craft', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Jewellery Design', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Graphic Design', 'name_ar' => ''
                    ],
                ]
            ],
            //
            [
                'name' => 'WorkShops',
                'name_ar' => '',
            ],
            //
            [
                'name' => 'Literature',
                'name_ar' => '',
                'children' => [
                    [
                        'name' => 'Editorial', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Poetry', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Children literature', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Comics & Graphic Novel', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Digital', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Conceptual', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Playwright', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Short story Authors', 'name_ar' => ''
                    ],
                ]
            ],
            //
            [
                'name' => 'Stage - Movie - Theatre ',
                'name_ar' => '',
                'children' => [
                    [
                        'name' => 'Film making', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Play action', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Singer', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Band', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Standup Performance', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Actor', 'name_ar' => ''
                    ],
                    [
                        'name' => 'Documentary', 'name_ar' => ''
                    ],
                ]
            ],
        ];

        foreach ($data as $item)
        {
            $category = [];
            $category['name'] = $item['name'];
            $category['name_ar'] = $item['name_ar'];

            $category['slug'] = $this->generateSlug($item['name']);

            if(Category::where('name',$item['name'])->first())
                $category = Category::where('name',$item['name'])->first();
            else
                $category = Category::create($category);

            if($category){

                if(isset($item['children'])){
                    foreach ($item['children'] as $child){

                        $subcategory = [];
                        $subcategory['name'] = $child['name'];
                        $subcategory['name_ar'] = $child['name_ar'];

                        if(!Category::where('name',$child['name'])->first()) {
                            $subcategory['slug'] = $this->generateSlug($category->slug . '-' . $child['name']);
                            $subcategory = Category::create($subcategory);
                            CategoryParent::create(['category_id' => $subcategory->id, 'parent_category_id' => $category->id]);
                        }

                    }
                }
            }
        }
    }
}
