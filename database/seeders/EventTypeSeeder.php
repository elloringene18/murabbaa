<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\EventType;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class EventTypeSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, EventType $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'title' => 'Workshops',
                'title_ar' => 'ورشات العمل',
            ],
            [
                'title' => 'Specialized learning Programs',
                'title_ar' => 'برامج التعلم المتخصصة',
            ],
            [
                'title' => 'Interviews',
                'title_ar' => 'المقابلات',
            ],
            [
                'title' => 'Talks and Conversations',
                'title_ar' => 'المحادثات',
            ],
            [
                'title' => 'Panel Discussions',
                'title_ar' => 'حلقات النقاش',
            ],
            [
                'title' => 'Lectures',
                'title_ar' => 'المحاضرات',
            ],
            [
                'title' => 'Film Nights',
                'title_ar' => 'ليالي الأفلام',
            ],
            [
                'title' => 'Musical Nights',
                'title_ar' => 'ليالي موسيقية',
            ],
        ];

        foreach ($data as $category)
        {
            EventType::create([
                'slug'=> $this->generateSlug($category['title']),
                'title'=> $category['title'],
                'title_ar'=> $category['title_ar'],
            ]);
        }
    }
}
