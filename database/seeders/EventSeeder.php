<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\EventType;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class EventSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Event $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [];

        for($x=0;$x<100;$x++){
            $data[] = ['title' => $this->faker->text(rand(10,30))];
        }

        foreach ($data as $item)
        {
            $product = [];
            $product['date'] = $this->faker->date('Y-m-d');
            $product['location'] = $this->faker->address();
            $product['title'] = $item['title'];
            $product['title_ar'] = $item['title'].' AR';
            $product['slug'] = $this->generateSlug($item['title']);
            $product['content'] = '<p>'.$this->faker->paragraph('50').'</p>';
            $product['content_ar'] = '<p>'.$this->faker->paragraph('50').'</p>';
            $product['event_type_id'] = EventType::inRandomOrder()->first()->id;

            Event::create($product);
        }


    }
}
