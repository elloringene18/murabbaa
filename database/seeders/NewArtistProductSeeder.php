<?php

namespace Database\Seeders;

use App\Models\Artist;
use App\Models\Product;
use App\Models\ProductArtist;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class NewArtistProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name' => 'LAMA',
                'name_ar' => 'LAMA',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => 'MUSIC',
//                        'title' => 'MUSIC',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>19 cm x 21.5 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'SPRING',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => 'uploads\products\lama\Spring.JPG',
//                        'photo_full' => 'uploads\products\lama\Spring.JPG',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'THINKING',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>210 cm x 70 cm</p>',
//                        'photo' => 'uploads\products\lama\Thinking.jpg',
//                        'photo_full' => 'uploads\products\lama\Thinking.jpg',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'SUMMER
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MOON',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Portrait On Paper
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on Paper</p>
//<p>Portrait</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'FREEDOM
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => 'uploads\products\lama\Freedom.jpg',
//                        'photo_full' => 'uploads\products\lama\Freedom.jpg',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Accept Underst and Love
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => 'uploads\products\lama\AcceptUnderstandLove.jpg',
//                        'photo_full' => 'uploads\products\lama\AcceptUnderstandLove.jpg',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'RED CARPET
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>
//',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'WORDS
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Landscape</p>
//<p>300 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'SUMMER
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'FEMALE
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'SUNNY
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media on fine art paper</p>
//<p>Portrait</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
            [
                'name' => 'LOAI',
                'name_ar' => 'LOAI',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 1',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>2-Portrait panels</p>
//<p>80 cm x 60 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 2',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 3',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 4',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>200 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 5',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>2-Portrait panels</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 6',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>3-Landscape panel</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 7',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 1400 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 8',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 9',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>300 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 10',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>140 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 11',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 12',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 13',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 14',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'LOAI 15',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
            [
                'name' => 'MARIA',
                'name_ar' => 'MARIA',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'Oops! Alice!
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>2-Portrait panels</p>
//<p>60 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'I’d Rather Be in Bed!
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>70 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Palm Symphony
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media photography</p>
//<p>&amp; acrylic</p>
//<p>2-Portrait panels</p>
//<p>70 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Benjamin',
//                        'description' => '<p>Original</p>
//<p>Mixed media photography</p>
//<p>and acrylic</p>
//<p>Landscape</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Junk Gold
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media, acrylic &amp; gold leaf</p>
//<p>3-Portrait panels</p>
//<p>90 cm x 60 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Mad Mask
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>140 cm x 200 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'K’Ching
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>3-Square panels</p>
//<p>140 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Tears Apart
//',
//                        'description' => '<p>Original</p>
//<p>Photography &amp; acrylic</p>
//<p>8-Portrait panels</p>
//<p>35 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Frida Cracks Gold
//',
//                        'description' => '<p>Original</p>
//<p>Acrylic &amp; gold leaf</p>
//<p>Landscape</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Heartbreak Sunshine
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>80 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Happy Marilyn
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Disco Flamingo
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'The Kiss
//
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>2-Portrait panels</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Mean Girls
//
//',
//                        'description' => '<p>Original</p>
//<p>Mixed media &amp; acrylic</p>
//<p>Portrait</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
//            [
//                'name' => 'Simone Micheli',
//                'name_ar' => 'Simone Micheli',
//                'bio' => '',
//                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 1',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>110 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 2',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>80 cm x 40 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 3',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>110 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 4',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>110 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 5',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>160 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 6',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 7',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 8',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 9',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>160 cm x 200 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 10',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>40 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 11',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>160 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 12',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 13',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>100 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 14',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 15',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>120 cm x 275 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 16',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>120 cm x 275 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Flower Collection 17',
//                        'description' => '<p>Original</p>
//<p>Photography printed in Fine Art Cotton canvas with additional Acrylic painting touch &amp;</p>
//<p>brush of the artist to be a unique piece .</p>
//<p>One off author production</p>
//<p>130 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 1',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 2',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 3',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>110 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 4',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 5',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>200 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 6',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>140 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 7',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>140 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 8',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>140 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 9',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>200 cm x 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Molecules',
//                        'description' => '<p>Original</p>
//<p>Installation</p>
//<p>Diameter 30 cm &amp; 60 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 11',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Green Grass
//',
//                        'description' => '<p>Original</p>
//<p>Installation </p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 13',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Molecules',
//                        'description' => '<p>Original</p>
//<p>Installation</p>
//<p>Diameter 30 cm &amp; 60 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 15',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 16',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Black White & Silver Collection 17',
//                        'description' => '<p>Original</p>
//<p>Black white &amp; silver photography printed in Fine Art Cotton canvas.</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                ],
//            ],
            [
                'name' => 'Rayyes',
                'name_ar' => 'Rayyes',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 1',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>120 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 2',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 3',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>70 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 4',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>200 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 5',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>2-Portrait panels</p>
//<p>100 cm x 70 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 6',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>Diameter 140 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 7',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>2-Portrait panels</p>
//<p>180 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 8',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>300 cm x 50 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 9',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 10',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>140 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 11',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 12',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 13',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 14',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>260 cm x 180 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Rayyes 15',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 90 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
            [
                'name' => 'Thaer Khazem',
                'name_ar' => 'Thaer Khazem',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 1',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>200 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 2',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 3',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>100 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 4',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>100 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 5',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>200 cm x 200 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 6',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 7',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 8',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 9',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 10',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 11',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Landscape</p>
//<p>200 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 12',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>120 cm x 80 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 13',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>150 cm x 150 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 14',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>160 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 15',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>100 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 16',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 17',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Portrait</p>
//<p>200 cm x 100 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'Thaer Khazem 18',
//                        'description' => '<p>Original</p>
//<p>Mixed media on canvas</p>
//<p>Square</p>
//<p>120 cm x 120 cm</p>',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
            [
                'name' => 'QIP',
                'name_ar' => 'QIP',
                'bio' => '',
                'products' => [
                ],
            ],
            [
                'name' => 'MISK',
                'name_ar' => 'MISK',
                'bio' => '',
                'products' => [
//                    [
//                        'year' => '',
//                        'title' => 'MISK 1',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 2',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 3',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 4',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 5',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 6',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 7',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 8',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
//                    [
//                        'year' => '',
//                        'title' => 'MISK 9',
//                        'description' => '',
//                        'photo' => '',
//                        'photo_full' => '',
//                        'category_id' => 2,
//                        'for_sale' => 0,
//                    ],
                ],
            ],
        ];

        foreach($data as $item)
        {
            $artist['name'] = $item['name'];
            $artist['name_ar'] = $item['name'];
            $artist['bio'] = $item['bio'];
            $artist['slug'] = Str::slug($item['name']);

            $newartist = Artist::create($artist);

            if($newartist){
                foreach ($item['products'] as $product){
                    $product['slug'] = $this->generateSlug($product['title']);
                    $newProduct = Product::create($product);


                    ProductArtist::create(['product_id'=>$newProduct->id,'artist_id'=>$newartist->id]);
                }
            }
        }
    }
}
