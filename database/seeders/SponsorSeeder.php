<?php

namespace Database\Seeders;

use App\Models\Sponsor;
use Faker\Generator;
use Illuminate\Database\Seeder;

class SponsorSeeder extends Seeder
{

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x=0;$x<12;$x++)
        {
            $name = $this->faker->company;

            Sponsor::create([
                'name' => $name,
                'name_ar' => $name.' AR',
            ]);
        }
    }
}
