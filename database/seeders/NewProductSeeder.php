<?php namespace Database\Seeders;

use App\Models\Artist;
use App\Models\Category;
use App\Models\MonthArtist;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Models\ProductArtist;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Database\Seeder;

class NewProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker)
    {
        $this->model = $model;
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();
        $output->writeln("Starting Seed");

        $destinationPath = 'public/artworks/';

        // $this->model->delete();

        foreach (Storage::disk('local')->listContents('public') as $folder) {

            if($folder['type']=='dir'){

                $output->writeln($folder['path']);

                foreach (Storage::disk('local')->listContents($folder['path']) as $file) {
                    $image =  new \Illuminate\Http\UploadedFile( storage_path("app/".$file['path']), 'tmp.jpg', 'image/jpeg',null,true);

                    $img = Image::make($image);

                    $fullName = $image->getFilename();
                    $extension = $image->getClientOriginalExtension();
                    $onlyName = explode('.'.$extension,$fullName);

                    if(Artist::where('slug',Str::slug($folder['basename']))->count() == 0){
                        $artist = Artist::create([
                            'name' => $folder['basename'],
                            'name_ar' => $folder['basename'],
                            'slug' => Str::slug($folder['basename'])
                        ]);

                        $output->writeln('Artist Created: '. $artist->name);
                    } else {
                        $artist = Artist::where('slug',Str::slug($folder['basename']))->first();
                        $output->writeln('Artist Exists: '. $artist->name);
                    }

                    File::makeDirectory($destinationPath.$file['dirname'], 0777, true, true);  

                    $output->writeln('Folder Created: '. $file['dirname']); 

                    if ($img->height()>$img->width())
                        Image::make($image->getRealPath())->resize(null,1080, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'public/'.$folder['basename'].'/'.$fullName);
                    else
                        Image::make($image->getRealPath())->resize(1080,null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'public/'.$folder['basename'].'/'.$fullName);

                    // if(!file_exists(public_path('/artworks/public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension))){
                        
                        // $output->writeln('New Product'); 

                        Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension);
                        // Create

                        $item = [
                            'year' => '2020',
                            'title' => $onlyName[0],
                            'title_ar' => $onlyName[0],
                            'description' => '<p></p>',
                            'description_ar' => '<p></p>',
                            'photo' => $destinationPath.'public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension,
                            'photo_full' => $destinationPath.$file['path'],
                            'category_id' => 1,
                            'for_sale' => 1,
                            'slug' => \Illuminate\Support\Str::slug($onlyName[0]).'-'.rand(1,99999),
                        ];

                        $newproduct = Product::create($item);

                        if($newproduct){

                            // if($newproduct->category->parent)
                            //     $attributes = $newproduct->category->parent->attributes;
                            // else
                            //     $attributes = $newproduct->category->attributes;

                            // foreach ($attributes as $attribute){
                            //     $value = $attribute->values()->inRandomOrder()->first();
                            //     ProductAttribute::create(['attribute_value_id'=>$value->id,'product_id'=>$newproduct->id]);
                            // }

                            ProductArtist::create(['product_id'=>$newproduct->id,'artist_id'=>$artist->id]);

                        }
                    // } else {
                    //     $output->writeln('Product Exists'); 
                    // }
                }
            }
        }
    }
}

