<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
                'name' => 'Gene Ellorin',
                'email' => 'gene@thisishatch.com',
                'password' => \Illuminate\Support\Facades\Hash::make('secret')
            ];

        \App\Models\User::create($data);

        $data = [
                'name' => 'Mawaheb Admin',
                'email' => 'webmaster@mawaheb.com',
                'password' => \Illuminate\Support\Facades\Hash::make('Ajman@Mawaheb#2021!')
            ];

        \App\Models\User::create($data);
    }
}
