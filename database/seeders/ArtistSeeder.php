<?php

namespace Database\Seeders;

use App\Models\Artist;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class ArtistSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Artist $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        // 'name','bio','email','telephone','mobile'

//
//        for($x=0;$x<5;$x++)
//        {
//            $name = $this->faker->name;
//            $details = $this->faker->text(rand(30,50));
//
//            Artist::create([
//                'name' => $name,
//                'name_ar' => $name.'_ar',
//                'slug' => $this->generateSlug($name),
//                'bio' => '<p>'.$this->faker->paragraph('20').'</p>',
//                'bio_ar' => '<p>'.$this->faker->paragraph('20').'</p>',
//                'email' => $this->faker->safeEmail,
//                'telephone' => rand(100000,999999),
//                'mobile' => $this->faker->phoneNumber,
//            ]);
//        }


        $data = [
            [
                'name' => 'Ammar Mohammed Al Attar',
                'name_ar' => 'عمار محمد العطار',
                'slug' => $this->generateSlug('Ammar Mohammed Al Attar'),
                'email' => 'ammaralattar@gmail.com',
                'mobile' => '0507917911',
                'bio' => '<p>Ammar Al Attar is a photographer and mixed media artist. Born in 1981, he lives in Ajman in the United Arab Emirates.</p>
                    <p>Completely self-taught, Al Attar&rsquo;s practice seeks to not only document and translate but also methodically research and examine aspects of Emirati ritual, material culture, and geographic orientation that are increasingly illusive in his rapidly globalizing society.</p>
                    <p>&nbsp;</p>
                    <p><strong>Exhibitions:</strong></p>
                    <p>2019 - 2020</p>
                    <ul>
                    <li>Place I call home &ldquo;Touring exhibition between GCC countries and UK 2019-2020</li>
                    </ul>
                    <p>2017</p>
                    <ul>
                    <li>Deformation, Cuadro Art Gallery, Dubai</li>
                    <li>Plaza Cinema, Commission by Al Serkal Avenue, Dubai</li>
                    </ul>
                    <p>2018</p>
                    <ul>
                    <li>Art Abu Dhabi commission, Qasr Al Muaigai, Al Ain</li>
                    </ul>
                    <p>2016</p>
                    <ul>
                    <li>Al Haraka Baraka, Maraya Art Center, Sharjah, UAE</li>
                    <li>Place and Unity, Abu Dhabi, UAE</li>
                    <li>Shara Art Fair, Jeddah, KSA</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Awards:</strong></p>
                    <ul>
                    <li>Sharjah Islamic Art Festival photography exhibition 2013</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Website:&nbsp;</strong>www.ammaralattar.com</p>
                    <p><strong>Social Media:&nbsp;@ammaralattar</strong></p>',
                'bio_ar' => '<p>Ammar Al Attar is a photographer and mixed media artist. Born in 1981, he lives in Ajman in the United Arab Emirates.</p>
                    <p>Completely self-taught, Al Attar&rsquo;s practice seeks to not only document and translate but also methodically research and examine aspects of Emirati ritual, material culture, and geographic orientation that are increasingly illusive in his rapidly globalizing society.</p>
                    <p>&nbsp;</p>
                    <p><strong>Exhibitions:</strong></p>
                    <p>2019 - 2020</p>
                    <ul>
                    <li>Place I call home &ldquo;Touring exhibition between GCC countries and UK 2019-2020</li>
                    </ul>
                    <p>2017</p>
                    <ul>
                    <li>Deformation, Cuadro Art Gallery, Dubai</li>
                    <li>Plaza Cinema, Commission by Al Serkal Avenue, Dubai</li>
                    </ul>
                    <p>2018</p>
                    <ul>
                    <li>Art Abu Dhabi commission, Qasr Al Muaigai, Al Ain</li>
                    </ul>
                    <p>2016</p>
                    <ul>
                    <li>Al Haraka Baraka, Maraya Art Center, Sharjah, UAE</li>
                    <li>Place and Unity, Abu Dhabi, UAE</li>
                    <li>Shara Art Fair, Jeddah, KSA</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Awards:</strong></p>
                    <ul>
                    <li>Sharjah Islamic Art Festival photography exhibition 2013</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Website:&nbsp;</strong>www.ammaralattar.com</p>
                    <p><strong>Social Media:&nbsp;@ammaralattar</strong></p>',
            ],
            [
                'name' => 'Budour Alali',
                'name_ar' => 'بدور العلي',
                'slug' => $this->generateSlug('Budour Alali'),
                'bio' => '<p>Painting has been my one true love since I was little, and my art helped me explore and discover who I was, and who I wanted to be in this world. I&rsquo;ve seen how it transformed me over the years, opened and expanded my heart, and offered me a sense of belonging and purpose, no matter what was going on around me.</p>
                    <p>Aside from the meditative value it adds, I believe that art tells an important story about who we are, where we are and expands us as a people. It inculcates a love of learning and of acquiring knowledge because it opens the mind. This is why I do my best to encourage others to paint and cultivate their artistic talents through a number of different initiatives and workshops.</p>
                    <p>I seek creativity out in all areas of my life. You may know me as an artist, but painting is simply one of the many passions that occupy a space in my full heart. I love music and play the zither, I write poetry filled with my most intimate heartbeats, I am inspired by working with young minds of our future, and enjoy nothing more than surrounding myself with the beauty and wonder of Mother Nature.</p>
                    <p>I believe all of us create art every single day. Art is not just the meticulous brush strokes on a blank canvas. Art can be found in the way we move our bodies upon hearing our favorite song, it can be found in watching the flowers you planted last year bloom in your back garden, and it can be found in the blanket of twinkling stars that illuminate the dark of the night sky and light up the way. We are all works of art in progress, and life is our canvas. There is no right or wrong, only choices.</p>
                    <p>Over the years, I have created multiple art initiatives with local schools, as a way to give back to my community and empower our youth - the future of our nation. I believe that the arts harbor special powers that remove all barriers and boundaries within our children, opening up a pathway for them to glimpse their inner beauty and truest selves; and providing them with the courage to let that shine for the world to see, in a space that is judgment free.</p>
                    <p>My light burns brightly for my homeland of the UAE and everything it stands for. Through my artistic expressions, I highlight the beauty of all its heritage, landscape and wildlife. This country has been abundantly generous with its residents, locals and visitors alike for which I am grateful. It is full of charm and I want to share it with the world.</p>
                    <p>I am able to achieve this not only by producing art pieces about the UAE but also through founding my gallery BEDOURS where I showcase my sentiments for the UAE in the form of art. I also produce art collections, themes and initiatives that invite people to appreciate different aspects of the UAE.</p>
                    <p>I also hold much pride in being a woman, and I strive to strengthen the image of the independent and hardworking women in the Arab and Muslim world. I wish to empower women and dignify their role in society by illustrating the positive values they exude. In collaboration with government entities and other galleries, I am continually developing cultural initiatives that celebrate what it means to be a Muslim Arab woman today.&nbsp;</p>
                    <p>Beyond the pieces I create or the projects I undertake, I&rsquo;m a free spirit flowing with passion and love in the river I call&nbsp;<em>life.</em></p>
                    <p>As you can see, I am poetic in nature and love is important to me. In its purest form, it is the fabric of our being and exists below the ruffles that form our identity and it is exactly that which I live for, cultivate and seek out in people.</p>
                    <p>Sensitive to my surroundings, energy is something I pick up on all the time. I pay attention to it; I process it and transform it into something that makes me smile inside. After all, everything is a choice and I choose to rejoice in life whether it is in the food I cook, the paintings I make or the articles or poems I write.</p>
                    <p>In the early hours of dawn, I meditate to connect with my inner peace and pray for the resilience to live by my values and the strength to help others around me.</p>
                    <p>And every sunset I kneel with love and humility to give thanks for everything I have been blessed with. From the country I am from, to the friendships I have formed and to the family I have been given: I am overflowing with waves of gratitude</p>
                    <p>I am happy to have found a place for all my values and dreams to unite; and this is under the strokes and shades of my brushes.</p>
                    <p>I don&rsquo;t know what comes next, and I don&rsquo;t know where I&rsquo;ll end up going, but I can&rsquo;t wait to find out.</p>
                    <p>&nbsp;</p>',
                                    'bio_ar' => '<p>Painting has been my one true love since I was little, and my art helped me explore and discover who I was, and who I wanted to be in this world. I&rsquo;ve seen how it transformed me over the years, opened and expanded my heart, and offered me a sense of belonging and purpose, no matter what was going on around me.</p>
                    <p>Aside from the meditative value it adds, I believe that art tells an important story about who we are, where we are and expands us as a people. It inculcates a love of learning and of acquiring knowledge because it opens the mind. This is why I do my best to encourage others to paint and cultivate their artistic talents through a number of different initiatives and workshops.</p>
                    <p>I seek creativity out in all areas of my life. You may know me as an artist, but painting is simply one of the many passions that occupy a space in my full heart. I love music and play the zither, I write poetry filled with my most intimate heartbeats, I am inspired by working with young minds of our future, and enjoy nothing more than surrounding myself with the beauty and wonder of Mother Nature.</p>
                    <p>I believe all of us create art every single day. Art is not just the meticulous brush strokes on a blank canvas. Art can be found in the way we move our bodies upon hearing our favorite song, it can be found in watching the flowers you planted last year bloom in your back garden, and it can be found in the blanket of twinkling stars that illuminate the dark of the night sky and light up the way. We are all works of art in progress, and life is our canvas. There is no right or wrong, only choices.</p>
                    <p>Over the years, I have created multiple art initiatives with local schools, as a way to give back to my community and empower our youth - the future of our nation. I believe that the arts harbor special powers that remove all barriers and boundaries within our children, opening up a pathway for them to glimpse their inner beauty and truest selves; and providing them with the courage to let that shine for the world to see, in a space that is judgment free.</p>
                    <p>My light burns brightly for my homeland of the UAE and everything it stands for. Through my artistic expressions, I highlight the beauty of all its heritage, landscape and wildlife. This country has been abundantly generous with its residents, locals and visitors alike for which I am grateful. It is full of charm and I want to share it with the world.</p>
                    <p>I am able to achieve this not only by producing art pieces about the UAE but also through founding my gallery BEDOURS where I showcase my sentiments for the UAE in the form of art. I also produce art collections, themes and initiatives that invite people to appreciate different aspects of the UAE.</p>
                    <p>I also hold much pride in being a woman, and I strive to strengthen the image of the independent and hardworking women in the Arab and Muslim world. I wish to empower women and dignify their role in society by illustrating the positive values they exude. In collaboration with government entities and other galleries, I am continually developing cultural initiatives that celebrate what it means to be a Muslim Arab woman today.&nbsp;</p>
                    <p>Beyond the pieces I create or the projects I undertake, I&rsquo;m a free spirit flowing with passion and love in the river I call&nbsp;<em>life.</em></p>
                    <p>As you can see, I am poetic in nature and love is important to me. In its purest form, it is the fabric of our being and exists below the ruffles that form our identity and it is exactly that which I live for, cultivate and seek out in people.</p>
                    <p>Sensitive to my surroundings, energy is something I pick up on all the time. I pay attention to it; I process it and transform it into something that makes me smile inside. After all, everything is a choice and I choose to rejoice in life whether it is in the food I cook, the paintings I make or the articles or poems I write.</p>
                    <p>In the early hours of dawn, I meditate to connect with my inner peace and pray for the resilience to live by my values and the strength to help others around me.</p>
                    <p>And every sunset I kneel with love and humility to give thanks for everything I have been blessed with. From the country I am from, to the friendships I have formed and to the family I have been given: I am overflowing with waves of gratitude</p>
                    <p>I am happy to have found a place for all my values and dreams to unite; and this is under the strokes and shades of my brushes.</p>
                    <p>I don&rsquo;t know what comes next, and I don&rsquo;t know where I&rsquo;ll end up going, but I can&rsquo;t wait to find out.</p>
                    <p>&nbsp;</p>',
            ],
            [
                'name' => 'Diaa Allam',
                'name_ar' => 'ضياء العلم',
                'slug' => $this->generateSlug('Diaa Allam'),
                'bio' => '',
                'bio_ar' => '',
            ],
            [
                'name' => 'Nadia Yoshioka',
                'name_ar' => 'نادية يوشي اوكا',
                'slug' => $this->generateSlug('Nadia Yoshioka'),
                'bio' => '<p>My name is Nadia Yoshioka and I am a self-taught artist living in Ajman, UAE. I am passionate about arts in general but I specifically enjoy painting abstracts and landscapes, sometimes I combine them two and create an abstract landscape painting. I am inspired by nature to paint and I believe in happiness and peacefulness that comes from painting a beautiful piece.  I’m a Japanese Pakistani National and I am currently studying Masters of Arts in Business in an university in Dubai. I was always good in arts since childhood and since then I have been creating and improving my self as an artist.</p>',
                'bio_ar' => '<p>My name is Nadia Yoshioka and I am a self-taught artist living in Ajman, UAE. I am passionate about arts in general but I specifically enjoy painting abstracts and landscapes, sometimes I combine them two and create an abstract landscape painting. I am inspired by nature to paint and I believe in happiness and peacefulness that comes from painting a beautiful piece.  I’m a Japanese Pakistani National and I am currently studying Masters of Arts in Business in an university in Dubai. I was always good in arts since childhood and since then I have been creating and improving my self as an artist.</p>',
            ],
            [
                'name' => 'Anna Maria Aoun',
                'name_ar' => 'آنا ماريا عون',
                'slug' => $this->generateSlug('Anna Maria Aoun'),
                'bio' => '<p>Anna Maria Aoun (b. 1987, Lebanon) is a graphic designer based in Sharjah, with a BFA in Visual Communication from the American University in Dubai (2008). She previously held a position at Leo Burnett as an art director, and worked on global brands such as Kelloggs, McDonalds and Baume et Mercier. She has also been involved in the development of the digital platform Mother Arabia based out of Beirut, Lebanon, and the brand development for MAM Foundation, Kurdistan. Anna Maria is currently working on her passion project Anna Tangles, which focuses more on monochromatic line art she creates and applying it to a series of abstract and illustrative artwork, as well as home decor and lifestyle products.</p>
<p>For more of her work please visit&nbsp;<a href="http://www.annatangles.com ">www.annatangles.com</a>.</p>
<p><br />You can also follow her creative process on instagram <strong>@anna.tangles </strong></p>',
                'bio_ar' => '<p>Anna Maria Aoun (b. 1987, Lebanon) is a graphic designer based in Sharjah, with a BFA in Visual Communication from the American University in Dubai (2008). She previously held a position at Leo Burnett as an art director, and worked on global brands such as Kelloggs, McDonalds and Baume et Mercier. She has also been involved in the development of the digital platform Mother Arabia based out of Beirut, Lebanon, and the brand development for MAM Foundation, Kurdistan. Anna Maria is currently working on her passion project Anna Tangles, which focuses more on monochromatic line art she creates and applying it to a series of abstract and illustrative artwork, as well as home decor and lifestyle products.</p>
<p>For more of her work please visit&nbsp;<a href="http://www.annatangles.com ">www.annatangles.com</a>.</p>
<p><br />You can also follow her creative process on instagram <strong>@anna.tangles </strong></p>',
            ],
        ];

        foreach($data as $item)
        {
            Artist::create($item);
        }

    }
}
