<?php

namespace Database\Seeders;

use App\Models\PageContent;
use Illuminate\Database\Seeder;

class OpenCallPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'page','section','title','title_ar','content','content_ar','type'

        $data = [
            [
                'page' => 'open-call',
                'section' => 'top-content',
                'content' => '
                    <h2 class="">Open Call:</h2>
                    <p><strong class="bluetext">Artists, performers & creative minds! <br/>
                            Register today to participate in the Al Murabbaa Art Festival!</strong></p>
                    <p>
                        Be part of the UAE’s newest art festival, Al Murabbaa. A local, 10-day showcase of diverse and collaborative art experiences, instalations, exhibitions, programmes, workshops, and more in Ajman.
                    </p>
                    <br/>
                    <h4>Are you an artist looking for new opportunities <br/>
                        or a creative with knowledge to share?</h4>
                    <p>
                        Al Murabbaa invites you to join our art festival to expand your exposure, share knowledge and exchange information with your community.
                    </p>
                    <p>
                        We welcome artists, creators, and speakers across:
                    </p>
                    <ul>
                        <li>Art</li>
                        <li>Design</li>
                        <li>Workshops</li>
                        <li>Literature</li>
                        <li>Music, theater & film / Music, performance & film</li>
                    </ul>
                    ',
                                    'content_ar' => '
                    
                    <h2 class="">دعوى عامة:</h2>
                    <p><strong class="bluetext">لجميع الفنانين والنجوم والعقول الإبداعية!<br/>
                            سجل اليوم للمشاركة في مهرجان المربعة الفني </strong></p>
                    <p>
                        كن جزءًا من أحدث مهرجان فني في الإمارات العربية المتحدة. على مدى عشرة أيام يعرض المربعة تجارب فنية متنوعة وتعاونية ومنشآت ومعارض وبرامج وورش عمل والمزيد في عجمان.
                    </p>
                    <br/>
                    <h4>إن كنتَ فنان يبحث عن فرص جديدة أو مبدع يسعى لنشر معرفته
                        سارع بالتسجيل!
                    </h4>
                    <p>
                        يدعوك مهرجان المربعة للانضمام إلى مهرجاننا الفني لتوسيع نطاقك ومشاركة معرفتك وتبادل معلومات مع مجتمعك.
                    </p>
                    <p>
                        نرحب بالفنانين والمبدعين والمتحدثين عبر:
                    </p>
                    <ul>
                        <li>	الفن</li>
                        <li>	التصميم</li>
                        <li>	ورش العمل </li>
                        <li>	الآداب </li>
                        <li>	الموسيقى والمسرح والأفلام / الموسيقى والأداء والأفلام</li>
                    </ul>',
                'type' => 'html',
            ],

        ];

        foreach ($data as $item){
            PageContent::create($item);
        }
    }
}

