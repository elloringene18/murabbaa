<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="{{ asset('fonts/webfonts/stylesheet.css') }}">
  <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
  <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('css/pages/exhibition.css') }}">
  <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animations.css') }}">
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

    @if($lang=='ar')
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600;700;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main-ar.css') }}">
    @else
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    @endif
<!--

    <link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
/>
<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
-->

  <meta name="theme-color" content="#fafafa">

    <!-- Demo styles -->
    <style>
        html,
        body {
            position: relative;
            height: 100%;
        }

        body {
            background: #eee;
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font-size: 14px;
            color: #000;
            margin: 0;
            padding: 0;
        }

        .swiper-container {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            height: 100vh;
            width: 100vh;
            background-image: url('{{asset('img/exhibition/gallery-bg.jpg')}}');
            background-size: auto 100%;
            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
            background-position-x: center;
        }
    </style>
    <style>
        .swiper-slide .box {
            width: 255px;
            height: 255px;
            background-color: #0b1684;
            color: #fff;
        }
        body.modal-open {
            overflow: visible;
        }
        #panorama {
            direction: ltr !important;
        }
        #panorama .img {
        }

    @if($lang=='ar')
        body {
            font-family: 'Cairo';
        }
        #nav {
            left: auto;
            right: 30px;
            border-right: 3px dotted #c42f66;
            border-left: 0;
        }
        #nav ul li .dot {
            margin-right: 5px;
        }

        #back {
            right: auto;
            left: 30px;
        }
        #nav-icon {
            left: auto;
            right: -21px;
        }
        .swiper-slide .imgwrap {
            margin-left: 0;
            margin-right: 20%;
        }
        .short-details {
            position: absolute;
            top: 99%;
            left: auto;
            right: 0;
            text-align: right;
            width: 100%;
            padding-left: 0;
            padding-right: 40px;
            background-position: right center;
        }
        .artmodal .contact {
            text-align: right;
        }
        body {
            direction: rtl;
        }

        #scroller {
            margin-left: 0;
            margin-right: -23px;
        }

        #nav ul li a {
            margin-left: 0;
            margin-right: 53px;
            float: right;
        }

        @endif

        /* width */
        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #fff;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #c22861;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            margin: 0 5px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #c22861;
        }
    </style>
</head>

<body>

    @inject('contentService', 'App\Services\ContentProvider')
    <?php $artistOfMonth = $contentService->getArtistOfTheMonth(\Carbon\Carbon::now(),$lang,100); ?>

    @include('partials.modal')

    <div id="instructions">
        <div class="message">
            <img src="{{ asset('img/exhibition/drag-icon.png') }}">
            <p>Drag to see more</p>
        </div>
    </div>

    <a href="{{ $lang=='ar' ? url('/ar') : url('/') }}" id="back">{{ $lang == 'ar' ? 'العودة للموقع' : 'Back to website' }}</a>
    <section id="" class="news-block">
        <div class="container-fluid">
            <div class="row">

                <!-- Swiper -->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @if(count($artistOfMonth['featured_artworks']))
                            @foreach($artistOfMonth['featured_artworks'] as $product)
                                <div class="swiper-slide fade-in">
                                    <div class="imgwrap">
                                        <a href="#" class="productLink" data-id="{{ $product['id'] }}">
                                            <img src="{{ $product->thumbnailUrl }}">
                                            <div class="short-details">
                                                <div class="content">
                                                    <span class="title">{{ $lang=='en' ? $product['title'] : $product['title_ar'] }}</span>
                                                    <span class="subs">{{ $lang == 'en' ? $product['category']->name :$product['category']->name_ar }}, {{ $product['year'] }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            @foreach($artistOfMonth['artworks'] as $product)
                                <div class="swiper-slide fade-in">
                                    <div class="imgwrap">
                                        <a href="#" class="productLink" data-id="{{ $product['id'] }}">
                                            <img src="{{ asset(''.$product['photo']) }}">
                                            <div class="short-details">
                                                <div class="content">
                                                    <span class="title">{{ $lang=='en' ? $product['title'] : $product['title_ar'] }}</span>
                                                    <span class="subs">{{ $lang == 'en' ? $product['category']->name :$product['category']->name_ar }}, {{ $product['year'] }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- Add Scrollbar -->
                    <div class="swiper-scrollbar"></div>
                </div>

                <div id="nav" class="">
                    <h1>{{ $lang=='ar' ? 'أهلا بك' : 'Welcome' }}</h1>
                    <p>{{ $lang=='ar' ? 'فنان الشهر:' : 'Artist of the month: '}}<br/>
                        <a style="color:#c42f66" href="{{ url('artists/'.$artistOfMonth['slug']) }}">{{ $artistOfMonth['name'] }}</a>
                    </p>
                    <a href="#" id="mobileMenu" class=""></a>
                    <div id="scroller">
                        <ul>
                            <?php $counter = 0; ?>

                            @if(count($artistOfMonth['featured_artworks']))
                                @foreach($artistOfMonth['featured_artworks'] as $product)
                                    <?php $counter++; ?>
                                    <li><div class="dot slide-in-left">{{ $counter }}<span></span></div><a class="slide-in-left {{ $counter == 1 ? 'active' : '' }}" href="#" data-id="{{ $counter }}">{{ $lang=='ar' ? $product['title_ar'] : $product['title']  }}</a></li>
                                @endforeach
                            @else
                                @foreach($artistOfMonth['artworks'] as $product)
                                    <?php $counter++; ?>
                                    <li><div class="dot slide-in-left">{{ $counter }}<span></span></div><a class="slide-in-left {{ $counter == 1 ? 'active' : '' }}" href="#" data-id="{{ $counter }}">{{ $lang=='ar' ? $product['title_ar'] : $product['title'] }}</a></li>
                                @endforeach
                            @endif

                        </ul>
                    </div>
                    <img src="{{ asset('img/exhibition/nav-icon.png') }}" id="nav-icon">
                </div>
            </div>
        </div>
    </section>

  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('js/vendor/modernizr-3.11.2.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/exhibition.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>

  <script>
      var baseUrl = "{{url('/')}}";
      var siteLang = "{{$lang}}";
  </script>
  <script src="{{ asset('js/main.js') }}"></script>


@if($lang=='ar')
        <script>
        $( window ).on('load',function() {
            console.log('s');
            setTimeout(function(){
                $('#nav li a').first().trigger('click');
            },300);
        });
    </script>
@endif

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 0,
            freeMode: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            scrollbar: {
                el: '.swiper-scrollbar',
                hide: true,
            },
        });
    </script>

    <script>
        var baseUrl = "{{url('/')}}";
        var siteLang = "{{$lang}}";

        function checkVisability() {
            var row = $('.animate');
            row.each(function(){
                if ($(this).inView()) {
                    $(this).addClass($(this).attr('data-animation'));
                } else {

                }
            });

            var row = $('.animate-only');
            row.each(function(){
                if ($(this).inView()) {
                    $(this).addClass($(this).attr('data-animation'));
                } else {

                }
            });
        }
        checkVisability();

        $(window).scroll(function() {
            checkVisability();
        });
    </script>
  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<!--
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
-->
</body>

</html>
