@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <style>
        .item {
            margin-bottom: 20px;
            min-height: 250px;
        }
        .title {
            font-size: 20px !important;
            text-transform: capitalize;
        }
        .details {
            border-top: 1px solid #ccc;
            margin-top: 10px;
            padding-top: 10px
        }
        .border-gray-300 {
            border-color: #ccc !important;
            color: #ccc !important;
        }
        a.border, .backlink {
            border-color: #c42f66 !important;
        }
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section id="" class="news-block pt-5 pb-5">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1 class="header-line-bot animate" data-animation="slide-in-left">Ajman Touring<span></span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-3 item">
                        <a href="{{ url('/artists/qais-salman-abood') }}">
                            <img src="{{ asset("artworks/public/Qais Salman Abood/Duu'a-thumb.jpg") }}" width="100%">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="left-line"></span>
                                    <div class="details">
                                        <h5 class="title">Qais Salman Abood</h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
@endsection()

