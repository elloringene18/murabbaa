@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/community.css') }}">
@endsection

@section('content')

<body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 animate" data-animation="slide-in-left">
                    <h1 class="header-line-bot mt-4">{{ $lang=='ar' ? 'أخبار المجتمع' : 'Community News' }}<span></span></h1>
                </div>
            </div>
            <div class="row news animate" data-animation="slide-in-left-2">
                <?php for($x=0;$x<9;$x++){ ?>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="block">
                        <span class="type">{{ $lang=='ar' ? 'المعارض' : 'News' }}</span>
                        <a href="{{ url('article') }}"><img src="{{ asset('img/news/blank.jpg') }}" width="100%"></a>
                        <div class="row">
                            <div class="col-md-6 text-right">
                                <h4 class="title">{{ $lang=='ar' ? 'لوريم ايبسوم دولار سيت أميت' : 'Sed do eiusmod tempor incididunt ut labore' }}</h4>
                                <p class="date">{{ $lang=='ar' ? '17 سبتمبر 2018.' : 'September 17, 2018.' }}</p>
                            </div>
                            <div class="col-md-6 text-left">
                                <p class="details">{{ $lang=='ar' ? 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور
أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم
أدايبا.' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.' }}</p>
                                <a href="{{ url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                            </div>
                        </div>
                        <span class="line"></span>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row animate" data-animation="slide-in-left-2">
                <div class="col-md-12">
                        <a href="#" class="pink-cta float-right mt-3 mb-5">{{ $lang=='ar' ? 'اظهار الكل' : 'Load More' }}</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="pb-5 animate" data-animation="slide-in-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="header-line-bot mt-4">{{ $lang=='ar' ? 'انضم إلى مجتمعنا' : 'Join our community' }}<span></span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form id="subscribeForm">
                        <input type="email" placeholder="{{ $lang=='ar' ? 'الاسم' : 'Email' }}">
                        <input type="submit" value="{{ $lang=='ar' ? 'الإشتراك' : 'Subscribe' }}">
                    </form>
                </div>
            </div>
        </div>
    </section>
    
@endsection()
