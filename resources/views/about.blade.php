@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $administrators = $contentService->getAdministrators($lang); ?>
<?php $pageContent = $contentService->getPageContent('about',$lang); ?>
<?php $sponsors = $contentService->getSponsors('strategic',$lang); ?>
<?php $titles = $contentService->getSponsors('title',$lang); ?>
<?php $golds = $contentService->getSponsors('gold',$lang); ?>
<?php $bronzes = $contentService->getSponsors('bronze',$lang); ?>
<?php $hotels = $contentService->getSponsors('hotel',$lang); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <style>
        .swiper-container {
            width: 100%;
        }

        .swiper-button-next.swiper-button-disabled, .swiper-button-prev.swiper-button-disabled {
            display: none;
        }

        .toggle-navs {
            list-style:none;
            padding: 0;
        }

        .toggle-nav {
            height: 65px;
            border-bottom: 1px solid #c42f66;
            float: left;
            width: 100%;
            display: block;
            line-height: 65px;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            position: relative;
        }
        .toggle-nav.active:after {
            border-color: #c42f66  transparent transparent transparent;
        }

        .toggle-nav a {
            color: #c42f66;
        }

        .toggle-nav:hover a {
            color: #0b1684;
        }

        .toggle-nav.active a {
            color: #0b1684;
        }

        .toggle-content {
            display: none;
            margin-top: 16px;
        }

        .toggle-content strong {
            color: #c42f66;
        }

        .toggle-content.active {
            display: block;
        }

        .toggle-nav:after {
            position: absolute;
            content: "";
            top: 30px;
            right: 0;
            width: 0;
            height: 0;
            border: 6px solid transparent;
            border-color: #0b1684  transparent transparent transparent;
        }

        .toggle-nav.active:after {
            right: -5px;
            border-color: transparent transparent transparent #c42f66;
        }

    </style>

    @if($lang=='ar')
        <style>
            .toggle-nav:after {
                left: 10px;
                right: auto !important;
            }
        </style>
    @endif
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section id="about">
        <div class="container animate" data-animation="slide-in-bottom">
            <div class="row">
                <div class="col-md-12 pt-5 pb-4">
                    <h3 class="header-line-bot mt-5">{{ $pageContent['welcome-title'] }}<span></span></h3>
                    {!! $pageContent['welcome-content'] !!}
                </div>
            </div>
        </div>
    </section>

    <div class="toggle-wrap mt-2 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <ul class="toggle-navs">
                        <li class="toggle-nav active" data-target="#mission-vision"><a href="#mission-vision">{{ $lang == 'ar' ? 'الرؤية و الرسالة' : 'Our Mission & Vision' }}</a></li>
                        <li class="toggle-nav" data-target="#our-values"><a href="#our-values">{{ $lang == 'ar' ? 'قيمنا' : 'Our Values' }}</a></li>
                        <li class="toggle-nav" data-target="#our-team"><a href="#our-team">{{ $lang == 'ar' ? 'فريقنا' : 'Our Team' }}</a></li>
                        <li class="toggle-nav" data-target="#partners"><a href="#partners">{{ $lang == 'ar' ? 'شركاؤنا الرسميون' : 'Our Partners' }}</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="toggle-content active" data-target="#mission-vision">
                        <section class="pb-5" id="mission-vision">
                            <div class="container">
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12 " data-animation="slide-in-left">--}}
                                        {{--<h1 class="header-line-bot">{{ $pageContent['mission-title'] }}<span></span></h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="row " data-animation="slide-in-right">
                                    {{--<div class="col-md-3">--}}
                                    {{--<img src="{{ asset(''.$pageContent['mission-photo']) }}" width="100%">--}}
                                    {{--</div>--}}
                                    <div class="col-md-12">
                                        {!! $pageContent['mission-content'] !!}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="toggle-content" data-target="#our-values">
                        <div class="container">
                            <div class="row " data-animation="slide-in-right">
                                {{--<div class="col-md-3">--}}
                                {{--<img src="{{ asset(''.$pageContent['mission-photo']) }}" width="100%">--}}
                                {{--</div>--}}
                                <div class="col-md-12">
                                    {!! $pageContent['values-content'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="toggle-content" data-target="#our-team">
                        @if(count($administrators))
                            <section class="pb-5">
                                <div class="container">
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12 " data-animation="slide-in-left">--}}
                                            {{--<h1 class="header-line-bot">{{ $pageContent['administration-carousel-title'] }}<span></span></h1>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="row " data-animation="slide-in-bottom">
                                        <?php $odd = false; ?>
                                        @foreach($administrators as $item)
                                            <div class="col-md-3 {{ $odd ? 'pt-5' : '' }}">
                                                <img src="{{ $item['photo'] }}" width="100%">
                                                <h3 class="pt-2 bluetext">{{ $item['name'] }}</h3>
                                                <p>{{ $item['title'] }}</p>
                                            </div>
                                            @if($odd)
                                                <?php $odd = false; ?>
                                            @else
                                                <?php $odd = true; ?>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @endif
                    </div>
                    <div class="toggle-content" data-target="#partners">
                        <section class="pb-5" id="partners">
                            <div class="container">
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<h1 class="header-line-bot " data-animation="slide-in-left">--}}
                                            {{--{{ $pageContent['partner-title'] }}--}}
                                            {{--<span></span></h1>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row pb-5">--}}
                                {{--<div class="col-md-3">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-9 animate" data-animation="slide-in-right">--}}
                                {{--{!! $pageContent['partner-content'] !!}--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                @if(count($titles))
                                    <p><strong>{{ $lang=='en' ? 'Platinum Sponsor:' : 'الراعي البلاتيني:' }}</strong></p>
                                    <div class="row " data-animation="slide-in-bottom">
                                        @foreach($titles as $title)
                                            <div class="col-md-3">
                                                <img src="{{ $title['photo'] }}" width="100%">
                                            </div>
                                        @endforeach
                                    </div>
                                    <br/>
                                @endif

                                @if(count($golds))
                                    <p><strong>{{ $lang=='en' ? 'Gold Sponsor:' : 'الراعي الذهبي:' }}</strong></p>
                                    <div class="row " data-animation="slide-in-bottom">
                                    @foreach($golds as $gold)
                                        <div class="col-md-3">
                                            <img src="{{ $gold['photo'] }}" width="100%">
                                        </div>
                                    @endforeach
                                    </div>
                                    <br/>
                                @endif

                                @if(count($bronzes))
                                <p><strong>{{ $lang=='en' ? 'Bronze Sponsor:' : 'الراعي البرونزي:' }}</strong></p>
                                <div class="row " data-animation="slide-in-bottom">
                                        @foreach($bronzes as $bronze)
                                            <div class="col-md-3">
                                                <img src="{{ $bronze['photo'] }}" width="100%">
                                            </div>
                                        @endforeach

                                </div>
                                <br/>
                                @endif

                                @if(count($sponsors))
                                <p><strong>{{ $lang=='en' ? 'Strategic Partners:' : 'الشركاء الاستراتيجيين:' }}</strong></p>
                                <div class="row " data-animation="slide-in-bottom">
                                        @foreach($sponsors as $sponsor)
                                            <div class="col-md-3">
                                                <img src="{{ $sponsor['photo'] }}" width="100%">
                                            </div>
                                        @endforeach

                                </div>
                                <br/>
                                @endif

                                @if(count($hotels))
                                <p><strong>{{ $lang=='en' ? 'Hotel Partners:' : 'الفندق الشريك:' }}</strong></p>
                                <div class="row " data-animation="slide-in-bottom">
                                        @foreach($hotels as $hotel)
                                            <div class="col-md-3">
                                                <img src="{{ $hotel['photo'] }}" width="100%">
                                            </div>
                                        @endforeach
                                </div>
                                @endif
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--<section class="pt-5 pb-5" id="administrators">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12 animate" data-animation="slide-in-left">--}}
                    {{--<h1 class="header-line-bot">{{ $pageContent['administration-title'] }}<span></span></h1>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row animate" data-animation="slide-in-right">--}}
                {{--<div class="col-md-3">--}}
                {{--</div>--}}
                {{--<div class="col-md-9">--}}
                    {{--{!! $pageContent['administration-content'] !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}





@endsection()

@section('js')
    <script>
        $('.toggle-nav').on('click',function(){
            $('.toggle-nav').removeClass('active');
            $(this).addClass('active');

            $('.toggle-content').removeClass('active');
            $('.toggle-content').eq($(this).index('.toggle-nav')).addClass('active');
        });

        cTarget = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
        cTarget = '#'+cTarget.slice(cTarget.lastIndexOf('#') + 1);


        if($("li[data-target='"+cTarget+"']").length){
            $('.toggle-nav').removeClass('active');
            $('.toggle-content').removeClass('active');
            $("li[data-target='"+cTarget+"']").each(function(){ $(this).addClass('active'); });
            $("div[data-target='"+cTarget+"']").each(function(){ $(this).addClass('active'); });
        }
    </script>
@endsection
