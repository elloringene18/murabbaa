@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <style>
        .swiper-container {
            width: 100%;
        }

        .swiper-button-next.swiper-button-disabled, .swiper-button-prev.swiper-button-disabled {
            display: none;
        }

        .toggle-navs {
            list-style:none;
            padding: 0;
        }

        .toggle-nav {
            height: 65px;
            border-bottom: 1px solid #c42f66;
            float: left;
            width: 100%;
            display: block;
            line-height: 65px;
            text-transform: uppercase;
            font-weight: bold;
            cursor: pointer;
            position: relative;
        }
        .toggle-nav.active:after {
            border-color: #c42f66  transparent transparent transparent;
        }

        .toggle-nav a {
            color: #c42f66;
        }

        .toggle-nav:hover a {
            color: #0b1684;
        }

        .toggle-nav.active a {
            color: #0b1684;
        }

        .toggle-content {
            display: none;
            margin-top: 16px;
        }

        .toggle-content strong {
            color: #c42f66;
        }

        .toggle-content.active {
            display: block;
        }

        .toggle-nav:after {
            position: absolute;
            content: "";
            top: 30px;
            right: 0;
            width: 0;
            height: 0;
            border: 6px solid transparent;
            border-color: #0b1684  transparent transparent transparent;
        }

        .toggle-nav.active:after {
            right: -5px;
            border-color: transparent transparent transparent #c42f66;
        }

    </style>

    @if($lang=='ar')
        <style>
            .toggle-nav:after {
                left: 10px;
                right: auto !important;
            }
        </style>
    @endif
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section id="about">
        <div class="container animate" data-animation="slide-in-bottom">
            <div class="row">
                <div class="col-md-12 pt-5 pb-4">
                    <h3 class="header-line-bot mt-5">{{ $lang == 'ar' ? 'البرامج' : 'Programs' }}<span></span></h3>
                </div>
            </div>
        </div>
    </section>

    <div class="toggle-wrap mt-2 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <ul class="toggle-navs">
                        <li class="toggle-nav active" data-target="#workshops"><a href="#workshops">{{ $lang == 'ar' ? 'ورش عمل' : 'Workshops' }}</a></li>
                        <li class="toggle-nav" data-target="#stages"><a href="#stages">{{ $lang == 'ar' ? 'المسرح' : 'Stage' }}</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="toggle-content active" data-target="#workshops">
                        <section class="pb-5" id="workshops">
                            <div class="container">
                                <img src="{{ asset('img/programs/V5/workshops-'.$lang.'.png')}}" width="100%">
                            </div>
                        </section>
                    </div>
                    <div class="toggle-content" data-target="#stages">
                        <section class="pb-5" id="stages">
                            <div class="container">
                                <img src="{{ asset('img/programs/V5/stages-'.$lang.'.png')}}" width="100%">
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @endsection()

    @section('js')
        <script>
            $('.toggle-nav').on('click',function(e){
                e.preventDefault();
                $('.toggle-nav').removeClass('active');
                $(this).addClass('active');

                $('.toggle-content').removeClass('active');
                $('.toggle-content').eq($(this).index('.toggle-nav')).addClass('active');
            });

            cTarget = window.location.href.slice(window.location.href.lastIndexOf('/') + 1);
            cTarget = '#'+cTarget.slice(cTarget.lastIndexOf('#') + 1);


            if($("li[data-target='"+cTarget+"']").length){
                $('.toggle-nav').removeClass('active');
                $('.toggle-content').removeClass('active');
                $("li[data-target='"+cTarget+"']").each(function(){ $(this).addClass('active'); });
                $("div[data-target='"+cTarget+"']").each(function(){ $(this).addClass('active'); });
            }
        </script>
@endsection
