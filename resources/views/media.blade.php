@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/calendar.css') }}">

    @if($lang=='ar')
        <style>
            .border {
                float: right;
            }
        </style>
    @endif
@endsection

@section('content')

<body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')
    
    <section class="pb-5">
        <div class="container calendarList">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="header-line-bot mt-4">{{ $lang=='ar' ? 'الإعلام والصحافة' : 'Media and Press' }}<span></span></h1>
                </div>
            </div>
            
            {{--<div class="row">--}}
                {{--<div class="col-md-8 pb-3">--}}
                    {{--<div class="custom-select">--}}
                        {{--<select id="eventfilter" name="">--}}
                            {{--<option value="" disabled selected>{{ $lang == 'ar' ? 'Select Media Type' : 'Select Media Type' }}</option>--}}
                            {{--@foreach($types as $type)--}}

                                {{--@if($cat)--}}
                                    {{--<option value="{{ $type['slug'] }}" {{ $type['slug'] == $cat->slug ? 'selected' : '' }}>{{ $type['title'] }}</option>--}}
                                {{--@else--}}
                                    {{--<option value="{{ $type['slug'] }}">{{ $type['title'] }}</option>--}}
                                {{--@endif--}}

                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 pb-3">--}}
                    {{--<hr/>--}}
                {{--</div>--}}
            {{--</div>--}}

            @if(count($data))
                @foreach($data as $item)
                    <div class="row pb-3 animate" data-animation="slide-in-left">
                        <div class="col-md-2">
                            <img src="{{ $item['thumbnail'] }}" width="100%">
                        </div>
                        <div class="col-md-5 cols">
                            <h5 class="vcenter">{{ $item['title'] }}</h5>
                        </div>
                        <div class="col-md-3 cols text-center">
                            <p class="vcenter" style="margin: 0 20px;">{{ $item['date'] }}</p>
                        </div>
                        <div class="col-md-2 cols">
                            <a href="{{  $lang=='en' ? url('/media/p/'.$item['slug']) : url('ar/media/p/'.$item['slug']) }}" class="vcenter redLineBt">{{ $lang=='ar' ? 'عرض' : 'VIEW' }}</a>
                        </div>
                        <div class="col-md-12 pt-3">
                            <hr/>
                        </div>
                    </div>
                @endforeach

               {{--  <div class="row pagntin">
                    <div class="col-md-6 text-left">
                        {{ $results->links() }}
                    </div>
                    <div class="col-md-6 text-right">

                        @if($lang=='ar')
                            إظهار {{($results->currentpage()-1)*$results->perpage()+1}} إلى {{ ($results->currentpage()*$results->perpage()) <= $results->total() ? $results->currentpage()*$results->perpage() : $results->total() }} نتائج

                        @else
                            Showing {{($results->currentpage()-1)*$results->perpage()+1}} to {{ ($results->currentpage()*$results->perpage()) <= $results->total() ? $results->currentpage()*$results->perpage() : $results->total() }}
                            of  {{$results->total()}} entries
                        @endif

                    </div>
                </div> --}}
            @else
                @if($lang=='ar')
                    <p>No articles under this category.</p>
                @else
                    <p>No articles under this category.</p>
                @endif

            @endif
            {{--<div class="row">--}}
                {{--<div class="col-md-12 pb-3 pt-5 animate" data-animation="slide-in-left">--}}
                    {{--<h3 class="bluetext">{{ $lang=='ar' ? 'المعارض السابقة' : 'Previous Exhibitions' }}</h3>--}}
                    {{--<hr/>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row pb-3 animate" data-animation="slide-in-right">--}}
                {{--<div class="col-md-2">--}}
                    {{--<img src="{{ asset('img/placeholder-event.jpg') }}" width="100%">--}}
                {{--</div>--}}
                {{--<div class="col-md-5 cols">--}}
                    {{--<h5 class="vcenter">{{ $lang=='ar' ? 'لقاء ومعرض فن دبي' : '“Reviving al Sadu” Workshop by the Artist Chan Seth' }}</h5>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 cols">--}}
                    {{--<p class="vcenter">{{ $lang=='ar' ? 'انتهى المعرض' : 'Event Ended' }}</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 pt-3">--}}
                    {{--<hr/>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row pb-3 animate" data-animation="slide-in-left">--}}
                {{--<div class="col-md-2">--}}
                    {{--<img src="{{ asset('img/placeholder-event.jpg') }}" width="100%">--}}
                {{--</div>--}}
                {{--<div class="col-md-5 cols">--}}
                    {{--<h5 class="vcenter">{{ $lang=='ar' ? 'لقاء ومعرض فن دبي' : '“Reviving al Sadu” Workshop by the Artist Chan Seth' }}</h5>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 cols">--}}
                    {{--<p class="vcenter">{{ $lang=='ar' ? 'انتهى المعرض' : 'Event Ended' }}</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 pt-3">--}}
                    {{--<hr/>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row pb-3 animate" data-animation="slide-in-right">--}}
                {{--<div class="col-md-2">--}}
                    {{--<img src="{{ asset('img/placeholder-event.jpg') }}" width="100%">--}}
                {{--</div>--}}
                {{--<div class="col-md-5 cols">--}}
                    {{--<h5 class="vcenter">{{ $lang=='ar' ? 'لقاء ومعرض فن دبي' : '“Reviving al Sadu” Workshop by the Artist Chan Seth' }}</h5>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 cols">--}}
                    {{--<p class="vcenter">{{ $lang=='ar' ? 'انتهى المعرض' : 'Event Ended' }}</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 pt-3">--}}
                    {{--<hr/>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>
    
@endsection()

@section('js')
    <script>
        $('#eventfilter').on('change',function(){
            if(siteLang=='en')
                window.location = baseUrl+'/media/'+$(this).val();
            else
                window.location = baseUrl+'/ar/media/'+$(this).val();
        });
    </script>
@endsection
