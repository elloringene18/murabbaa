@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageContent('al-murabba-festival',$lang); ?>
<?php $festivalItems = $contentService->getFestivalItems($lang); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    @if($lang=='ar')
        <style>
            .box-list li {
                float: right;
            }
            .box-list li ul li a {
                font-size: 13px;
                text-align: right;
            }
            .box-list li a {
                float: right;
            }
            #subscribeForm input[type="submit"] {
                float: left;
            }
        </style>
    @endif

    <style>
        .inner h1, .inner h2, .inner h3, .inner h4, .inner h5 {
            color: #c42f66 !important;
            font-weight: bold;
            line-height: 1.1 !important;
        }

        .inner .step-gallery .sub {
            font-size: 18px;
            color: #0b1684;
        }

        .inner p strong {
            color: #c42f66;
        }


        .contents h1, .contents h2,.contents h3,.contents h4,.contents h5,.contents h6 {
            margin-bottom: 40px;
            position: relative;
            margin-top: 20px;
        }

        .contents h1:after, .contents h2:after,.contents h3:after,.contents h4:after,.contents h5:after,.contents h6:after {
            content: '';
            display: block;
            position: absolute;
            width: 60px;
            height: 3px;
            background-color: #c42f66;
            bottom: -20px;
        }

        #main-featured-art.inner {
            background-image: url('{{ asset('img/home/festival.jpg') }}');
        }
    </style>
@endsection


@section('content')
    <body class="inner">

    @include('partials.modal')
    @include('partials.menu')
    @include('partials.featured-nav')

        <div class="container animate slide-in-bottom" data-animation="slide-in-bottom">
        <div class="row">
            <div class="col-md-12 pt-5 pb-4 contents">
                {!! $pageContent['top-content'] !!}
            </div>
            </div>
            <div class="row step-gallery animate" data-animation="slide-in-bottom">
                @foreach($festivalItems as $item)
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 cols">
                    <img src="{{ asset(''.$item['thumbnail']) }}">
                    <h3 class="title">{{ $item['title'] }}</h3>
                    <span class="sub">{{ $item['content'] }}</span>
                </div>
                @endforeach
            </div>
        </div>


    <section class="pb-5 animate" data-animation="slide-in-bottom" id="sponsors">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gallery">
                        <div class=""><img src="{{ asset('img/festival/fest11.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest12.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest13.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest14.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest15.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest16.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest17.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest18.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest19.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest20.jpg') }}" width="99%"></div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <section class="pb-5 animate" data-animation="slide-in-bottom" id="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('public/img/festival/map-'.$lang.'.jpg') }}" width="100%">
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    <section class="pb-5 animate" data-animation="slide-in-bottom" id="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="header-line-bot mt-4">{{ $lang=='ar' ? 'أتريد معرفة المزيد؟' : 'Want to know more?' }}<span></span></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="pink-underline">{{ $lang=='ar' ? 'حمِّل الكتيب الخاص بنا وجد كل ما تريد معرفته. ' : 'Find everything you need to know by downloading our brochure. ' }}</p>
                    <a href="{{ url('public/'.$pageContent['pdf-file']) }}" target="_blank" class="download-bt float-right">{{ $lang=='ar' ? 'زر تحميل' : 'DOWNLOAD' }}</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    @endsection()

    @section('js')
        <script type="text/javascript">
            
        $('.gallery').slick({
            dots: true,
            infinite: false,
            speed: 500,
            @if($lang=='ar')
                rtl: true,
            @endif
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
        </script>
    @endsection



