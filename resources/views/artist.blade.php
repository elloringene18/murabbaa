@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/catalog.css') }}">
    <style>
        .artist-items {
            overflow: hidden;
        }
        @if($lang=='ar')
            .catalog .left-line {
                float: right;
            }
            .catalog .details {
                padding-right: 20px;
            }
        @endif

        @if($artist['banner'])
            #main-featured-art {
                background-image: url({{ $artist['banner'] }}) !important;
            }
        @endif
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.modal')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5">
                    <a class="backlink" href="{{ $lang=='en'? url('artists') : url('ar/artists') }}">
                        <strong>{{ $lang == "en" ? '< Back to artists page' : 'العودة لصفحة الفنانون' }}</strong>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 animate" data-animation="slide-in-left">
                    <h1 class="header-line-bot mt-5">{{ $artist['name'] }}<span></span></h1>
                </div>
            </div>
            <div class="row animate" data-animation="slide-in-right">
                <div class="col-md-3">
                    <img src="{{ $artist['photo'] }}" width="100%">
                </div>
                <div class="col-md-9">
                    {!! $artist['bio'] !!}
                </div>
            </div>
        </div>
    </section>


    @if(count($artist['products']))
        <section class="pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 animate" data-animation="slide-in-left">
                        <h1 class="header-line-bot mt-5">{{$lang=="en" ? 'Artworks' : 'عمل فني'}}<span></span></h1>
                    </div>
                </div>
                <div class="row catalog">
                        @foreach($artist['products'] as $product)
                            <div class="col-md-3">
                                <a href="#" class="productLink" data-id="{{$product['id']}}">
                                    <img src="{{ asset(''.$product['photo']) }}" width="100%">
                                </a>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="left-line"></span>
                                        <div class="details">
                                            <h5 class="title">{{ $lang == 'en' ? $product['title'] : $product['title_ar'] }}</h5>
                                            <p class="sub">{{ $lang == 'en' ? $product['category']->name : $product['category']->name_ar }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                </div>
            </div>
        </section>
    @endif

    @if(count($artist['collections']))
        <section class="pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 animate" data-animation="slide-in-left">
                        <h1 class="header-line-bot mt-5">{{ $lang == 'en' ? 'Artistic Collections' : 'مجموعات فنية' }}<span></span></h1>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="artist-items" id="video-swiper">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                @foreach($artist['collections'] as $collection)
                                    <div class="swiper-slide">
                                        @if($collection['type']=="photo")
                                            <img src="{{ asset(''.$collection->path) }}">
                                        @elseif($collection['type']=="video")
                                            <video width="100%" controls>
                                                <source src="{{ asset(''.$collection->path) }}" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection()

@section('js')
    <script>
        var mySwiper = new Swiper('.artist-items', {
            // Optional parameters
            slidesPerView: 2,
            spaceBetween: 30,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                1: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
            }
        })
    </script>
@endsection
