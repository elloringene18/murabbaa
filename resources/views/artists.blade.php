@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <style>
        .item {
            margin-bottom: 20px;
            min-height: 250px;
        }
        .title {
            font-size: 20px !important;
            text-transform: capitalize;
        }
        .details {
            border-top: 1px solid #ccc;
            margin-top: 10px;
            padding-top: 10px
        }
        .border-gray-300 {
            border-color: #ccc !important;
            color: #ccc !important;
        }
        a.border, .backlink {
            border-color: #c42f66 !important;
        }
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section id="" class="news-block pt-5 pb-5">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1 class="header-line-bot animate" data-animation="slide-in-left">{{ $lang=='ar' ? 'فنان' : 'Artists' }} <span></span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        @foreach($artists as $item)
                            <div class="col-md-3 item">
                                <a href="{{ $lang == 'en' ? url('/artist-gallery/'.$item['slug']) : url('ar/artist-gallery/'.$item['slug'])}}">
                                    <img src="{{ $item['photo'] }}" width="100%">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="left-line"></span>
                                        <div class="details">
                                            <h5 class="title">{{ $item['name'] }}</h5>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        @endforeach

                        <div class="col-md-12"><hr/></div>
                        <div class="col-md-12 pagntin">
                            <div class="row">
                                <div class="col-md-6 paginations">
                                    {{ $data->links() }}
                                </div>
                                <div class="col-md-6 text-right">
                                    @if($lang=='ar')
                                        إظهار {{($data->currentpage()-1)*$data->perpage()+1}} إلى {{ ($data->currentpage()*$data->perpage()) <= $data->total() ? $data->currentpage()*$data->perpage() : $data->total() }} نتائج

                                    @else
                                        Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{ ($data->currentpage()*$data->perpage()) <= $data->total() ? $data->currentpage()*$data->perpage() : $data->total() }}
                                        of  {{$data->total()}} entries
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection()

@section('js')
@endsection()

