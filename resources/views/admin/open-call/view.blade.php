@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Inquiry</h5><br/>
                            <a href="{{ url('admin/open-call') }}">< Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    @foreach($data as $key=>$item)
                                        <tr>
                                            <td>{{ $key }}</td>
                                            <td>
                                                @if($key=='portfolio')
                                                    <a href="{{ asset(''.$item) }}" target="_blank">{{ asset(''.$item) }}</a>
                                                @else
                                                    {{ $item }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
