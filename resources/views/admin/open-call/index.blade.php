@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Open Call Submissions</h5><br/>
                            <a href="{{ url('admin/open-call/export') }}">Export to XSL</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Email</th>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th onclick="sortTable(0)">Category</th>
                                        <th onclick="sortTable(0)">Date</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item['email'] }}</td>
                                            <td>{{ $item['name'] }}</td>
                                            <td>{{ isset($item['category']) ? $item['category'] : '' }}</td>
                                            <td>
                                                <a href="{{ URL('admin/open-call/view/'.$item['id']) }}">View</a>
                                                |
                                                <a href="{{ URL('admin/open-call/delete/'.$item['id']) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                            {{ $results->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
