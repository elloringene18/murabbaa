@extends('admin.partials.master')

@section('css')
    <style>
        .item {
            padding: 30px 30px;
            border: 1px solid #ccc;
            margin-bottom: 20px;
        }

        .form-control {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Page Content</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/al-murabba-festival/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @foreach($data as $item)
                                    @if($item->type=='text')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputNamea1">{{ $item->section }} EN</label>
                                                    <input value="{{$item->content}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Content" name="section[{{$item->id}}][content]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputNamea1">{{ $item->section }} AR</label>
                                                    <input value="{{$item->content_ar}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Content Ar" name="section[{{$item->id}}][content_ar]">
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($item->type=='html')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail3">{{ $item->section }} EN</label>
                                                    <input type="hidden" name="section[{{$item->id}}][content]" value="{{ $item->content }}"/>
                                                    <div class="summernote">
                                                        {!! $item->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail3">{{ $item->section }} AR</label>
                                                    <input type="hidden" name="section[{{$item->id}}][content_ar]" value="{{ $item->content_ar }}"/>
                                                    <div class="summernote">
                                                        {!! $item->content_ar !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($item->type=='code')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail3">{{ $item->section }} EN</label>
                                                    <textarea class="form-control" name="section[{{$item->id}}][content]" rows="30">
                                                            {!! $item->content !!}
                                                        </textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail3">{{ $item->section }} AR</label>
                                                    <textarea class="form-control" name="section[{{$item->id}}][content_ar]" rows="30">
                                                            {!! $item->content_ar !!}
                                                        </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($item->type=='image')
                                        <div class="row">
                                            <div class="col-md-12 grid-margin stretch-card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label>{{ $item->section }}</label>
                                                            @if($item->content)
                                                                <br/>
                                                                <img src="{{asset(''.$item->content)}}" width="200">
                                                                <br/>
                                                                <br/>
                                                            @endif
                                                            <input type="file" class="form-control" name="section[{{$item->id}}][content]" data-lang="en">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5>Festival Highlights</h5>
                                        <br/>
                                    </div>
                                </div>
                                <div class="row">
                                    @foreach($festivalItems as $index=>$item)
                                        <div class="col-md-6 item">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <img src="{{ asset(''.$item->thumbnail) }}" width="200">
                                                    <br/>
                                                    <label>Thumbnail (255x255)</label>
                                                    <input type="file" class="form-control" name="items[{{$item->id}}][thumbnail]" accept="image/*">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Title</label>
                                                    <input type="text" class="form-control" name="items[{{$item->id}}][title]" value="{{ $item->title }}">
                                                    <label>Description</label>
                                                    <textarea class="form-control" name="items[{{$item->id}}][content]" rows="5">{{ $item->content }}</textarea>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Title AR</label>
                                                    <input type="text" class="form-control" name="items[{{$item->id}}][title_ar]" value="{{ $item->title_ar }}">
                                                    <label>Description AR</label>
                                                    <textarea class="form-control" name="items[{{$item->id}}][content_ar]" rows="5">{{ $item->content_ar }}</textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{ url('admin/al-murabba-festival/delete/'.$item->id) }}">Delete highlight</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="col-md-6 item">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br/>
                                                <h5>Add a new highlight:</h5>
                                                <br/>
                                                <label>Thumbnail (255x255)</label>
                                                <input type="file" class="form-control" name="newitem[thumbnail]" accept="image/*">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Title</label>
                                                <input type="text" class="form-control" name="newitem[title]">
                                                <label>Description</label>
                                                <textarea class="form-control" name="newitem[content]"></textarea>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Title AR</label>
                                                <input type="text" class="form-control" name="newitem[title_ar]">
                                                <label>Description AR</label>
                                                <textarea class="form-control" name="newitem[content_ar]"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <label>Festival Brochure</label>
                                @if($brochure->content)
                                    <br/>
                                    <a target="_blank" href="{{ url('public/'.$brochure->content) }}">Click here to Brochure</a>
                                    <br/>
                                    <br/>
                                @endif
                                <input type="file" class="form-control" name="brochure" data-lang="en" accept="application/pdf">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
@endsection
