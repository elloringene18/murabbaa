<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Artworks</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="products">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products') }}">View Artworks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products/create/') }}">Add an Artwork</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories') }}">View Categories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories/create/') }}">Add a Category</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#artists" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Artists</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="artists">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/artists') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/artists/create/') }}">Add an Artists</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#administrators" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">About Page</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="administrators">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/administrators') }}">View Team Members</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/administrators/create/') }}">Add a Team Member</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/partners') }}">View Partners</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/partners/create/') }}">Add a Partner</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/page-contents/about/') }}">Other Page Contents</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#events" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Calendar Events</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="events">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events') }}">View All Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events/create/') }}">Add an Event</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events/types') }}">Event Types</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events/types/create/') }}">Add Event Types</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#media" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Media</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="media">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/media') }}">View All Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/media/create/') }}">Add a Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/media/types') }}">Media Types</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/media/types/create/') }}">Add Media Types</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#artist-of-the-month" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Artist of the month</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="artist-of-the-month">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/artist-of-the-month') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/artist-of-the-month/create/') }}">Add an Artist of the month</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/subscribers') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Subscribers</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/contacts') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Inquiries</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#open-call" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Open Call</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="open-call">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/page-contents/open-call') }}">Page Content</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/open-call') }}">Submissions</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/al-murabba-festival') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Al Murabbaa Art Festival</span>
            </a>
        </li>

        {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#files" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Files</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="files">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/files') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/files/create/') }}">Upload a File</a>
                    </li>
                </ul>
            </div>
        </li>
         --}}
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Settings</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="settings">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/settings/home-items') }}">Home Categories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/page-contents/header/') }}">Header Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/page-contents/footer/') }}">Footer Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/page-contents/contact/') }}">Contact Settings</a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
