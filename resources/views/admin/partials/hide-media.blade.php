<div class="form-group" style="border-top:3px solid #ccc" >
    <div class="row">
        <div class="col-md-12" id="file-uploads">
            <div class="file-upload card-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Versions:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No English:</label>
                            <input type="checkbox" class="form-control" name="no_english" {{ isset($item) ? ($item->no_english ? 'checked' : false) : '' }}>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Arabic:</label>
                            <input type="checkbox" class="form-control" name="no_arabic" {{ isset($item) ? ($item->no_arabic ? 'checked' : false) : '' }}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>