@if(count($item->slides))
    <div class="form-group" style="border-top:3px solid #ccc">
    <br/>
    <strong>Images</strong>
    <br/><br/>

        <div class="row" id="current-images">
            @foreach($item->slides as $slide)
                <div class="file-upload">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('public/'.$slide->source) }}" width="100%">
                            <input type="hidden" name="uploads[{{$slide->id}}][id]" value="{{ $slide->id }}" width="100%">
                        </div>
                        <div class="col-md-6">
                            {{-- <div class="form-group">
                                <label>Image Caption EN:</label>
                                <input type="text" class="form-control" placeholder="Image Caption EN" name="upload-captions[{{$slide->id}}][EN]"  value="{{ $slide->caption }}">
                            </div>
                            <div class="form-group">
                                <label>Image Caption AR:</label>
                                <input type="text" class="form-control" placeholder="Image Caption AR" name="upload-captions[{{$slide->id}}][AR]"  value="{{ $slide->caption_ar }}">
                            </div>

                            <div class="form-group">
                                <label>Replace Image:</label>
                                <input type="file" class="form-control" name="uploads[{{$slide->id}}]" placeholder="Upload Image">
                            </div> --}}

                            <div class="form-group">
                                <label><a href="{{ url('admin/media/slides/delete/'.$slide->id) }}">Delete Slide</a></label>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif