@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit artist of the month</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/artist-of-the-month/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{$item->id}}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                        </div>
                                    </div>
                                @endif
                                    @if ($errors->any())
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Artist</strong></label>
                                            <select name="artist_id" class="form-control" id="artist">
                                                @foreach($artists as $artist)
                                                    <option value="{{$artist->id}}" {{$artist->id == $item->artist_id ? 'selected' : ''}}>{{$artist->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Featured Artwork</strong></label>
                                            <br/>
                                            <span id="featured_art_image">
                                                <img src="{{ $item->product->photoUrl }}" width="200">
                                            </span>
                                            <select name="product_id" class="form-control" id="featured_art">
                                                @foreach($artworks as $artwork)
                                                    <option value="{{$artwork->id}}" {{ $artwork->id == $item->product->id ? "selected" : "" }}>{{$artwork->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Exhibition Artworks</strong></label>
                                            <div id="artworks" class="row">

                                            </div>
                                        </div>
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Month</label>
                                            <select name="month" class="form-control">
                                                <?php
                                                $months = array(
                                                    'January',
                                                    'February',
                                                    'March',
                                                    'April',
                                                    'May',
                                                    'June',
                                                    'July ',
                                                    'August',
                                                    'September',
                                                    'October',
                                                    'November',
                                                    'December',
                                                );
                                                $x = 1;
                                                ?>
                                                @for($x==1;$x<12;$x++)
                                                    <option value="{{$months[$x-1]}}" {{ \Carbon\Carbon::parse($item->date_from)->format('F') == $months[$x-1] ? 'selected' : '' }}>{{$months[$x-1]}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Year</label>
                                            <?php $x = 2020; ?>
                                            <select name="year" class="form-control">
                                                @for($x==2020;$x<2100;$x++)
                                                    <option value="{{$x}}" {{ \Carbon\Carbon::parse($item->date_from)->format('Y') == $x ? 'selected' : '' }}>{{$x}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var artistproducts = [];
        var currentProducts = JSON.parse('{{$seclectedartworks}}');
        var currentProduct = JSON.parse('{{$seclectedartwork}}');

        function getArtistProducts(){

            $('#artworks').html('');
            $('#featured_art').html('');

            var id = $('#artist').val();
            $.ajax({
                type: "GET",
                url: baseUrl+'/api/products/get-by-artist/'+id,
                success: function(response){
                    artistproducts = [];

                    $.each( response, function( key, cat ) {
                        artistproducts[cat.id] = cat.photo;
                        $('#featured_art').append('<option value="'+cat.id+'" '+ (currentProduct == cat.id ? "selected" : "") +'>'+cat.title+'</option>');
                    });

                    $.each( response, function( key, cat ) {

                        label = '<div class="col-md-4"><br/>';
                        label += '<label><p>'+cat.title+'</p><br/><input type="checkbox" name="products[]" value="'+cat.id+'" '+ (currentProducts.includes(cat.id) ? "checked" : "") +'/>';
                        label += '<img src="'+cat.photo+'" width="200"/>';
                        label += '</label><br/>';
                        label += "</div>";

                        $('#artworks').append(label);
                    });
                },
                statusCode: {
                    // 401: function() {
                    //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
                    // }
                },
                complete : function (event,error){
                    showFeaturedArt();
                }
            });
        }
        setTimeout(function () {
            getArtistProducts();
        },1000);

        function showFeaturedArt(){
            $('#featured_art_image').html('');
            $('#featured_art_image').append('<img src="'+artistproducts[$('#featured_art').val()]+'" width="200"/>');
        }


        $('#artist').on('change',function(){
            getArtistProducts();
        });

        $('#featured_art').on('change',function(){
            showFeaturedArt();
        });
    </script>
@endsection
