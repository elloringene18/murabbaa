@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add an artist of the month</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/artist-of-the-month/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Artist</strong></label>
                                            <select name="artist_id" class="form-control" id="artist">
                                                @foreach($artists as $artist)
                                                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Featured Artwork</strong></label>
                                            <br/>
                                            <span id="featured_art_image"></span>
                                            <select name="product_id" class="form-control" id="featured_art">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1"><strong>Exhibition Artworks</strong></label>
                                            <div id="artworks" class="row">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Month</label>
                                            <select name="month" class="form-control">
                                                <?php
                                                    $months = array(
                                                        'January',
                                                        'February',
                                                        'March',
                                                        'April',
                                                        'May',
                                                        'June',
                                                        'July ',
                                                        'August',
                                                        'September',
                                                        'October',
                                                        'November',
                                                        'December',
                                                    );
                                                    $x = 1;
                                                ?>
                                                @for($x==1;$x<=12;$x++)
                                                    <option value="{{$months[$x-1]}}">{{$months[$x-1]}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Year</label>
                                            <?php $x = 2020; ?>
                                            <select name="year" class="form-control">
                                                @for($x==2020;$x<2100;$x++)
                                                    <option value="{{$x}}">{{$x}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var artistproducts = [];

        function getArtistProducts(){

            $('#artworks').html('');
            $('#featured_art').html('');

            var id = $('#artist').val();
            $.ajax({
                type: "GET",
                url: baseUrl+'/api/products/get-by-artist/'+id,
                success: function(response){
                    artistproducts = [];

                    $.each( response, function( key, cat ) {

                        artistproducts[cat.id] = cat.photo;

                        $('#featured_art').append('<option value="'+cat.id+'">'+cat.title+'</option>');
                    });

                    $.each( response, function( key, cat ) {

                        label = '<div class="col-md-4"><br/>';
                        label += '<label><p>'+cat.title+'</p><br/><input type="checkbox" name="products[]" value="'+cat.id+'"/>';
                        label += '<img src="'+cat.photo+'" width="200"/>';
                        label += '</label><br/>';
                        label += "</div>";

                        $('#artworks').append(label);
                    });
                },
                statusCode: {
                    // 401: function() {
                    //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
                    // }
                },
                complete : function (event,error){
                    showFeaturedArt();
                }
            });
        }
        getArtistProducts();

        function showFeaturedArt(){
            $('#featured_art_image').html('');
            $('#featured_art_image').append('<img src="'+artistproducts[$('#featured_art').val()]+'" width="200"/>');
        }

        $('#artist').on('change',function(){
            getArtistProducts();
        });

        $('#featured_art').on('change',function(){
            showFeaturedArt();
        });
    </script>
@endsection
