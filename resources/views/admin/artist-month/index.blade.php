@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Artists of the month</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            @if(Session::has('success'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert-success alert">{{ Session::get('success') }}</div>
                                    </div>
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                    </div>
                                </div>
                            @endif

                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th onclick="sortTable(0)">Date</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->artist->name }}</td>
                                            <td>{{ \Carbon\Carbon::parse($row->date_from)->format('M, Y') }}</td>
                                            <td><a href="{{ URL('admin/artist-of-the-month/'.$row->id) }}">Edit</a>
                                                |
                                                <a href="{{ URL('admin/artist-of-the-month/delete/'.$row->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
