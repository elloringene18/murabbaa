@extends('admin.partials.master')

@section('content')
    <!-- partial -->

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">

                @if(Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert-success alert">{{ Session::get('success') }}</div>
                    </div>
                @endif

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Files</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ url('admin/files/create') }}">+ Upload a File</a>
                                </div>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>

                            <table class="table table-striped" width="100%">
                                    <tr>
                                        <td>File Name</td>
                                        <td>Link</td>
                                        <td>Updated On</td>
                                        <td>Actions</td>
                                    </tr>
                                @foreach($posts as $post)
                                        <tr>
                                            <td style="max-width:200px;overflow: hidden;">{{ $post->original_name }}</td>
                                            <td style=""><a download="{{ $post->original_name }}" href="{{ asset($post->url) }}">{{ asset($post->url) }}</a></td>
                                            <td style="max-width:300px;overflow: hidden;">{{ $post->updated_at }}</td>
                                            <td style="max-width:300px;">
                                                <a href="{{ URL('admin/files/delete/'.$post->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                            </td>
                                        </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
