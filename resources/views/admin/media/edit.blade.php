@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Media</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/media/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">English</label>
                                            <input value="{{$item->title}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="title">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Arabic</label>
                                            <input value="{{$item->title_ar}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="title_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Description</label>
                                            <input type="hidden" name="content" value="{{ $item->content }}"/>
                                            <div class="summernote">
                                                {!! $item->content !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Description</label>
                                                <input type="hidden" name="content_ar" value="{{ $item->content_ar }}"/>
                                                <div class="summernote">
                                                    {!! $item->content_ar !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Date</label>
                                            <input value="{{$item->date}}" type="text" id='datetimepicker4'  class="form-control" placeholder="Name" name="date">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Location</label>
                                                <input value="{{$item->location}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="location">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Event Type</label>
                                                <select class="form-control type_select" name="event_type_id" data-lang="en" id="category_id">
                                                    @foreach($types as $type)
                                                        <option value="{{ $type->id }}" {{ $type->id == $item->type->id ? 'selected' : '' }}>{{ $type->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    @if($item->photo)
                                                        <br/>
                                                        <img src="{{asset(''.$item->photo)}}" width="200">
                                                        <br/>
                                                        <br/>
                                                        <label>Delete photo: <input type="checkbox" name="delete_photo" value="true"></label>
                                                        <br/>
                                                        <br/>
                                                    @endif
                                                    <input type="file" class="form-control" name="photo" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @include('admin.partials.pages.featured-images-list')

                                @include('admin.partials.pages.featured-image-form')

                                @include('admin.partials.hide-media')

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#datetimepicker4').datepicker();
    </script>
    <script src="{{ asset('public/admin/js/file-upload.js') }}"></script>
@endsection
