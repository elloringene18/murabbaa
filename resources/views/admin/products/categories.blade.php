@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Artwork</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/update-categories') }}" method="post" enctype="multipart/form-data">
                @csrf

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                @foreach($data as $artist)
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="">{{ $artist->name }}</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="cats">
                                                @foreach($artist->products as $item)
                                                    <tr>
                                                        <td>
                                                            <label>{{ $item->title }}</label></td>
                                                        <td style="padding-left: 30px;">
                                                            <select class="form-control type_select" name="product[{{$item->id}}][category_id]" data-lang="en" id="category_id">
                                                                @foreach($categories as $category)
                                                                    <option value="{{ $category['id'] }}" {{ $category['id'] == $item->category->id ? 'selected' : '' }}>{{ $category->parent ? $category->parent->name .' --- ' : '' }}{{ $category['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select class="form-control change_all">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category['id'] }}">{{ $category->parent ? $category->parent->name .' --- ' : '' }}{{ $category['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
    <script>
        $('.change_all').on('change',function(){
            cat = $(this).val();

            console.log($(this).closest('.card-body').find('.cats select'));
        });
    </script>
@endsection
