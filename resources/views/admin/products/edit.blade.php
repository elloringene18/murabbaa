@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Artwork</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">English</h4>
                                <div class="form-group">
                                    <input value="{{ $item->title }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" >
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description" value="{{ $item->description }}"/>
                                    <div class="summernote">
                                        {!! $item->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Arabic</label>
                                    <input value="{{ $item->title_ar }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title_ar">
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description_ar" value="{{ $item->description_ar }}"/>
                                    <div class="summernote">
                                        {!! $item->description_ar !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Image</label><br/>
                                    <img src="{{ $item->thumbnailUrl }}" width="200"><img src="{{ $item->photoUrl }}" width="200"><br/><br/>
                                    <input type="file" class="form-control" name="image" data-lang="en" accept="image/png, image/jpeg, image/gif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control type_select" name="category_id" data-lang="en" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category['id'] }}" {{ $category['id'] == $item->category->id ? 'selected' : '' }}>{{ $category->parent ? '--- ' : '' }}{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Artist</label>
                                    <select class="form-control type_select" name="artist_id" data-lang="en">
                                        @foreach($artists as $artist)
                                            <option value="{{ $artist['id'] }}" {{ $item->artist->id == $artist->id ? 'selected' : '' }}>{{ $artist['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>For Sale</label>
                                    <select class="form-control type_select" name="for_sale" data-lang="en">
                                        <option value="1" {{ $item->for_sale ? 'selected' : '' }}>Yes</option>
                                        <option value="0" {{ !$item->for_sale ? 'selected' : '' }}>No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Year</label>
                                    <select class="form-control type_select" name="year" data-lang="en">
                                        <?php for($x=1900;$x<=2100;$x++){ ?>
                                        <option value="{{ $x }}"  {{ $item->year == $x ? 'selected' : '' }}>{{ $x }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
    <script>
        var currentAttributes = JSON.parse("<?php print_r($currentAttributes); ?>");

        function getCategoryAttributes(){
            $('#attributes').html('');
            var id = $('#category_id').val();
            $.ajax({
                type: "GET",
                url: baseUrl+'/api/categories/get-attributes/'+id,
                success: function(response){
                    console.log(response);

                    $.each( response, function( key, cat ) {
                        label = '<div class="col-md-4"><br/>';
                        label += cat.name+':<br/><br/>';

                        $.each( cat.values, function( key, value ) {
                            label += '<label><input type="checkbox" name="attributes[]" '+ (currentAttributes.includes(value.id) ? 'checked' : '') +' value="'+value.id+'"/> '+value.value+'</label><br/>';
                            console.log(value.value)
                        });

                        label += "</div>";
                        $('#attributes').prepend(label);
                    });
                },
                statusCode: {
                    // 401: function() {
                    //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
                    // }
                },
                complete : function (event,error){
                    // hideLoader();
                }
            });
        }
        // getCategoryAttributes();

        $('#category_id').on('change',function(){

            getCategoryAttributes();
        });
    </script>
@endsection
