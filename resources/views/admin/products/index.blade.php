@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Artworks</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Title</th>
                                        <th onclick="sortTable(0)">Details</th>
                                        <th onclick="sortTable(0)">Artist</th>
                                        <th onclick="sortTable(2)">Category</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>
                                                <img src="{{ $item->thumbnailUrl }}" width="200" style="width: 100px !important;height: auto !important;border-radius: 0 !important;">
                                                {{ $item->title }}</td>
                                            <td>{!! \Illuminate\Support\Str::words(strip_tags($item->description),5) !!}...</td>
                                            <td>{{ $item->artist ? $item->artist->name : '' }}</td>
                                            <td>{{ $item->category ? $item->category->name : '' }}</td>
                                            <td><a href="{{ URL('admin/products/'.$item->id) }}">Edit</a>
                                                |
                                                <a href="{{ URL('admin/products/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
