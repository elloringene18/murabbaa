@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Artist</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/artists/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">English</label>
                                            <input value="{{ $item->name }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Arabic</label>
                                            <input value="{{ $item->name_ar }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="name_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Bio</label>
                                            <input type="hidden" name="bio"  value="{{ $item->bio }}"/>
                                            <div class="summernote">
                                                {!! $item->bio !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Bio</label>
                                                <input type="hidden" name="bio_ar"  value="{{ $item->bio_ar }}"/>
                                                <div class="summernote">
                                                    {!! $item->bio_ar !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Email (Optional)</label>
                                            <input value="{{ $item->email }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="email">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Telephone (Optional)</label>
                                                <input value="{{ $item->telephone }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="telephone">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Mobile (Optional)</label>
                                                <input value="{{ $item->mobile }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="mobile">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    @if($item->photo)
                                                        <br/>
                                                        <img src="{{asset(''.$item->photo)}}" width="200">
                                                        <br/>
                                                        <br/>
                                                        <label>Delete photo: <input type="checkbox" name="delete_photo" value="true"></label>
                                                        <br/>
                                                        <br/>
                                                    @endif
                                                    <input type="file" class="form-control" name="photo" data-lang="en">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Banner Photo</label>
                                                    @if($item->banner)
                                                        <br/>
                                                        <img src="{{$item->bannerUrl}}" width="700">
                                                        <br/>
                                                        <br/>
                                                    @endif
                                                    <input type="file" class="form-control" name="banner" accept="image/png, image/jpeg, image/gif">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($item->products)
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group collection">
                                    <label>Products</label>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                @foreach($item->products as $product)
                                                    <div class="col-md-4">
                                                        <a href="{{ URL('admin/products/'.$product->id) }}"><img src="{{$product->thumbnailUrl}}" width="200"></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group collection">
                                    <label>Collections</label>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                @foreach($item->collections as $collection)
                                                    <div class="col-md-4" style="margin-bottom: 30px;">
                                                        @if($collection->type=='photo')
                                                            <img src="{{asset(''.$collection->path)}}" width="100%">
                                                        @elseif($collection->type=='video')
                                                            <video width="100%" height="250" controls>
                                                                <source src="{{asset(''.$collection->path)}}" type="video/mp4">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        @endif

                                                        <a href="{{ url('admin/artists/delete-collection/'.$collection->id) }}">Delete</a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr/>
                                            Add Collection
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[0]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[1]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[2]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[3]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[4]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="file" class="form-control" name="collection[5]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
