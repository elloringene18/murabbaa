@extends('admin.partials.master')

@section('css')
    <style>
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add an Artist</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/artists/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-danger alert">{{ Session::get('error') }}</div>
                                        </div>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">English</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Arabic</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="name_ar">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Bio</label>
                                            <input type="hidden" name="bio" value=""/>
                                            <div class="summernote">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Bio</label>
                                                <input type="hidden" name="bio_ar" value=""/>
                                                <div class="summernote">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Email (Optional)</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="email">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Telephone (Optional)</label>
                                                <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="telephone">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group content">
                                                <label for="exampleInputEmail3">Mobile (Optional)</label>
                                                <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="mobile">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    <input type="file" class="form-control" name="photo" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Banner Photo</label>
                                                    <input type="file" class="form-control" name="banner" accept="image/png, image/jpeg, image/gif">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 grid-margin stretch-card">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group collection">
                                                    <label>Collections</label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[0]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[1]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[2]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[3]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[4]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="file" class="form-control" name="collection[5]" accept="image/png, image/jpeg, image/gif, video/mp4">
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
@endsection
