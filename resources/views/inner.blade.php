@extends('master')

@inject('categoryService', 'App\Services\CategoryProvider')
<?php $categories = $categoryService->getAll($lang); ?>
<?php $attributes = $categoryService->getCategoryAttributes($cat,$lang); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/catalog.css') }}">

    @if($lang=='ar')
        <style>
            .dropcategories .expand {
                right: auto;
                left: 0;
            }
            .dropcategories li a.cat, .taglist li a.cat {
                padding: 10px 15px 10px 35px !important;
            }
            .dropcategories ul a {
                font-size: 12px;
            }
            .taglist a.cat {
                background-position: left center;
            }
            .page-bt {
                float: right;
            }
            .catalog .left-line {
                float: right;
            }
            .catalog .details {
                width: 90%;
                float: right;
                padding-right: 20px;
                padding-left: 0;
            }
        </style>
    @endif
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')
    @include('partials.modal')

    <section id="" class="news-block pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 animate mb-5" data-animation="slide-in-left">
                    <ul class="dropcategories">
                        @foreach($categories as $category)
                            <li>
                                <a href="{{ $lang == 'en' ? url('/gallery/'.$category['slug']) : url('ar/gallery/'.$category['slug']) }}" class="cat {{ in_array($category['slug'],$activeCats) ? 'active' : ''}} {{ count($category['children']) ? 'hasChild' : '' }}">{{ $category['name'] }}</a>
                                {!! count($category['children']) > 0 ? '<span class="expand '.(in_array($category['slug'],$activeCats) ? 'active' : '').'"></span>' : ''  !!}
                                @if(count($category['children']))
                                    <ul class="{{ in_array($category['slug'],$activeCats) ? 'active' : ''}} sub-cats">
                                        @foreach($category['children'] as $child)
                                            <li><a href="{{ $lang == 'en' ? url('/gallery/'.$child['slug']) : url('ar/gallery/'.$child['slug']) }}" class="{{ in_array($child['slug'],$activeCats) ? 'active' : ''}}">{{ $child['name'] }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    <br/>

{{--                    <form action="{{ $lang == 'en' ? url('gallery') : url('ar/gallery') }}" method="post">--}}
{{--                    <input type="hidden" name="lang" value="{{$lang}}">--}}
{{--                    <input type="hidden" name="cat" value="{{$cat}}">--}}
{{--                    <input type="hidden" name="page" value="1">--}}
{{--                    @csrf--}}
{{--                    @foreach($attributes as $attribute)--}}
{{--                        @if($attribute['slug']=="color")--}}
{{--                            <ul class="colors taglist">--}}
{{--                                <li>--}}
{{--                                    <a href="#" class="cat active">{{ $lang=='ar' ? 'اللون' : 'Color' }}</a>--}}
{{--                                    <ul class="active">--}}
{{--                                        @foreach($attribute['values'] as $value)--}}
{{--                                            <li>--}}
{{--                                                <label style="background-color: {{$value['name']}}" class="color {{ in_array($value['id'],$activeAttrs) ? 'active' : '' }}" for="cb{{$value['id']}}"></label>--}}
{{--                                                <input id="cb{{$value['id']}}" type="checkbox" {{ in_array($value['id'],$activeAttrs) ? 'checked="checked"' : '' }} name="values[]" value="{{ $value['id'] }}">--}}
{{--                                            </li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        @else--}}
{{--                            <ul class="taglist">--}}
{{--                                <li>--}}
{{--                                    <?php--}}
{{--                                    $catActive = false;--}}
{{--                                        foreach ($attribute['values'] as $value){--}}
{{--                                            if(in_array($value['id'],$activeAttrs)){--}}
{{--                                                $catActive = true;--}}
{{--                                                break;--}}
{{--                                            }--}}
{{--                                        }--}}
{{--                                    ?>--}}
{{--                                    <a href="#" class="cat {{ $catActive ? 'active' : '' }}">{{ $attribute['name'] }}</a>--}}
{{--                                    <ul class="{{ $catActive ? 'active' : '' }}">--}}
{{--                                        @foreach($attribute['values'] as $value)--}}
{{--                                            <li><label class="tag {{ in_array($value['id'],$activeAttrs) ? 'active' : '' }}" for="cb{{$value['id']}}">{{ $value['name'] }}</label><input id="cb{{$value['id']}}" type="checkbox" {{ in_array($value['id'],$activeAttrs) ? 'checked="checked"' : '' }} name="values[]" value="{{ $value['id'] }}"></li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        @endif--}}
{{--                    @endforeach--}}
{{--                    <input type="submit" value="{{ $lang == 'en' ? 'filter' : 'فلتر' }}" id="filterBt">--}}
{{--                    </form>--}}
                </div>
                <div class="col-md-9 animate" data-animation="slide-in-right">
                    <div class="row catalog">
                        @if(count($products))
                            @foreach($products as $product)
                            <div class="col-lg-4 col-md-6">
                                <a href="#" class="productLink" data-id="{{$product['id']}}">
                                <img src="{{ $product['photo'] }}" width="100%">
                                </a>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="left-line"></span>
                                        <div class="details">
                                            <h5 class="title">{{ $product['title'] }}</h5>
    {{--                                            <p class="sub">{{ $product['category']->name }}</p>--}}
                                            <p class="sub {{ $product['for_sale'] ? 'sale' : '' }}">{{ $product['for_sale'] ? ($lang== 'en' ? "FOR SALE" : 'للبيع' ): ($lang== 'en' ? "NOT FOR SALE" : 'ليس للبيع' ) }}</p>
                                            {{--<p class="sub">--}}
                                                {{--@foreach($product['attributes'] as $value)--}}
                                                    {{--{{ $value['value'] }}--}}
                                                {{--@endforeach--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <p>{{ $lang == 'en' ? 'No products found.' : 'لا توجد منتجات.' }}</p>
                            </div>
                        @endif
                    </div>
                    <div class="row pagntin">
                        <div class="col-md-12"><hr/></div>
                        <div class="col-md-6 paginations">
                            @if(count($activeAttrs))
                                @for($x=1;$x<$data->lastPage();$x++)
                                    <form action="{{ $lang == 'en' ? url('gallery') : url('ar/gallery') }}" method="post">
                                        @csrf

                                        @foreach($attributes as $attribute)
                                            @foreach($attribute['values'] as $value)
                                                @if(in_array($value['id'],$activeAttrs))
                                                    <input type="hidden" name="values[]" value="{{ $value['id'] }}">
                                                @endif
                                            @endforeach
                                        @endforeach

                                        <input type="hidden" name="lang" value="{{$lang}}">
                                        <input type="hidden" name="cat" value="{{$cat}}">
                                        <input type="hidden" name="page" value="{{ $x }}">
                                        <input type="submit" class="page-bt {{ $data->currentPage() == $x ? 'active' : '' }}" {{ $data->currentPage() == $x ? 'disabled' : '' }} value="{{ $x }}">
                                    </form>
                                @endfor
                            @else
                                @for($x=1;$x<=$data->lastPage();$x++)
                                    <form action="{{ $lang == 'en' ? url('gallery') : url('ar/gallery') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="lang" value="{{$lang}}">
                                        <input type="hidden" name="cat" value="{{$cat}}">
                                        <input type="hidden" name="page" value="{{ $x }}">
                                        <input type="submit" class="page-bt {{ $data->currentPage() == $x ? 'active' : '' }}" {{ $data->currentPage() == $x ? 'disabled' : '' }} value="{{ $x }}">
                                    </form>
                                @endfor
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            @if($lang=='ar')
                            إظهار {{($data->currentpage()-1)*$data->perpage()+1}} إلى {{ ($data->currentpage()*$data->perpage()) <= $data->total() ? $data->currentpage()*$data->perpage() : $data->total() }} نتائج

                            @else
                            Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{ ($data->currentpage()*$data->perpage()) <= $data->total() ? $data->currentpage()*$data->perpage() : $data->total() }}
                            of  {{$data->total()}} entries
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script src="{{ asset('js/catalog.js') }}"></script>
@endsection()

