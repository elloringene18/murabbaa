@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/contact.css') }}">

    @if($lang=='ar')
        <style>
            .contact input[type="submit"] {
                background-image: url({{ asset('img/community/submit-arrow-ar.png') }});
                padding: 8px 25px 8px 125px;
                background-position: 20px center;
            }
            #whatsapp-box {
                text-align: left;
            }

        </style>
    @endif

    <style>
        .this {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            height: 0;
            width: 0;
            z-index: -1;
        }
    </style>
@endsection

@section('content')

<body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="header-line-bot mt-4">{{ $lang=='ar' ? 'اتصل بنا' : 'Contact Us' }}<span></span></h1>
                </div>
            </div>
            <form class="contact" action="{{url('contact')}}" method="post">
                @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif
                @csrf

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <input type="hidden" name="lang" value="{{$lang}}">
                <div class="row ">
                    <div class="col-md-12">
                            <h3 class="bluetext mb-4">{{ $lang=='ar' ? 'أرسل لنا رسالة' : 'Send us a message' }}</h3>
                    </div>
                        <div class="col-md-12">
                            <div class="custom-select">
                                <select name="subject">
                                    <option value="" disabled selected>{{ $lang == 'en' ? 'Type of inquiry' : 'نوع الاستفسار' }}</option>
                                    <option value="general-inquiry">{{ $lang == 'en' ? 'General Inquiry' : 'استفسار عام' }}</option>
                                    {{--<option value="suggestions">{{ $lang == 'en' ? 'Suggestions' : 'اقتراحات' }}</option>--}}
                                    <option value="suggestions">{{ $lang == 'en' ? 'Feedback' : 'Feedback' }}</option>
                                    <option value="artist-listing">{{ $lang == 'en' ? 'Artist Listing' : 'قائمة الفنانون' }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input name="user_id" type="text" id="firstname" class="this" autocomplete="off" tabindex="-1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name" placeholder="{{ $lang=='ar' ? 'اسم' : 'Name' }}" required>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="email" placeholder="{{ $lang=='ar' ? 'البريد الإلكتروني' : 'Email' }}" required>
                        </div>
                        <div class="col-md-12">
                            <textarea name="message" placeholder="{{ $lang=='ar' ? 'رسالة' : 'Enter message here...' }}"></textarea>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="mt-3" value="{{ $lang=='ar' ? 'أرسل الرسالة' : 'Send the message' }}">
                        </div>
                </div>
            </form>
            @inject('contentService', 'App\Services\ContentProvider')
            <?php $contact = $contentService->getPageContent('contact',$lang); ?>

            <div class="row" id="whatsapp-box">
                <div class="col-md-12">
                    @if($lang=='en')
                    Don't want to use our online form? You can send us an email at: <a href="mailto:{{$contact['contact-email']}}">{{$contact['contact-email']}}</a>
                    @else
                         <br/>  لا تريد استخدام نموذجنا الإلكتروني؟ يمكنك إرسال بريد إلكتروني إلينا على
                        <a href="mailto:{{$contact['contact-email']}}">{{$contact['contact-email']}}</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
    
@endsection()
