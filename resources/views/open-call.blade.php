@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageContent('open-call',$lang); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/contact.css') }}">
    @if($lang=='ar')
        <style>
            .box-list li {
                float: right;
            }
            .box-list li ul li a {
                font-size: 13px;
                text-align: right;
            }
            .box-list li a {
                float: right;
            }
            #subscribeForm input[type="submit"] {
                float: left;
            }

            .select-selected:after {
                left: 10px;
                right: auto;
            }

            .file-custom:after {
                left: 10px;
                right: auto !important;
            }
        </style>
    @endif

    <style>
        .inner h1, .inner h2, .inner h3, .inner h4, .inner h5 {
            font-weight: bold;
            line-height: 1.1 !important;
        }

        .inner .step-gallery .sub {
            font-size: 18px;
            color: #0b1684;
        }

        .inner p strong {
        }

        .custom-file-input {
            color: transparent;
        }
        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }
        .custom-file-input::before {
            content: 'Select some files';
            color: black;
            display: inline-block;
            background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
            border: 1px solid #999;
            border-radius: 3px;
            padding: 5px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;
        }
        .custom-file-input:hover::before {
            border-color: black;
        }
        .custom-file-input:active {
            outline: 0;
        }
        .custom-file-input:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }

        .file-custom {
            display: block;
            width: 100%;
            height: 45px;
            background-color: #fff;
            position: absolute;
            top: 0;
            border-bottom: 1px solid #c42f66;
            line-height: 45px;
            padding: 0 10px;
            color: #c42f66;
        }

        .file-custom:after {
            position: absolute;
            content: "";
            top: 14px;
            right: 10px;
            width: 0;
            height: 0;
            border: 6px solid transparent;
            border-color: #c42f66 transparent transparent transparent;
        }

        .relative {
            position: relative;
        }

        @if($lang=='ar')
            .select-items div, .select-selected, .file-custom {
                padding: 0 36px;
            }
        @endif


        .this {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            height: 0;
            width: 0;
            z-index: -1;
        }

        .contents h1, .contents h2,.contents h3,.contents h4,.contents h5,.contents h6 {
            margin-bottom: 40px;
            position: relative;
            margin-top: 20px;
        }

        .contents h1:after, .contents h2:after,.contents h3:after,.contents h4:after,.contents h5:after,.contents h6:after {
            content: '';
            display: block;
            position: absolute;
            width: 60px;
            height: 3px;
            background-color: #c42f66;
            bottom: -20px;
        }

    </style>
@endsection


@section('content')
    <body class="inner">

    @include('partials.modal')

    @include('partials.menu')
    @include('partials.featured-nav')

    <section id="" class="artist-month ">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12 contents">
                    {!! $pageContent['top-content'] !!}
                </div>
            </div>

            <div class="row pb-5 pt-5">
                <div class="col-md-12">
                    @if($lang=='en')
                        <form class="contact" action="{{url('open-call')}}" method="post" enctype="multipart/form-data">
                            @if(Session::has('message'))
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @csrf
                            <input type="hidden" name="lang" value="{{$lang}}">
                            <div class="row ">
                                <div class="col-md-12">
                                    <h4 class="bluetext mb-4">Fill in the form below to continue:</h4>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="name" placeholder="Full Name" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="phone" placeholder="Phone Number" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="email" placeholder="Email" required>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input name="name-full" type="text" id="firstname" class="this" autocomplete="off" tabindex="-1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="nationality" placeholder="Nationality" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="website" placeholder="Website">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="social-media-handles" placeholder="Social Media Handles">
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-select">
                                        <select name="category" required>
                                            <option value="" disabled selected>Choose Category</option>
                                            <option value="art" >Art</option>
                                            <option value="design" >Design</option>
                                            <option value="workshops" >Workshops</option>
                                            <option value="literature" >Literature</option>
                                            <option value="music-theater-film" >Music, theater & film</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="relative">
                                        <input id="file" type="file" class="custom-file-input" name="portfolio" placeholder="Portfolio" required accept=".jpeg,.jpg,.png,.pdf,.doc,.docx,.ppt,.pptx">
                                        <span class="file-custom">Upload portfolio</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" placeholder="Tell us a little about yourself"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="mt-3" value="Submit">
                                </div>
                            </div>
                        </form>
                    @else
                        <form class="contact" action="{{url('open-call')}}" method="post">
                            @if(Session::has('message'))
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @csrf
                            <input type="hidden" name="lang" value="{{$lang}}">
                            <div class="row ">
                                <div class="col-md-12">
                                    <h4 class="bluetext mb-4">للمتابعة، أملأ النموذج أدناه:</h4>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="name" placeholder="الاسم الكامل" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="phone" placeholder="رقم الهاتف" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="email" placeholder="البريد الالكتروني" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="nationality" placeholder="الجنسية" required>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input name="name-full" type="text" id="firstname" class="this" autocomplete="off" tabindex="-1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="website" placeholder="الموقع الالكتروني" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="social-media-handles" placeholder="حسابات مواقع التواصل الاجتماعي">
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-select">
                                        <select name="category" required>
                                            <option value="" disabled selected>اختر فئة</option>
                                            <option value="art" disabled selected>الفن</option>
                                            <option value="design" disabled selected>التصميم</option>
                                            <option value="workshops" disabled selected>ورش العمل </option>
                                            <option value="literature" disabled selected>الآداب </option>
                                            <option value="music-theater-film" disabled selected>الموسيقى والمسرح والأفلام</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="relative">
                                        <input type="file" class="custom-file-input" name="portfolio" placeholder="رفع الاقتراح" required>
                                        <span class="file-custom">رفع الاقتراح</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" placeholder="أخبرنا عن نفسك قليلاً"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="mt-3" value="إرسال">
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>

        </div>

    </section>

@endsection()

@section('js')
    <script>
        $('.custom-file-input').on('change',function(){
        });

        var uploadField = document.getElementById("file");

        uploadField.onchange = function() {
            if(this.files[0].size > 2097152){
                alert("The maximum file size is 2MB");
                this.value = "";
            }
            else
                $('.file-custom').first().html($(this).val().replace(/C:\\fakepath\\/i, ''));
        };
    </script>
@endsection

