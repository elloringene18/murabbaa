@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/community.css') }}">
    <style>
        .searchCta {
            background-color: #203f7a;
            padding: 8px 20px;
            border-radius: 20px;
            color: #fff !important;
            display: inline-block;
            text-transform: uppercase;
            bottom: 30px;
            right: 45px;
            left: auto;
            margin-left: 0;
            text-decoration: none;
        }
        .searchCta:hover {
            background-color: #c42f66;
            text-decoration: none;
        }
        .inner p {
            font-size: 16px;
        }
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.modal')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            @if(isset($keyword))
                <div class="row">
                    <div class="col-md-12 " >
                        <h1 class="header-line-bot mt-4">{{ $lang=='ar' ? 'نتائج البحث لـ "'.$keyword.'"' : 'Search results for "'.$keyword.'"' }}<span></span></h1>
                    </div>
                </div>

            @if(
                !count($data['artists']) &&
                !count($data['products']) &&
                !count($data['media']) &&
                !count($data['events'])
                )
                <p>{{ $lang=='ar' ? 'لا توجد نتائج بحث للكلمة' : 'No search results for the keyword.' }}</p>
            @endif

            @if(count($data['products']))
                <div class="row">
                    <div class="col-md-12 " >
                        <h5 class="header-line-bot mt-4">{{ $lang=='ar' ? 'أعمال فنية' : 'Artworks' }}<span></span></h5>
                    </div>
                </div>

                @foreach($data['products'] as $product)
                    <div class="row news ">
                        <div class="col-md-12 pl-5">
                            <h5>{{ $lang == 'ar' ? $product->searchable->title_ar : $product->searchable->title }}</h5>
                            <p>{{ $lang == 'ar' ? \Illuminate\Support\Str::words(strip_tags($product->searchable->description_ar),40) : \Illuminate\Support\Str::words(strip_tags($product->searchable->description),40)  }}..</p>                            {{--<a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="searchCta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>--}}
                            <a href="#" class="productLink" data-id="{{$product->searchable->id}}">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                            <hr/>
                        </div>
                    </div>
                @endforeach
            @endif

            @if(count($data['media']))
                <div class="row">
                    <div class="col-md-12 " >
                        <h5 class="header-line-bot mt-4">{{ $lang=='ar' ? 'الإعلام والصحافة' : 'Media and Press' }}<span></span></h5>
                    </div>
                </div>

                @foreach($data['media'] as $product)
                    <div class="row news ">
                        <div class="col-md-12 pl-5">
                            <h5>{{ $lang == 'ar' ? $product->searchable->title_ar : $product->searchable->title }}</h5>
                            <p>{{ $lang == 'ar' ? \Illuminate\Support\Str::words(strip_tags($product->searchable->content_ar),40) : \Illuminate\Support\Str::words(strip_tags($product->searchable->content),40)  }}..</p>                            {{--<a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="searchCta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>--}}
                            <a href="{{ $lang=='ar' ? url('ar/media/p/'.$product->searchable->slug) : url('media/p/'.$product->searchable->slug) }}">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                            <hr/>
                        </div>
                    </div>
                @endforeach
            @endif

            @if(count($data['events']))
                <div class="row">
                    <div class="col-md-12 " >
                        <h5 class="header-line-bot mt-4">{{ $lang=='ar' ? 'الأحداث' : 'Events' }}<span></span></h5>
                    </div>
                </div>

                @foreach($data['events'] as $item)
                    <div class="row news ">
                        <div class="col-md-12 pl-5">
                            <h5>{{ $lang == 'ar' ? $item->searchable->title_ar : $item->searchable->title }}</h5>
                            <p>{{ $lang == 'ar' ? \Illuminate\Support\Str::words(strip_tags($item->searchable->content_ar),40) : \Illuminate\Support\Str::words(strip_tags($item->searchable->content),40)  }}..</p>
                            <a href="{{ $lang=='ar' ? url('ar/calendar/p/'.$item->searchable->slug) : url('calendar/p/'.$item->searchable->slug) }}">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                            <hr/>
                        </div>
                    </div>
                @endforeach
            @endif

            @if(count($data['artists']))
                <div class="row">
                    <div class="col-md-12 " >
                        <h5 class="header-line-bot mt-4">{{ $lang=='ar' ? 'الفنانون' : 'Artists' }}<span></span></h5>
                    </div>
                </div>

                @foreach($data['artists'] as $item)
                    <div class="row news ">
                        <div class="col-md-12 pl-5">
                            <h5>{{ $lang == 'ar' ? $item->searchable->name_ar : $item->searchable->name }}</h5>
                            <p>{{ $lang == 'ar' ? \Illuminate\Support\Str::words(strip_tags($item->searchable->bio_ar),40) : \Illuminate\Support\Str::words(strip_tags($item->searchable->bio),40)  }}..</p>
                            <a href="{{ $lang=='ar' ? url('ar/artist-gallery/'.$item->searchable->slug) : url('artist-gallery/'.$item->searchable->slug) }}">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                            <hr/>
                        </div>
                    </div>
                @endforeach
            @endif

            @endif
        </div>
    </section>

@endsection
