@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/catalog.css') }}">
    <style>
        .artist-items {
            overflow: hidden;
        }
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <a class="backlink" href="{{ $lang=='en'? url('media') : url('ar/media') }}">
                        <strong>{{ $lang == "en" ? '< Media and Press' : '< الإعلام والصحافة' }}</strong>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 animate" data-animation="slide-in-left">
                    <h1 class="header-line-bot mt-5">{{ $data['title'] }}<span></span></h1>
                    <p>{{ $data['date'] }}</p>

                </div>
            </div>
            <div class="row news ">
                <div class="col-md-12">
                    <img src="{{ $data['photo'] }}" width="100%">
                    <br/>
                    <br/>
                    {!!  $data['content']  !!}
                </div>
            </div>
            <div class="row news ">
                <div class="col-md-12">
                    @if(isset($data['slides']))
                        @if(count($data['slides']))
                            <br/>
                            <div class="sponsors">
                                @foreach($data['slides'] as $slide)
                                    <div class="">
                                        <img src="{{ asset('public/'.$slide['source']) }}" width="100%">
                                    </div>
                                @endforeach
                            </div>  
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </section>

    @endsection()

    @section('js')
        <script>
            $('.sponsors').slick({
                dots: true,
                infinite: false,
                speed: 500,
                @if($lang=='ar')
                    rtl: true,
                @endif
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                // responsive: [
                //     {
                //         breakpoint: 1024,
                //         settings: {
                //             slidesToShow: 3,
                //             slidesToScroll: 3,
                //             infinite: true,
                //             dots: true
                //         }
                //     },
                //     {
                //         breakpoint: 600,
                //         settings: {
                //             slidesToShow: 2,
                //             slidesToScroll: 2
                //         }
                //     },
                //     {
                //         breakpoint: 480,
                //         settings: {
                //             slidesToShow: 1,
                //             slidesToScroll: 1
                //         }
                //     }
                //     // You can unslick at a given breakpoint now by adding:
                //     // settings: "unslick"
                //     // instead of a settings object
                // ]
            });
        </script>
@endsection
