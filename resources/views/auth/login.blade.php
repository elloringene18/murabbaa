<?php header("X-Frame-Options: DENY"); header("X-Content-Type-Options: nosniff"); header("Content-Security-Policy: frame-ancestors 'none'"); header("X-XSS-Protection: 1; mode=block");?>

<style>
    body { 
        display : none   
    }
</style>

<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <img src="{{ asset('img/M-logo.png') }}" width="70">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email"  autocomplete="off" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password"  autocomplete="off" name="password" required autocomplete="current-password" />
            </div>

            {{--<div class="block mt-4">--}}
                {{--<label for="remember_me" class="flex items-center">--}}
                    {{--<input id="remember_me" type="checkbox" class="form-checkbox" name="remember">--}}
                    {{--<span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>--}}
                {{--</label>--}}
            {{--</div>--}}

            <div class="flex items-center justify-end mt-4">
                {{--@if (Route::has('password.request'))--}}
                    {{--<a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">--}}
                        {{--{{ __('Forgot your password?') }}--}}
                    {{--</a>--}}
                {{--@endif--}}

                <x-jet-button class="ml-4">
                    {{ __('Login') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>


<script>
    if(self == top) {
        document.getElementsByTagName("body")[0].style.display = 'block';
    }
    else{
        top.location = self.location;
    }
</script>