<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('fonts/webfonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/pages/exhibition.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    @if($lang=='ar')
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600;700;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main-ar.css') }}">
    @else
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
@endif
<!--

    <link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
/>
<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
-->

    <meta name="theme-color" content="#fafafa">

    <style>
        .swiper-slide .box {
            width: 255px;
            height: 255px;
            background-color: #0b1684;
            color: #fff;
        }
        body.modal-open {
            overflow: visible;
        }
        #panorama {
            direction: ltr !important;
        }
        #panorama .img {
        }

        @if($lang=='ar')
        #nav {
            left: auto;
            right: 30px;
        }
        #nav ul li .dot {
            margin-right: -45px;
        }
        @endif

    </style>
</head>

<body>

@include('partials.modal')

<section id="" class="news-block">
    <div class="container-fluid">
        <div class="row">
            <div id="panorama">
                <div class="img">
                    <img src="{{ asset('img/exhibition/gallery-bg.jpg') }}">
                    <div class="hotspot" data-id="1" data-y="50" data-x="24"></div>
                    <div class="hotspot" data-id="2" data-y="45" data-x="34"></div>
                    <div class="hotspot" data-id="3" data-y="45" data-x="40"></div>
                    <div class="hotspot" data-id="4" data-y="50" data-x="46"></div>
                    <div class="hotspot" data-id="5" data-y="45" data-x="57"></div>
                    <div class="hotspot" data-id="6" data-y="50" data-x="64"></div>
                    <div class="hotspot" data-id="7" data-y="45" data-x="71"></div>
                </div>
            </div>
            <div id="nav">
                <h1>{{ $lang=='ar' ? 'أهلا بك' : 'Welcome' }}</h1>
                <p>{{ $lang=='ar' ? 'ما هو "لوريم إيبسوم' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' }}</p>
                <a href="#" id="mobileMenu" class=""></a>

                @if($lang=='ar')
                    <ul>
                        <li><div class="dot">1<span></span></div><a href="#" data-id="7">{{ $lang=='ar' ? 'بالسعادة، ولكن بفضل هؤلاء' : 'Incididunt ut labore et' }}</a></li>
                        <li><div class="dot">2<span></span></div><a href="#" data-id="6">{{ $lang=='ar' ? 'حد يرفض أو يكره أو يتجنب' : 'Sit ipsum elit' }}</a></li>
                        <li><div class="dot">3<span></span></div><a href="#" data-id="5">{{ $lang=='ar' ? 'حقيقة وأساس تلك السعادة ' : 'Lorem  ipsum dolor' }} </a></li>
                        <li><div class="dot">4<span></span></div><a href="#" data-id="4">{{ $lang=='ar' ? 'وسأعرض لك التفاصيل لتكتشف' : 'Incididunt ut labore et' }}</a></li>
                        <li><div class="dot">5<span></span></div><a href="#" data-id="3">{{ $lang=='ar' ? 'النشوة وتمجيد الألم نشأت بالفعل،' : 'Sed do eiusmod tempor' }}</a></li>
                        <li><div class="dot">6<span></span></div><a href="#" data-id="2">{{ $lang=='ar' ? 'الأفكار المغلوطة حول استنكار' : 'Consectetur adipiscing elit' }}</a></li>
                        <li><div class="dot">7<span></span></div><a href="#" data-id="1">{{ $lang=='ar' ? 'لكن لا بد أن أوضح لك أن كل هذه' : 'Lorem ipsum dolor sit' }}</a></li>
                    </ul>
                @else
                    <ul>
                        <li><div class="dot">1<span></span></div><a href="#" data-id="1">{{ $lang=='ar' ? 'لكن لا بد أن أوضح لك أن كل هذه' : 'Lorem ipsum dolor sit' }}</a></li>
                        <li><div class="dot">2<span></span></div><a href="#" data-id="2">{{ $lang=='ar' ? 'الأفكار المغلوطة حول استنكار' : 'Consectetur adipiscing elit' }}</a></li>
                        <li><div class="dot">3<span></span></div><a href="#" data-id="3">{{ $lang=='ar' ? 'النشوة وتمجيد الألم نشأت بالفعل،' : 'Sed do eiusmod tempor' }}</a></li>
                        <li><div class="dot">4<span></span></div><a href="#" data-id="4">{{ $lang=='ar' ? 'وسأعرض لك التفاصيل لتكتشف' : 'Incididunt ut labore et' }}</a></li>
                        <li><div class="dot">5<span></span></div><a href="#" data-id="5">{{ $lang=='ar' ? 'حقيقة وأساس تلك السعادة ' : 'Lorem  ipsum dolor' }} </a></li>
                        <li><div class="dot">6<span></span></div><a href="#" data-id="6">{{ $lang=='ar' ? 'حد يرفض أو يكره أو يتجنب' : 'Sit ipsum elit' }}</a></li>
                        <li><div class="dot">7<span></span></div><a href="#" data-id="7">{{ $lang=='ar' ? 'بالسعادة، ولكن بفضل هؤلاء' : 'Incididunt ut labore et' }}</a></li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.kinetic/jquery.kinetic.js"></script>
<script src="{{ asset('js/vendor/modernizr-3.11.2.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/exhibition.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>


@if($lang=='ar')
    <script>
        $( window ).on('load',function() {
            console.log('s');
            setTimeout(function(){
                $('#nav li a').first().trigger('click');
            },300);
        });
    </script>
@endif

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<!--
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
-->
</body>

</html>
