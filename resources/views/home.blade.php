@extends('master')

@inject('productService', 'App\Services\ProductProvider')
@inject('categoryService', 'App\Services\CategoryProvider')
@inject('contentService', 'App\Services\ContentProvider')
<?php $artistOfMonth = $contentService->getArtistOfTheMonth(\Carbon\Carbon::now(),$lang); ?>
<?php $products = $productService->getPaginate(10,$lang); ?>

<?php
    $homeCategories = $contentService->getHomeCategories($lang);
    $homeProducts = [];

    foreach($homeCategories as $homeCategory){
        $homeProducts[$homeCategory['slug']]['name'] = $homeCategory['name'];
        $homeProducts[$homeCategory['slug']]['products'] = $productService->getByChildCat($homeCategory['slug'],$lang,10);
    }
?>

<?php $categories = $categoryService->getAll($lang); ?>
<?php $sponsors = $contentService->getSponsors(null,$lang); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('css/pages/home.css') }}">
    @if($lang=='ar')
        <style>
            .box-list li {
                float: right;
            }
            .box-list li ul li a {
                font-size: 13px;
                text-align: right;
            }
            .box-list li a {
                float: right;
            }
            #subscribeForm input[type="submit"] {
                float: left;
            }
        </style>
    @endif

    <style>
        {{--#main-featured-art {--}}
        {{--    @if(count($artistOfMonth['artworks']))--}}
        {{--        background-image: url('{{$artistOfMonth['featured_artwork']}}');--}}
        {{--        background-size: 100% auto;--}}
        {{--    @endif--}}
        {{--}--}}

        .sponsors .slick-dots {
            width: 100%;
            text-align: center;
            position: relative;
            float: left;
            display: inline-block;
            margin-top: 30px;
        }

        .sponsors .slick-dots li {
            float: none;
            display: inline-block;
        }

        .sponsors .slick-slide {
            text-align: center;
        }

        .sponsors .slick-slide img {
            max-width: 200px;
            display: inline-block;
        }
    </style>
@endsection


@section('content')
<body>

    @include('partials.modal')
    @include('partials.menu')

    <section id="main-featured-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="main-featured-art">
                        <div id="featured-nav">
                            <a href="#" class="arrow"></a>
                        </div>
                        @include('partials.featured-nav-menu')
                        {{--@if($artistOfMonth)--}}
                            {{--<div class="content">--}}
                                {{--<h2 class="title">{{ $artistOfMonth['name'] }} </h2>--}}
                                {{--<p class="details">{{ $lang=='ar' ? 'فنان الشهر' : 'Discover the artist of the month.' }}</p>--}}
                            {{--</div>--}}
                            {{--<a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/exhibition') : url('exhibition') }}">{{ $lang=='ar' ? 'اكتشف المعرض--}}
    {{--' : 'discover gallery' }}</a>--}}
                        {{--@endif--}}


                    <!-- Slider main container -->
                        @if($lang=='ar')
                            <div dir="ltr" style="direction: ltr">
                                <div class="" id="featured-slide">
{{--                                    <div class="swiper-slide" style="background-image:url('{{ asset('img/home/open-call-2.jpg') }}')"  dir="rtl">--}}
{{--                                        <div class="overlay"></div>--}}
{{--                                        <div class="content">--}}
{{--                                            <h2 class="title">دعوى لجميع الفنانين والنجوم والعقول الإبداعية</h2>--}}
{{--                                            <p class="details">مهرجان المربعة للفنون | ٢٨ أكتوبر - ٦ نوفمبر ٢٠٢١</p>--}}
{{--                                        </div>--}}
{{--                                        --}}{{--<a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/open-call') : url('open-call') }}">قم بالتسجيل الآن</a>--}}
{{--                                    </div>--}}
                                    <div class="swiper-slide" style="background-image:url('{{ asset('img/home/festival.jpg') }}')" dir="rtl">
                                        <div class="overlay"></div>
                                        <div class="content">
                                            <h2 class="title">نظرة على مهرجان المربعة للفنون 2021
                                            </h2>
                                        </div>
                                        {{--<a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/al-murabbaa-festival') : url('al-murabbaa-festival') }}">اكتشف المزيد</a>--}}
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="" id="featured-slide">
{{--                                <div class="swiper-slide" style="background-image:url('{{ asset('img/home/open-call-2.jpg') }}')">--}}
{{--                                    <div class="overlay"></div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h2 class="title">CALLING ALL ARTISTS,--}}
{{--                                            PERFORMERS & CREATIVE MINDS</h2>--}}
{{--                                        <p class="details">--}}
{{--                                            Al Murabbaa Art Festival | 28<sup>th</sup> OCTOBER – 6<sup>th</sup> NOVEMBER 2021</p>--}}
{{--                                    </div>--}}
{{--                                    --}}{{--<a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/open-call') : url('open-call') }}">Apply Now</a>--}}
{{--                                </div>--}}
                                {{--<div class="swiper-slide" style="background-image:url('{{ asset('img/home/Unknown-2.jpg') }}')">--}}
                                    {{--<div class="overlay"></div>--}}
                                    {{--<div class="content">--}}
                                        {{--<h2 class="title">EXPLORE OUR--}}
                                            {{--ONLINE ART GALLERY--}}
                                        {{--</h2>--}}
                                    {{--</div>--}}
                                    {{--<a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/exhibition') : url('exhibition') }}">Discover</a>--}}
                                {{--</div>--}}
                                <div class="swiper-slide" style="background-image:url('{{ asset('img/home/festival.jpg') }}')">
                                    <div class="overlay"></div>
                                    <div class="content">
                                        <h2 class="title">Al Murabbaa Arts Festival 2021 Highlights
                                        </h2>
                                    </div>
                                    <a class="blue-cta cta" href="{{ $lang=='ar' ? url('ar/al-murabbaa-festival') : url('al-murabbaa-festival') }}">Discover</a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right pt-3 pb-3">
               {{ $lang=='ar' ? 'بمبادرة من ' : 'Curated by ' }} &nbsp; <a href="https://ajman.travel/" target="_blank"><img src="{{ asset('img/ajman-tourism-logo.png') }}"></a>
                </div>
            </div>
        </div>
    </section>

    {{--@if(count($categories))--}}
        {{--<section id="home-categories">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h3 class="header-line-bot small mt-5 animate" data-animation="slide-in-left">{{ $lang=='ar' ? 'الأعمال الفنية' : 'Artwork' }} <span></span></h3>--}}
                        {{--<ul class="box-list animate" data-animation="slide-in-right">--}}

                                {{--@foreach($categories as $category)--}}
{{--                                    <li><a href="{{ $lang=='ar' ? url('ar/gallery/'.$category['slug']) : url('gallery/'.$category['slug']) }}">{{ $category['name'] }}</a>--}}
                                    {{--<li><a href="#">{{ $category['name'] }}</a>--}}
                                        {{--@if(count($category['children']))--}}
                                            {{--<ul class="sub-cats">--}}
                                                {{--@foreach($category['children'] as $child)--}}
                                                    {{--<li><a href="{{ $lang == 'en' ? url('/gallery/'.$child['slug']) : url('ar/gallery/'.$child['slug']) }}" class="">{{ $child['name'] }}</a></li>--}}
                                                    {{--<li><a href="#" class="">{{ $child['name'] }}</a></li>--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--@endif--}}
                                    {{--</li>--}}
                                {{--@endforeach--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/gallery/') : url('gallery/') }}">{{ $lang=='ar' ? 'كل الوسائط' : 'All Media' }}</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
    {{--@endif--}}

    {{--<section id="" class=" animate" data-animation="slide-in-right">--}}
        {{--<div class="container pt-5 pb-5">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<!-- Slider main container -->--}}
                    {{--<div dir="ltr" style="direction: ltr">--}}
                        {{--<div class="swiper-container">--}}
                            {{--<!-- Additional required wrapper -->--}}
                            {{--<!-- Slides -->--}}
                            {{--@if(count($products))--}}
                                {{--@foreach($products as $product)--}}
                                {{--<div class="swiper-slide">--}}
                                    {{--<a href="#" class="productLink" data-id="{{$product['id']}}"><img src="{{ $product['photo'] }}" class="animista fade-in-bottom"></a>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="navs"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    {{--<section id="" class="artist-month pt-5 pb-5">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-12 animate" data-animation="slide-in-left">--}}
                    {{--<span class="subheading">{{ $lang=='ar' ? 'فنان الشهر' : 'Artist of the Month' }} </span>--}}
                    {{--<h3 class="header-line-bot">{{ $artistOfMonth['name'] }} <span></span></h3>--}}
                {{--</div>--}}
                {{--<div class="col-md-8 col-sm-12 animate" data-animation="slide-in-right">--}}
                    {{--<p class="details">--}}
                        {{--{{ \Illuminate\Support\Str::words(strip_tags($artistOfMonth['bio']),50) }}--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row step-gallery animate" data-animation="slide-in-bottom">--}}
                {{--@if(count($artistOfMonth['featured_artworks']))--}}
                    {{--<?php $countArt = 1;?>--}}
                    {{--@foreach($artistOfMonth['featured_artworks'] as $product)--}}
                        {{--@if($countArt<5)--}}
                            {{--<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 cols">--}}
                                {{--<a href="#" class="productLink"  data-id="{{$product['id']}}">--}}
                                    {{--<img src="{{ $product->thumbnailUrl }}">--}}
                                    {{--<h3 class="title">{{ $lang=='ar' ? $product['title_ar'] : $product['title'] }}</h3>--}}
                                    {{--<span class="sub">{{ $lang=='ar' ? $product['category']->name_ar : $product['category']->name }}, {{ $product['year'] }}</span>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<?php $countArt++;?>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--@elseif(count($artistOfMonth['artworks']))--}}
                    {{--@foreach($artistOfMonth['artworks'] as $product)--}}
                    {{--<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 cols">--}}
                        {{--<a href="#" class="productLink"  data-id="{{$product['id']}}">--}}
                            {{--<img src="{{ asset(''.$product['photo']) }}">--}}
                            {{--<h3 class="title">{{ $lang=='ar' ? $product['title_ar'] : $product['title'] }}</h3>--}}
                            {{--<span class="sub">{{ $lang=='ar' ? $product['category']->name_ar : $product['category']->name }}, {{ $product['year'] }}</span>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
            {{--</div>--}}
            {{--<div class="row animate" data-animation="slide-in-bottom">--}}
                {{--<div class="col-md-12 text-center">--}}
                    {{--<a class="blue-cta cta thick" href="{{ $lang=='ar' ? url('ar/artists/'.$artistOfMonth['slug']) : url('artists/'.$artistOfMonth['slug']) }}">{{ $lang=='ar' ? 'مشاهدة ملف الفنان ' : 'View Artist Profile' }}</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    {{--@foreach($homeProducts as $homeCat)--}}
        {{--@if($homeCat['products'])--}}
            {{--<section id="" class="slider-category pt-5 pb-5">--}}
                {{--<div class="container animate" data-animation="slide-in-left">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<span class="subheading">{{ $lang=='ar' ? 'الفئة' : 'Category' }}</span>--}}
                            {{--<h3 class="header-line-bot">{{ $homeCat['name'] }}<span></span></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="container animate" data-animation="slide-in-right">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<!-- Slider main container -->--}}
                        {{--<div dir="ltr" style="direction: ltr">--}}
                            {{--<div class="swiper-container">--}}
                                {{--<!-- Slides -->--}}
                                {{--@foreach($homeCat['products'] as $product)--}}
                                {{--<div class="swiper-slide animista fade-in-bottom">--}}
                                    {{--<a href="#" class="productLink"  data-id="{{$product['id']}}">--}}
                                    {{--<img src="{{ $product['photo'] }}">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<span class="left-line"></span>--}}
                                            {{--<div class="details">--}}
                                                {{--<h5 class="title">{{ $product['title'] }}</h5>--}}
                                                {{--<p class="sub">{{ $lang=='en' ? $product['category']->name : $product['category']->name_ar }}, {{ $product['year'] }}</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</section>--}}
        {{--@endif--}}
    {{--@endforeach--}}

    <section class="pb-5 animate" data-animation="slide-in-left" id="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-3">
                    <video width="100%" controls>
                      <source src="{{  asset('videos/intro.mp4') }}" type="video/mp4">
                    Your browser does not support the video tag.
                    </video>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>


    <section class="pb-5 animate" data-animation="slide-in-bottom" id="sponsors">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gallery">
                        <div class=""><img src="{{ asset('img/festival/fest1.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest2.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest3.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest4.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest5.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest6.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest7.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest8.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest9.jpg') }}" width="99%"></div>
                        <div class=""><img src="{{ asset('img/festival/fest10.jpg') }}" width="99%"></div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

    @if(count($sponsors))
        <section class="pb-5 animate" data-animation="slide-in-bottom" id="sponsors">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="header-line-bot mt-4">{{ $lang=='ar' ? 'شركاؤنا الرسميون' : 'Our Partners' }}<span></span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="sponsors">
                            @foreach($sponsors as $sponsor)
                                <div class="">
                                    <img src="{{ $sponsor['photo'] }}" width="100%">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
    @endif

    <section class="pb-5 animate" data-animation="slide-in-bottom" id="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="header-line-bot mt-4">{{ $lang=='ar' ? 'انضم إلى مجتمعنا' : 'Join our community' }}<span></span></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form id="subscribeForm" action="{{ url('/subscribe') }}" method="post">
                        @csrf
                        <input name="lang" type="hidden" value="{{$lang}}">
                        <input type="email" name="email" placeholder="{{ $lang=='ar' ? 'الاسم' : 'Email' }}">
                        <div class="alert alert-success" id="subscribeSuccess"></div>
                        <div class="alert alert-success" id="subscribeError"></div>
                        <input type="submit" value="{{ $lang=='ar' ? 'الإشتراك' : 'Subscribe' }}">
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>
<!--

    <section id="" class="news-block pt-5 pb-5">
        <div class="container  animate" data-animation="slide-in-bottom">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="header-line-bot">{{ $lang=='ar' ? 'أخبار المجتمع' : 'Community News' }} <span></span></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="block">
                        <span class="type">{{ $lang=='ar' ? 'أخبار' : 'News' }}</span>
                        <img src="{{ asset('img/news/news-ph-square.jpg') }}">
                        <h4 class="title">{{ $lang=='ar' ? 'B&O شغل A9' : 'B&0 Play A9' }}</h4>
                        <a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="block full">
                        <span class="type">{{ $lang=='ar' ? 'المعارض' : 'Exhibitions' }}</span>
                        <img src="{{ asset('img/news/news-ph-full.jpg') }}">
                        <div class="details">
                            <h4 class="title">{{ $lang=='ar' ? 'ألف-المناظر الطبيعية
المجردة by Stanislav' : 'Abstract landscapes by Stanislav' }}</h4>
                            <p class="det">{{ $lang=='ar' ? 'الفنان البلغاري الحائز على جائزة ستانيسلاف بوجانكوف ، الذي يعرض عمله على نطاق واسع ، كان يرسم طالما يتذكر.' : 'The award-winning Bulgarian artist Stanislav Bojankov , whose work is widely exhibited, has been painting as long as he can remember. ' }}</p>
                        </div>
                        <a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="block">
                        <span class="type">{{ $lang=='ar' ? 'أخبار' : 'News' }}</span>
                        <img src="{{ asset('img/news/news-ph-square-2.jpg') }}">
                        <h4 class="title">{{ $lang=='ar' ? 'الأفضل
الجولات الافتراضية' : 'The best virtual tours' }}</h4>
                        <a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="block rect">
                        <img src="{{ asset('img/news/news-ph-rect.jpg') }}">
                        <h4 class="title">{!!  $lang=='ar' ? 'المقابلة
كورنيل فان لون' : 'Interview <br/>Cornell van Loon'  !!}</h4>
                        <a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block rect black">
                        <img src="{{ asset('img/news/news-ph-rect-2.jpg') }}">
                        <h4 class="title">{!! $lang=='ar' ? 'أبيض وأسود
فن جميل' : 'Black and white <br/>fine art'  !!}</h4>
                        <a href="{{ $lang=='ar' ? url('ar/article') : url('article') }}" class="cta">{{ $lang=='ar' ? 'اقرأ أكثر' : 'Read more' }}</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ $lang=='ar' ? url('ar/community') : url('community') }}" class="pink-cta float-right mt-3 mb-5">{{ $lang=='ar' ? 'انظر كل البريد' : 'see all post' }}</a>
                </div>
            </div>
        </div>
    </section>
-->

@endsection()

@section('js')
    <script>

        $('.sponsors').slick({
            dots: true,
            infinite: false,
            speed: 500,
            @if($lang=='ar')
                rtl: true,
            @endif
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('.gallery').slick({
            dots: true,
            infinite: false,
            speed: 500,
            @if($lang=='ar')
                rtl: true,
            @endif
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    </script>
@endsection

