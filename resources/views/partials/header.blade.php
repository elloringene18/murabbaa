<?php header("X-Frame-Options: DENY"); header("X-Content-Type-Options: nosniff"); header("Content-Security-Policy: frame-ancestors 'none'");  header("X-XSS-Protection: 1; mode=block");?>

<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @inject('contentService', 'App\Services\ContentProvider')
    <?php $header = $contentService->getPageContent('header',$lang); ?>
    <title>Al Murabbaa - Official Website</title>
    {!! $header['snippets'] !!}

    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="{{ asset('fonts/webfonts/stylesheet.css') }}">
  <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
  <link rel="stylesheet" href="{{ asset('css/slick.css') }}">

  {{--<link rel="stylesheet" href="{{ asset('css/main.css') }}">--}}
  <link rel="stylesheet" href="{{ asset('css/main.2.css?v=1.3') }}">

    @if($lang=='ar')
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600;700;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main-ar.css') }}">
        <style>
            body {
                font-family: 'Cairo' !important;
            }
        </style>
    @else
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    @endif

  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animations.css') }}">

  <meta name="theme-color" content="#fafafa">

    <style>
        .swiper-slide .box {
            width: 255px;
            height: 255px;
            background-color: #0b1684;
            color: #fff;
        }
        .border {
            border: 1px solid #c42f66 !important;
        }
    </style>
    @yield('css')


    @if($lang=='ar')
        <style>

            /* Style the arrow inside the select element: */
            .select-selected:after {
                left: 10px;
                right: auto !important;
            }
            .download-bt {
                background-position: 5% center;
            }
        </style>
    @endif

    <style>
        body {
            display : none
        }
    </style>
</head>
