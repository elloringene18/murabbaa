
        <!-- Modal -->
        <div class="modal fade artmodal" id="artModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                      <div class="container-fluid">
                          <div class="row">
                            <div class="col-md-6 image">
                              <img src="{{ asset('img/sample-art.jpg') }}" class="primary" id="PM-photo">
                            </div>
                            <div class="col-md-6">
                                <div class="details">
                                    <div class="artwork-details">
                                        <a href="#" id="PM-artistlink"><span class="artist" id="PM-artist"></span></a>
                                        <h3 class="title" id="PM-title"><span></span></h3>
                                        <p id="PM-details"></p>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-12" id="PM-attributes">
                                                <p><strong></strong>: {{ $lang=='ar' ? 'الفن المعاصر ، المفاهيم ، الحداثة ، الانطباعية ، التجريدية ، التكعيبية ، التعبيرية' : 'Contemporary, Conceptual art, Modernism, Impressionism, Abstract, Cubism, Expressionism' }} </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <a href="#" class="contact" id="contactArtist">{{ $lang=='ar' ? 'تواصل مع الفنان' : 'contact the artist' }}</a>
                                                <a href="#" class="" id="notForSale">{{ $lang=='ar' ? 'هذا الغرض ليس للبيع' : 'This item not for sale' }}</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="artwork-contact">
                                        <div class="message" id="formMessage">{{ $lang=='ar' ? 'تم إرسال رسالتك إلى الفنان
    ' : 'Your message has been sent..' }}</div>
                                        <span class="artist" id="artName"></span>
                                        <h3 class="title mb-3">{{ $lang=='ar' ? 'اتصل بالفنان
    ' : 'Contact The Artist' }}<span></span></h3>
                                        <br/>
                                        <form action="{{ url('send-interest-email') }}" method="post" id="sendArtistForm">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12 text-left">
                                                    <input type="hidden" id="PM-artist-id" name="artist_id">
                                                    <input type="text" class="form-control" name="name" placeholder="{{ $lang=='ar' ? 'اسمك' : 'Your Name' }}" required />
                                                    <input type="text" class="form-control" name="contact" placeholder="{{ $lang=='ar' ? 'رقم الاتصال' : 'Contact Number' }}" required/>
                                                    <textarea class="form-control" name="message" placeholder="{{ $lang=='ar' ? 'رسالة' : 'Message' }}"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <input type="submit" class="contact" id="sendArtistMessage" value="{{ $lang=='ar' ? 'إرسال' : 'Send' }}">
                                                </div>
                                            </div>
                                        </form>

                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-12 text-left" id="artist-whatsapp">--}}
                                                {{--@if($lang=='en')--}}
                                                    {{--Send the artist a direct message through Whatsapp: <a href="" target="_blank" id="PM-artist-contact">Click here</a>.--}}
                                                {{--@else--}}
                                                    {{--ابعث للفنان رسالة مباشرة عبر الواتساب: <a href="" target="_blank" id="PM-artist-contact">انقر هنا</a>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                          </div>
                      </div>
              </div>
            </div>
          </div>
        </div>
