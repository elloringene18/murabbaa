@inject('contentService', 'App\Services\ContentProvider')
<?php $navcats = $contentService->getCats(100,$lang); ?>

<nav id="featured-menu">
    <div class="row">
        <div class="col-lg-1 col-md-0 cols"></div>
        {{--<div class="col-lg-2 col-md-3 cols">--}}
            {{--<ul>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/gallery') : url('gallery') }}" class="header">{{ $lang=='ar' ? 'التصنيفات' : 'Categories' }}</a></li>--}}
                {{--@foreach($navcats as $nav)--}}
                    {{--<li>--}}
                        {{--<a href="{{ $lang=='ar' ? url('ar/gallery/'.$nav['slug']): url('gallery/'.$nav['slug']) }}">{{ $nav['name'] }}</a>--}}
                        {{--<a href="#">{{ $nav['name'] }}</a>--}}
                        {{--@if(count($nav['children']))--}}
                        {{--<ul>--}}
                            {{--@foreach($nav['children'] as $cnav)--}}
{{--                                <li><a href="{{ $lang=='ar' ? url('ar/gallery/'.$cnav['slug']): url('gallery/'.$cnav['slug']) }}">{{ $cnav['name'] }}</a></li>--}}
                                {{--<li><a href="#">{{ $cnav['name'] }}</a></li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                {{--@endforeach--}}
{{--                <li><a href="{{ $lang=='ar' ? url('ar/gallery/'): url('gallery/') }}">{{ $lang== 'en' ? 'View All' : 'مشاهدة الكل--}}
{{--' }}</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        {{--<div class="col-lg-2 col-md-3 cols">--}}
            {{--<ul>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/exhibition'): url('exhibition') }}" class="header">{{ $lang=='ar' ? 'المعرض' : 'Exhibition' }}</a></li>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/exhibition') : url('exhibition') }}">{{ $lang == 'ar' ? 'المعرض الرقمي' : 'Artist Exhibition' }}</a></li>--}}
{{--                <li><a href="{{ $lang=='ar' ? url('ar/artists-archive') : url('artists-archive') }}">{{ $lang == 'ar' ? 'أرشيف المعرض' : 'Exhibition Archive' }}</a></li>--}}
{{--                <li><a href="{{ $lang=='ar' ? url('ar/calendar') : url('calendar') }}">{{ $lang == 'ar' ? 'التقويم' : 'Calendar of Events' }}</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        {{--<div class="col-lg-2 col-md-3 cols">--}}
            {{--<ul>--}}
                 {{--<li><a href="{{ $lang=='ar' ? url('ar/gallery'): url('gallery') }}" class="header">{{ $lang=='ar' ? 'صالة العرض' : 'Gallery' }}</a></li>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/artists'): url('artists') }}">{{ $lang=='ar' ? 'الفنانين' : 'Artists' }}</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <div class="col-lg-2 col-md-3 cols">
            <ul>
                <li><a href="{{ $lang=='ar' ? url('ar/about'): url('about') }}" class="header">{{ $lang=='ar' ? 'نبذة عنا' : 'About Us' }}</a></li>
                <li><a href="{{ $lang=='ar' ? url('ar/about#mission-vision') : url('about#mission-vision') }}">{{ $lang == 'ar' ? 'الرؤية والرسالة' : 'Mission & Vision' }}</a></li>
                <li><a href="{{ $lang=='ar' ? url('ar/about#administrators') : url('about#administrators') }}">{{ $lang == 'ar' ? 'أعضاء الفريق' : 'Team Members' }}</a></li>
                <li><a href="{{ $lang=='ar' ? url('ar/about#partners') : url('about#partners') }}">{{ $lang == 'ar' ? 'الشركاء الاستراتيجين' : 'Partners' }}</a></li>
            </ul>
        </div>
        <div class="col-lg-2 col-md-12 cols mobile">
            <ul>
                <li><a href="{{ $lang == 'ar' ? url('ar/') : url('/') }}" class="header">{{ $lang == 'ar' ? 'الرئيسية' : 'Home' }}</a></li>
                <li><a href="{{ $lang == 'ar' ? url('ar/about') : url('about') }}" class="header">{{ $lang == 'ar' ? 'نبذة عنا' : 'About Us' }}</a></li>
                {{--<li><a href="{{ $lang == 'ar' ? url('ar/calendar') : url('calendar') }}" class="header">{{ $lang == 'ar' ? 'التقويم' : 'Calendar of Events' }}</a></li>--}}
                {{--<li><a href="{{ $lang == 'ar' ? url('ar/exhibition') : url('exhibition') }}" class="header">{{ $lang == 'ar' ? 'المعارض الحالية و القادمة' : 'Artist Exhibition' }}</a></li>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/al-murabbaa-festival') : url('al-murabbaa-festival') }}" class="header">{{ $lang == 'ar' ? 'مهرجان المربعة للفنون' : 'Al Murabba Festival' }}</a></li>--}}
                {{--<li><a href="{{ $lang=='ar' ? url('ar/open-call') : url('open-call') }}" class="header">{{ $lang == 'ar' ? 'دعوى عامة' : 'Open Call' }}</a></li>--}}
            </ul>
        </div>
        <div class="col-lg-2 col-md-3 cols"></div>
        <div class="col-lg-2 col-md-3 cols"></div>
        <div class="col-lg-2 col-md-3 cols"></div>
        <div class="col-lg-2 col-md-12 cols">
            <div class="icons">
                <img src="{{ asset('img/murabbaa-logo-white.png') }}" id="featured-nav-logo">
                <div class="socials">
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-google"></a>
                    <a href="#" class="fa fa-instagram"></a>
                </div>
            </div>
        </div>
    </div>
</nav>
