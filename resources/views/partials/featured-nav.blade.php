<section id="main-featured-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="main-featured-art" class="inner">
                    <div id="featured-nav">
                        <a href="#" class="arrow"></a>
                    </div>
                    @include('partials.featured-nav-menu')
                </div>
            </div>
        </div>
    </div>
</section>
