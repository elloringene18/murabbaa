
@inject('contentService', 'App\Services\ContentProvider')
<?php $footer = $contentService->getPageContent('footer',$lang); ?>

    <footer id="footer">
        <div class="container">
            <div class="row">
                    <div class="col-md-3 cols">
                        <img src="{{ asset('img/murabbaa-logo.png') }}">
                        <p id="copyright">{{ $footer['copyright'] }}</p>
                    </div>
                    <div class="col-md-1 cols">
                    </div>
                    <div class="col-md-2 cols">
                        <ul>
                            <li><a href="{{ $lang=='ar' ? url('ar/about') : url('about') }}" class="header">{{ $lang == 'ar' ? 'عن المربعة' : 'About' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/about#mission-vision') : url('about#mission-vision') }}">{{ $lang == 'ar' ? 'الرؤية والرسالة' : 'Mission & Vision' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/about#administrators') : url('about#administrators') }}">{{ $lang == 'ar' ? 'أعضاء الفريق' : 'Team Members' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/about#partners') : url('about#partners') }}">{{ $lang == 'ar' ? 'الشركاء الاستراتيجين' : 'Partners' }}</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 cols">
                        {{--<ul>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/exhibition') : url('exhibition') }}" class="header">{{ $lang == 'ar' ? 'المعرض' : 'Exhibition' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/exhibition') : url('exhibition') }}">{{ $lang == 'ar' ? 'المعرض الرقمي' : 'Artist Exhibition' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/artists-archive') : url('artists-archive') }}">{{ $lang == 'ar' ? 'أرشيف المعرض' : 'Exhibition Archive' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/calendar') : url('calendar') }}">{{ $lang == 'ar' ? 'التقويم' : 'Calendar of Events' }}</a></li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="col-md-2 cols">
                        {{--<ul>--}}
                             {{--<li><a href="{{ $lang=='ar' ? url('ar/gallery') : url('gallery') }}" class="header">{{ $lang == 'ar' ? 'صالة العرض' : 'Gallery' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/artists') : url('artists') }}">{{ $lang == 'ar' ? 'الفنانين' : 'Artists' }}</a></li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="col-md-2 cols">
                        <ul>
                             {{--<li><a href="{{ $lang=='ar' ? url('ar/contact') : url('contact') }}" class="header">{{ $lang == 'ar' ? 'استفسارات' : 'Contact' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/contact') : url('contact') }}">{{ $lang == 'ar' ? 'اتصل بنا' : 'Inquiries/Contact Us' }}</a></li>--}}
                        </ul>

                        <div id="socialbox">
                            <strong>{{ $lang == 'ar' ? 'تابعنا على مواقع التواصل الاجتماعي
' : 'Follow Us On Social Media' }}</strong>
                            <div class="socials gray left">
                                <a href="{{ $footer['facebook'] }}" class="fa fa-facebook"></a>
                                <a href="{{ $footer['twitter'] }}" class="fa fa-twitter"></a>
                                {{--<a href="#" class="fa fa-google"></a>--}}
                                <a href="{{ $footer['instagram'] }}" class="fa fa-instagram"></a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </footer>

    <script>
        if(self == top) {
            document.getElementsByTagName("body")[0].style.display = 'block';
        }
        else{
            top.location = self.location;
        }
    </script>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/vendor/modernizr-3.11.2.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/swiper-bundle.min.js') }}"></script>
<!-- Scrollama and intersection observer polyfill -->
  <script src="{{ asset('js/intersection-observer.js') }}"></script>
  <script src="{{ asset('js/scrollama.js') }}"></script>
  <script src="{{ asset('js/plugins.js') }}"></script>
  <script src="{{ asset('js/slick.min.js') }}"></script>
  <script src="{{ asset('js/jQuery-inView.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/main.js') }}" charset="utf-8"></script>
  @yield('js')
    <script>
        var baseUrl = "{{url('/')}}";
        var siteLang = "{{$lang}}";

        function checkVisability() {
            var row = $('.animate');
            row.each(function(){
                if ($(this).inView("topOnly")) {
                    $(this).addClass($(this).attr('data-animation'));
                } else {

                }
            });

            var row = $('.animate-only');
            row.each(function(){
                if ($(this).inView("topOnly")) {
                    $(this).addClass($(this).attr('data-animation'));
                } else {

                }
            });
        }
        checkVisability();

        $(window).scroll(function() {
            checkVisability();
        });
    </script>
    <script>
        var scroller = scrollama();

        scroller
          .setup({
            step: '.animista',
            offset: 0.85
          })
          .onStepEnter(function(response) {
            response.element.classList.add('appear');
          });

        window.addEventListener('resize', scroller.resize);

        var featuredSlide = $('#featured-slide').slick({
            autoplay : true,
            dots: true,
            slidesToShow: 1,
            slidesPerRow: 1,
            slidesToScroll: 1,
            fade: true,
            arrows: false,
        });

        $('.swiper-container').slick({
          // Optional parameters
            slidesToShow: 5,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0MMJ3651LC"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-0MMJ3651LC');
    </script>


{!! $footer['snippets'] !!}
</body>

</html>
