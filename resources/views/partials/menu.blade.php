<header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ $lang == 'ar' ? url('ar/') : url('/') }}" id="mainlogo"><img src="{{ asset('img/murabbaa-logo.png') }}"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    {{--<a href="{{ $lang == 'ar' ? url('ar/') : url('/') }}" id="shortlogo"><img src="{{ asset('img/M-logo.png') }}"></a>--}}
                    <nav id="main-nav">
                        <ul>
                            <li><a href="{{ $lang == 'ar' ? url('ar/') : url('/') }}">{{ $lang == 'ar' ? 'الرئيسية' : 'Home' }}</a></li>
                            <li><a href="{{ $lang == 'ar' ? url('ar/about') : url('about') }}">{{ $lang == 'ar' ? 'نبذة عنا' : 'About Us' }}</a></li>
                            {{--<li><a href="{{ $lang == 'ar' ? url('ar/calendar') : url('calendar') }}">{{ $lang == 'ar' ? 'التقويم' : 'Calendar of Events' }}</a></li>--}}
                            {{--<li><a href="{{ $lang == 'ar' ? url('ar/exhibition') : url('exhibition') }}">{{ $lang == 'ar' ? 'المعارض الحالية و القادمة' : 'Artist Exhibition' }}</a></li>--}}
                            <li><a href="{{ $lang=='ar' ? url('ar/al-murabbaa-festival') : url('al-murabbaa-festival') }}">{{ $lang == 'ar' ? 'مهرجان المربعة للفنون' : 'Al Murabba Festival' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/media') : url('media') }}">{{ $lang == 'ar' ? 'الإعلام والصحافة' : 'Media and Press' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/artists') : url('artists') }}" class="header">{{ $lang == 'ar' ? 'صالة العرض' : 'Gallery' }}</a></li>
                            <li><a href="{{ $lang=='ar' ? url('ar/ajman-touring') : url('ajman-touring') }}" class="header">{{ $lang == 'ar' ? 'Ajman Touring' : 'Ajman Touring' }}</a></li>
{{--                            <li><a href="{{ $lang=='ar' ? url('ar/programs') : url('programs') }}">{{ $lang == 'ar' ? 'البرامج' : 'Programs' }}</a></li>--}}
                            {{--<li><a href="{{ $lang=='ar' ? url('ar/open-call') : url('open-call') }}">{{ $lang == 'ar' ? 'دعوى عامة' : 'Open Call' }}</a></li>--}}
                        </ul>
                    </nav>
                </div>
                <div class="col-md-3">
                    <form id="searchForm" action="{{ $lang == 'ar' ? url('ar/search') : url('search') }}" method="post">
                        @csrf
                        <input type="hidden" name="lang" value="{{$lang}}">
                        <input type="submit" id="searchBt" value="">
                        <input type="text" name="keyword" id="searchField" placeholder="{{ $lang == 'ar' ? 'بحث...' : 'Search...' }}">
                    </form>
                    <div id="langHolder">
                        <a href="{{$lang == 'ar' ? url('/') : url('ar')}}" id="langSwitch" class="blue-cta">{{ $lang == 'ar' ? 'EN' : 'AR' }}</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
