@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/catalog.css') }}">
    <style>
        .artist-items {
            overflow: hidden;
        }
    </style>
@endsection

@section('content')

    <body class="inner">

    @include('partials.menu')
    @include('partials.featured-nav')

    <section class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <a class="backlink" href="{{ $lang=='en'? url('calendar') : url('ar/calendar') }}">
                        <strong>{{ $lang == "en" ? '< Back to events page' : 'العودة لصفحة الأحداث' }}</strong>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 animate" data-animation="slide-in-left">
                    <h1 class="header-line-bot mt-5">{{ $data['title'] }}<span></span></h1>
                </div>
            </div>
            <div class="row news ">
                <div class="col-md-12">
                    <img src="{{ $data['photo'] }}" width="100%">
                    <br/>
                    <br/>
                    <p><strong>{{ $lang=='en' ? 'Date:' : 'التاريخ:' }}</strong> {{ $data['date'] }}</p>
                    <p><strong>{{ $lang=='en' ? 'Location:' : 'المكان:' }} </strong> {{ $data['location'] }}</p>
                    {!!  $data['content']  !!}
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script>
        var mySwiper = new Swiper('.artist-items', {
            // Optional parameters
            slidesPerView: 2,
            spaceBetween: 30,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                1: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
            }
        })
    </script>
@endsection
