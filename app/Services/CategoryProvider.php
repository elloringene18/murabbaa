<?php

namespace App\Services;

use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;

class CategoryProvider {

    public function getAll($lang){
        $data = Category::with('attributes.values','children')->whereDoesntHave('parent')->get();

        $results = [];

        foreach ($data as $id => $cat){

            if($lang=='en'){
                $results[$id]['name'] = $cat->name;
            } else {
                $results[$id]['name'] = $cat->name_ar;
            }

            $results[$id]['slug'] = $cat->slug;
            $results[$id]['attributes'] = $cat->attributes;

            $results[$id]['children'] = [];

            foreach($cat->children as $childId => $child){

                if($lang=='en')
                    $results[$id]['children'][$childId]['name'] = $child->name;
                else
                    $results[$id]['children'][$childId]['name'] = $child->name_ar;

                $results[$id]['children'][$childId]['slug'] = $child->slug;
                $results[$id]['children'][$childId]['attributes'] = $child->attributes;
            }
        }

        return $results;
    }

    public function getCategoryAttributes($slug,$lang){

        if($slug=='all')
            $cat[] = Category::with('attributes.values','children')->first();
        else
            $cat[] = Category::with('attributes.values','children')->where('slug',$slug)->first();

        $results = [];

        if($cat){

            foreach ($cat as $categ){

                if($categ->parent)
                    $loopitem = $categ->parent->attributes;
                else
                    $loopitem = $categ->attributes;

                foreach ($loopitem as $id => $cat){

                    if($lang=='en'){
                        $results[$id]['name'] = $cat->name;
                    } else {
                        $results[$id]['name'] = $cat->name_ar;
                    }

                    $results[$id]['slug'] = $cat->slug;

                    foreach ($cat->values as $valId => $value){
                        if($lang=='en')
                            $results[$id]['values'][$valId]['name'] = $value->value;
                        else
                            $results[$id]['values'][$valId]['name'] = $value->value_ar;

                        $results[$id]['values'][$valId]['id'] = $value->id;
                    }

                }
            }
        }

        return $results;
    }
}
