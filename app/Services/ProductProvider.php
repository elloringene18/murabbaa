<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Product;

class ProductProvider {

    public function getAll($lang){
        $data = Product::with('category.attributes.values','attributes')->get();

        $results = $this->langFilter($data,$lang);
        return $results;
    }

    public function getPaginate($length,$lang){
        $data = Product::with('category.attributes.values','attributes')->limit($length)->get();

        $results = $this->langFilter($data,$lang);
        return $results;
    }

    public function getByCat($cat,$lang,$length){
        $data = Product::with('category.attributes.values','attributes')->whereHas('category',function($query) use($cat) {
            return $query->where('slug',$cat);
        })->limit($length)->get();

        $results = $this->langFilter($data,$lang);
        return $results;
    }

    public function getByChildCat($cat,$lang,$length){
        $cat = Category::where('slug',$cat)->first();
        $cats = [];

        if(count($cat->children)) {
            foreach ($cat->children as $child)
                $cats[] = $child->id;
        }
        else{
            $cats[] = $cat->id;
        }

        $data = Product::with('category.attributes.values','attributes')->whereHas('category',function($query) use($cats) {
            return $query->whereIn('id',$cats);
        })->inRandomOrder()->limit($length)->get();

        $results = $this->langFilter($data,$lang);

        return $results;
    }

    public function getByAttribute($attr){
        dd(Product::with('category')->whereHas('attributes',function($query) use($attr) {
            return $query->where('attribute_value_id',$attr);
        })->get());
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){

            if($lang=='en')
                $results[$item->id]['title'] = $item->title;
            else {
                if($item->title_ar)
                    $results[$item->id]['title'] = $item->title_ar;
                else
                    $results[$item->id]['title'] = $item->title;
            }

            $results[$item->id]['id'] = $item->id;
            $results[$item->id]['slug'] = $item->slug;
            $results[$item->id]['description'] = $item->description;
            $results[$item->id]['photo'] = asset($item->photo);
            $results[$item->id]['price'] = $item->price;
            $results[$item->id]['year'] = $item->year;
            $results[$item->id]['category'] = $item->category;
            $results[$item->id]['for_sale'] = $item->for_sale;
        }

        return $results;
    }
}
