<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Product extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $url
        );
    }

    protected $fillable = ['name','name_ar','slug','title','title_ar','year','photo_full','description','description_ar','photo','price','for_sale','category_id'];

    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function getCategoryAttribute(){
        return $this->category()->first();
    }

    public function attributes(){
        return $this->belongsToMany('App\Models\AttributeValue','product_attributes','product_id','attribute_value_id');
    }

    public function getAttributesAttribute(){
        return $this->attributes()->get();
    }

    public function artist(){
        return $this->belongsToMany('App\Models\Artist','product_artists','product_id','artist_id');
    }

    public function getArtistAttribute(){
        return $this->artist()->first();
    }

    public function getThumbnailUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return 'https://via.placeholder.com/260';
    }

    public function getPhotoUrlAttribute(){
        if($this->photo_full)
            return asset($this->photo_full);
        elseif($this->photo)
            return asset($this->photo);

        return 'https://via.placeholder.com/260';
    }

}
