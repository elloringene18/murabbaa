<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use HasFactory;

    protected $fillable = ['name','name_ar','position','position_ar','photo'];


    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return asset('img/user-placeholder.jpg');
    }
}
