<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class Event extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $url
        );
    }

    protected $fillable = ['title','title_ar','slug','location','date','thumbnail','photo','content','content_ar','event_type_id'];
    public $timestamps = ['date'];

    public function type(){
        return $this->hasOne('App\Models\EventType','id','event_type_id');
    }

    public function getTypeAttribute(){
        return $this->type()->first();
    }

    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return 'https://via.placeholder.com/1100x420';
    }

    public function getThumbnailUrlAttribute(){
        if($this->thumbnail)
            return asset($this->thumbnail);

        return 'https://via.placeholder.com/160x205';
    }
}
