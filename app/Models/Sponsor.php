<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use HasFactory;
    protected $fillable = ['name','name_ar','photo','category'];

    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return asset('img/partner-placeholder.png');
    }
}
