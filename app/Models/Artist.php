<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class Artist extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->name,
            $url
        );
    }

    protected $fillable = ['name','name_ar','slug','bio','bio_ar','email','telephone','mobile','photo','banner'];

    public function products(){
        return $this->belongsToMany('App\Models\Product','product_artists','artist_id','product_id');
    }

    public function getProductsAttribute(){
        return $this->products()->orderBy('id','DESC')->get();
    }

    public function collections(){
        return $this->hasMany('App\Models\ArtistCollection');
    }

    public function getCollectionsAttribute(){
        return $this->collections()->get();
    }

    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        if(count($this->products))
            return asset($this->products[0]->photo);

        return asset('img/user-placeholder.jpg');
    }

    public function getBannerUrlAttribute(){
        if($this->banner)
            return asset($this->banner);

        return asset('img/inner.JPG');
    }

}
