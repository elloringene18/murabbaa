<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class Media extends Model implements Searchable
{
    use HasFactory;

    public function getSearchResult(): SearchResult
    {
        $url = url('search');

        return new \Spatie\Searchable\SearchResult(
            $this,
            $this->title,
            $this->title_ar,
            $this->content,
            $this->content_ar,
            $url
        );
    }

    protected $fillable = ['title','title_ar','slug','location','date','thumbnail','photo','content','content_ar','media_type_id','no_english','no_arabic'];
    public $timestamps = ['date'];
    protected $dates = ['date'];

    public function type(){
        return $this->hasOne('App\Models\MediaType','id','media_type_id');
    }

    public function slides(){
        return $this->hasMany('App\Models\MediaSlide');
    }

    public function getTypeAttribute(){
        return $this->type()->first();
    }

    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset($this->photo);

        return 'https://via.placeholder.com/1100x420';
    }

    public function getThumbnailUrlAttribute(){
        if($this->thumbnail)
            return asset($this->thumbnail);

        return 'https://via.placeholder.com/160x205';
    }

    /**
     * An article has uploads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

}
