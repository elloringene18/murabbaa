<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryParent extends Model
{
    use HasFactory;

    public function parent(){
        return $this->hasOne('App\Models\Category','id','parent_category_id');
    }

    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }
}
