<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'path',
        'original_name',
        'file_name',
        'mime_type',
    ];

    public function getUrlAttribute(){
        return asset($this->path.'/'.$this->file_name);
    }
}
