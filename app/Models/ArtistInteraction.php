<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtistInteraction extends Model
{
    use HasFactory;

    protected $fillable = ['name','contact','message','artist_id'];

    public function artist(){
        return $this->hasOne('App\Models\Artist','id','artist_id');
    }
}
