<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaSlide extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */

    protected $fillable = ['source','caption','caption_ar'];

    use HasFactory;
}
