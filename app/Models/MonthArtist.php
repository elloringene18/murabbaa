<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthArtist extends Model
{
    use HasFactory;

    public $timestamps = ['date_from','date_to'];
    protected $fillable = ['artist_id','date_from','date_to','product_id'];

    public function artist(){
        return $this->hasOne('App\Models\Artist','id','artist_id');
    }

    public function product(){
        return $this->hasOne('App\Models\Product','id','product_id');
    }

    public function products(){
            return $this->belongsToMany('App\Models\Product','artist_galleries','month_artist_id','product_id');
    }
}
