<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FestivalItem extends Model
{
    use HasFactory;

    protected $fillable = ['title','title_ar','thumbnail','content','content_ar'];
}
