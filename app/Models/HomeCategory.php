<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeCategory extends Model
{
    use HasFactory;

    protected $fillable = ['category_id'];

    public function category(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function getCategoryAttribute(){
        return $this->category()->first();
    }
}
