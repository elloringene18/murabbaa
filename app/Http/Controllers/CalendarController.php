<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Event;
use App\Models\EventType;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{
    public function index($cat = null){
        $lang = 'en';
        return $this->indexControl($lang,$cat);
    }

    public function indexAr($cat = null){
        $lang = 'ar';
        return $this->indexControl($lang,$cat);
    }

    public function indexControl($lang,$cat){
        if($cat)
            $results = Event::whereHas('type', function ($query) use($cat) {
                return $query->where('slug',$cat);
            })->orderBy('id','DESC')->paginate(6);
        else {
            $results = Event::with('type')->paginate(6);
            $cat = 'all';
        }

        $data = $this->langFilter($results,$lang);
        $types = [];

        $typesDb = EventType::get();

        foreach($typesDb as $id=>$type){

            if($lang=='en')
                $types[$id]['title'] = $type->title;
            else
                $types[$id]['title'] = $type->title_ar;

            $types[$id]['slug'] = $type->slug;
        }

        $cat = EventType::where('slug',$cat)->first();

        return view('calendar',compact('data','results','lang','cat','types'));
    }

    public function show($slug){
        $lang = 'en';
        return $this->showControl($lang,$slug);
    }

    public function showAr($slug){
        $lang = 'ar';
        return $this->showControl($lang,$slug);
    }

    private function showControl($lang,$slug){
        $results = Event::with('type')->where('slug',$slug)->first();

        $data = $this->filter($results,$lang);

        return view('event',compact('data','lang'));
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){
            $results[$item->id] = $this->filter($item,$lang);
        }

        return $results;
    }

    private function filter($item,$lang){

        if($lang=='en'){
            $results['title'] = $item->title;
            $results['type'] = $item->type->title;
            $results['content'] = $item->content;
        }
        else{
            $results['title'] = $item->title_ar;
            $results['type'] = $item->type->title_ar;
            $results['content'] = $item->content_ar;
        }

        $results['id'] = $item->id;
        $results['slug'] = $item->slug;
        $results['photo'] = $item->photoUrl;
        $results['thumbnail'] = $item->thumbnailUrl;
        $results['location'] = $item->location;
        $results['date'] = Carbon::parse($item->year)->format('M d, Y');

        return $results;
    }
}
