<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getSingle($lang,$id){
        $data[] = Product::with('category.attributes.values','artist')->find($id);

        $results = null;

        if(!$data)
            return null;

        return $this->langFilter($data,$lang);
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){

            if($lang=='en'){
                $results['title'] = $item->title;
                $results['description'] = $item->description;
            }
            else {
                if($item->title_ar)
                    $results['title'] = $item->title_ar;
                else
                    $results['title'] = $item->title;

                if($item->description_ar)
                    $results['description'] = $item->description_ar;
                else
                    $results['description'] = $item->description;
            }

            $results['slug'] = $item->slug;
            $results['for_sale'] = $item->for_sale;
            $results['photo'] = $item->photoUrl;
            $results['price'] = $item->price;
            $results['category'] = $item->category;
            $results['artist'] = $item->artist;
            $results['artist_name'] = $lang == 'en' ? $item->artist->name : $item->artist->name_ar;

            $results['attributes'] = [];

            foreach ($item->attributes as $id=>$attribute){
                $results['attributes'][$attribute->attribute->name]['value'][] = $lang=='en' ? $attribute->value : $attribute->value_ar;
            }
        }

        return response()->json($results);
    }
}
