<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function __construct()
    {
        $this->pagination = 12;
    }

    public function index($cat = null){
        $lang = 'en';
        return $this->indexControl($lang,$cat);
    }

    public function indexAr($cat = null){
        $lang = 'ar';
        return $this->indexControl($lang,$cat);
    }

    public function indexControl($lang,$cat){
        $activeAttrs = [];
        $cat = $cat ? $cat : Category::first()->slug;

        $category = $cat ? Category::where('slug',$cat)->first() : Category::first();

        if($category->children()->count()){
            $cats[] = $cat;

            foreach ($category->children as $child)
                $cats[] = $child->slug;

            $data = Product::with('category.attributes.values','attributes')->whereHas('category',function($query) use($cats) {
                return $query->whereIn('slug',$cats);
            })->orderBy('id','DESC')->paginate($this->pagination);
        }
        else {
            $data = Product::with('category.attributes.values','attributes')->whereHas('category',function($query) use($cat) {
                return $query->where('slug',$cat);
            })->orderBy('id','DESC')->paginate($this->pagination);
        }

        $products = $this->langFilter($data,$lang);

        $activeCats[] = $cat;

        if($category->parent)
            $activeCats[] = $category->parent->slug;

        return view('inner',compact('products','data','lang','cat','activeAttrs','activeCats'));

    }

    public function artist($artist){
        $lang = 'en';
        return view('artist-exhibition',compact('artist','lang'));

    }

    public function filter(Request $request){
        $activeAttrs = [];
        $input = $request->input('values');
        $lang = $request->input('lang');
        $cat = $request->input('cat');
        $page = $request->input('page')-1;
        $activeCats[] = $cat;

        $category = Category::where('slug',$cat)->first();

        if($category->parent)
            $activeCats[] = $category->parent->slug;

        if($input){

            $data = Product::whereHas('attributes', function ($query) use($input) {
                return $query->whereIn('attribute_value_id',$input);
            })->whereHas('category', function ($query) use($category) {
                return $query->where('id',$category->id);
            })->orderBy('id','DESC')->paginate($this->pagination);

            $take = Product::whereHas('attributes', function ($query) use($input) {
                return $query->whereIn('attribute_value_id',$input);
            })->whereHas('category', function ($query) use($category) {
                return $query->where('id',$category->id);
            })->orderBy('id','DESC')->skip($this->pagination*$page)->limit($this->pagination)->get();

        } else {

            if($category->children()->count()){

                $cats[] = $cat;

                foreach ($category->children as $child)
                    $cats[] = $child->slug;

                $data = Product::whereHas('category',function($query) use($cats) {
                    return $query->whereIn('slug',$cats);
                })->orderBy('id','DESC')->paginate($this->pagination);

                $take = Product::whereHas('category',function($query) use($cats) {
                    return $query->whereIn('slug',$cats);

                })->skip($this->pagination*$page)->limit($this->pagination)->get();

            } else {
                $data = Product::whereHas('category',function($query) use($cat) {
                    return $query->where('slug',$cat);
                })->orderBy('id','DESC')->paginate($this->pagination);

                $take = Product::whereHas('category',function($query) use($cat) {
                    return $query->where('slug',$cat);

                })->skip($this->pagination*$page)->limit($this->pagination)->get();
            }

        }

        $activeAttrs = $request->input('values') ? $request->input('values') : [];

        $products = $this->langFilter($take,$lang);

        return view('inner',compact('products','data','lang','cat','activeAttrs','activeCats'));
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){

            if($lang=='en')
                $results[$item->id]['title'] = $item->title;
            else {
                if($item->title_ar)
                    $results[$item->id]['title'] = $item->title_ar;
                else
                    $results[$item->id]['title'] = $item->title;
            }

            $results[$item->id]['id'] = $item->id;
            $results[$item->id]['slug'] = $item->slug;
            $results[$item->id]['description'] = $item->description;
            $results[$item->id]['photo'] = $item->thumbnailUrl;
            $results[$item->id]['price'] = $item->price;
            $results[$item->id]['year'] = $item->year;
            $results[$item->id]['category'] = $item->category;
            $results[$item->id]['attributes'] = $item->attributes;
            $results[$item->id]['for_sale'] = $item->for_sale;
        }

        return $results;
    }
}
