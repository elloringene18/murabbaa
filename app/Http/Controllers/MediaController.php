<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Media;
use App\Models\MediaType;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\Artist;

class MediaController extends Controller
{
    public function index($cat = null){
        $lang = 'en';
        return $this->indexControl($lang,$cat);
    }

    public function indexAr($cat = null){
        $lang = 'ar';
        return $this->indexControl($lang,$cat);
    }

    public function indexControl($lang,$cat){
        if($cat)
            $results = Media::whereHas('type', function ($query) use($cat) {
                return $query->where('slug',$cat);
            })->orderBy('date','desc')->get();
        else {
            $results = Media::with('type')->orderBy('date','desc')->get();
            $cat = 'all';
        }

        $data = $this->langFilter($results,$lang);
        $types = [];

        $typesDb = MediaType::get();

        foreach($typesDb as $id=>$type){

            if($lang=='en')
                $types[$id]['title'] = $type->title;
            else
                $types[$id]['title'] = $type->title_ar;

            $types[$id]['slug'] = $type->slug;
        }

        $cat = MediaType::where('slug',$cat)->first();

        return view('media',compact('data','results','lang','cat','types'));
    }

    public function show($slug){
        $lang = 'en';
        return $this->showControl($lang,$slug);
    }

    public function showAr($slug){
        $lang = 'ar';
        return $this->showControl($lang,$slug);
    }

    private function showControl($lang,$slug){
        $results = Media::with('type','slides')->where('slug',$slug)->first();

        $data = $this->filter($results,$lang);
        return view('media-single   ',compact('data','lang'));
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){
            if($lang=='en' && !$item->no_english)
                $results[count($results)] = $this->filter($item,$lang);
            elseif($lang=='ar' && !$item->no_arabic)    
                $results[count($results)] = $this->filter($item,$lang);   
        }

        return $results;
    }

    private function filter($item,$lang){

        if($lang=='en'){
            $results['title'] = $item->title;
            $results['type'] = $item->type->title;
            $results['content'] = $item->content;
        }
        else{
            $results['title'] = $item->title_ar;
            $results['type'] = $item->type->title_ar;
            $results['content'] = $item->content_ar;
        }

        $results['id'] = $item->id;
        $results['slug'] = $item->slug;
        $results['photo'] = $item->photoUrl;
        $results['thumbnail'] = $item->thumbnailUrl;
        $results['location'] = $item->location;
        
        $results['date'] = Carbon::parse($item->date)->format('M d, Y');

        if($item->slides){
            foreach($item->slides as $slide){
                $results['slides'][$slide->id]['source'] = $slide->source;
                $results['slides'][$slide->id]['caption'] = $lang == 'en' ? $slide->caption : $slide->caption_ar;
            }
        }

        return $results;
    }
}
