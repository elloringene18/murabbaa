<?php
namespace App\Http\Controllers\API;

header('Access-Control-Allow-Origin: 192.168.211.229');
header("X-Frame-Options: SAMEORIGIN");
header("Content-Security-Policy: frame-ancestors 'self'");
header("X-XSS-Protection: 1; mode=block");

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryAPI extends Controller
{
    public function getAttributes($id){

        $category = Category::find($id);

        if($category){
            if($category->parent)
                return Attribute::with('values')->where('category_id',$category->parent->id)->get();

            return Attribute::with('values')->where('category_id',$id)->get();
        }

        return [];
    }

}
