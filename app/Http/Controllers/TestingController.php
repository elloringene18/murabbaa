<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class TestingController extends Controller
{
    public function getAll(){
        dd(Product::with('category.attributes.values','attributes')->get());
    }

    public function getByCat($cat){
        dd(Product::with('attributes')->whereHas('category',function($query) use($cat) {
            return $query->where('slug',$cat);
        })->get());
    }

    public function getByAttribute($attr){
        dd(Product::with('category')->whereHas('attributes',function($query) use($attr) {
            return $query->where('attribute_value_id',$attr);
        })->get());
    }

    public function getCategories(){
        dd(Category::with('children','parent')->get());
    }
}
