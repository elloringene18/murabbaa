<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Category;
use App\Models\MonthArtist;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Models\ProductArtist;

class ArtistController extends Controller
{
    use CanCreateSlug;

    public function __construct(Artist $model)
    {
        $this->model = $model;
    }

    public function index(){
        $lang = 'en';

        $data = Artist::orderBy('id','DESC')->paginate(12);

        $artists = $this->langFilter($data,$lang);

        return view('artists',compact('artists','lang','data'));
    }

    public function indexAr(){
        $lang = 'ar';

        $data = Artist::paginate(12);

        $artists = $this->langFilter($data,$lang);

        return view('artists',compact('artists','lang','data'));
    }

    public function archive(){
        $lang = 'en';
        return $this->archiveControl($lang);
    }

    public function archiveAr(){
        $lang = 'ar';
        return $this->archiveControl($lang);
    }

    public function archiveControl($lang){
        $data = MonthArtist::orderBy('id','DESC')->paginate(12);
        $artists = [];

        foreach ($data as $id=>$item){
            $artists[$id] = $this->filter($item->artist,$lang);
            $artists[$id]['date'] = Carbon::parse($item->date_from)->format('M, Y');
        }

        return view('archive',compact('artists','lang','data'));
    }

    public function show($slug){
        $lang = 'en';
        $data = Artist::where('slug',$slug)->first();

        $artist = $this->filter($data,$lang);
        return view('artist',compact('lang','artist'));
    }

    public function showAr($slug){
        $lang = 'ar';
        $data = Artist::where('slug',$slug)->first();

        $artist = $this->filter($data,$lang);
        return view('artist',compact('lang','artist'));
    }

    private function langFilter($data,$lang){
        $results = [];

        foreach ($data as $item){
            $results[$item->id] = $this->filter($item,$lang);
        }

        return $results;
    }

    private function filter($item,$lang){

        if($lang=='en'){
            $results['name'] = $item->name;
            $results['bio'] = $item->bio;
        }
        else {
            $results['name'] = $item->name_ar;
            $results['bio'] = $item->bio_ar;
        }

        $results['id'] = $item->id;
        $results['slug'] = $item->slug;
        $results['photo'] = $item->photoUrl;
        $results['banner'] = $item->bannerUrl;
        $results['email'] = $item->email;
        $results['telephone'] = $item->telephone;
        $results['products'] = $item->products()->orderBy('id','DESC')->get();
        $results['collections'] = $item->collections()->orderBy('id','DESC')->get();

        return $results;
    }

    public function getFiles(){

        $destinationPath = 'public/artworks/';

        foreach (Storage::disk('local')->listContents('public') as $folder) {

            if($folder['type']=='dir'){
                foreach (Storage::disk('local')->listContents($folder['path']) as $file) {
                    $image =  new \Illuminate\Http\UploadedFile( storage_path("app/".$file['path']), 'tmp.jpg', 'image/jpeg',null,true);

                    $img = Image::make($image);

                    $fullName = $image->getFilename();
                    $extension = $image->getClientOriginalExtension();
                    $onlyName = explode('.'.$extension,$fullName);

                    if(Artist::where('slug',Str::slug($folder['basename']))->count() == 0){
                        $artist = Artist::create([
                            'name' => $folder['basename'],
                            'name_ar' => $folder['basename'],
                            'slug' => Str::slug($folder['basename'])
                        ]);

                    } else {
                        $artist = Artist::where('slug',Str::slug($folder['basename']))->first();
                    }


                    File::makeDirectory($destinationPath.$file['dirname'], 0777, true, true);   

                    if ($img->height()>$img->width())
                        Image::make($image->getRealPath())->resize(null,1080, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'public/'.$folder['basename'].'/'.$fullName);
                    else
                        Image::make($image->getRealPath())->resize(1080,null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath.'public/'.$folder['basename'].'/'.$fullName);

                    if(!file_exists(public_path('/artworks/public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension))){
                        
                        Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension);
                        // Create

                        $item = [
                            'year' => '2020',
                            'title' => $onlyName[0],
                            'title_ar' => $onlyName[0],
                            'description' => '<p></p>',
                            'description_ar' => '<p></p>',
                            'photo' => $destinationPath.'public/'.$folder['basename'].'/'.$onlyName[0].'-thumb.'.$extension,
                            'photo_full' => $destinationPath.$file['path'],
                            'category_id' => 1,
                            'for_sale' => 1,
                            'slug' => \Illuminate\Support\Str::slug($onlyName[0]).'-'.rand(1,99999),
                        ];

                        $newproduct = Product::create($item);

                        if($newproduct){

                            if($newproduct->category->parent)
                                $attributes = $newproduct->category->parent->attributes;
                            else
                                $attributes = $newproduct->category->attributes;

                            foreach ($attributes as $attribute){
                                $value = $attribute->values()->inRandomOrder()->first();
                                ProductAttribute::create(['attribute_value_id'=>$value->id,'product_id'=>$newproduct->id]);
                            }

                            ProductArtist::create(['product_id'=>$newproduct->id,'artist_id'=>$artist->id]);

                        }
                    }

                }
            }
        }

    }
}
