<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PageContentController extends Controller
{
    public function __construct(PageContent $model)
    {
        $this->model = $model;
    }

    public function edit($page){
        $data = PageContent::where('page',$page)->get();

        if(!$data)
            return redirect()->back();

        $slug = $page;
        return view('admin.page-contents.index',compact('data','slug'));
    }

    public function update(Request $request){

        $input = $request->input();

        foreach($input['section'] as $id=>$item){
            $target = $this->model->find($id);

            if($target){
                if($item){
                    $target->update($item);
                }
            }
        }

        $input = $request->file();

        foreach($input['section'] as $id=>$item){
            $target = $this->model->find($id);

            if($target){
                if($item){
                    $ext = $item['content']->getClientOriginalExtension();
                    $allowed = ['JPG','jpg','jpeg','JPEG','PNG','GIF'];
                    if(in_array($ext,$allowed))
                        $target->update($item);
                }
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }
}
