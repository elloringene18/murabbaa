<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\Category;
use App\Models\MediaSlide;
use App\Models\MediaType;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\Services\Uploaders\ArticleImagesUploader;

use Carbon\Carbon;

class MediaController extends Controller
{
    use CanCreateSlug;

    public function __construct(Media $model, ArticleImagesUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function index(){
        $data = Media::orderBy('id','DESC')->paginate(100);

        return view('admin.media.index',compact('data'));
    }

    public function create(){
        $types = MediaType::get();
        return view('admin.media.create',compact('types'));
    }

    public function store(Request $request){
        $input = $request->except('_token','photo');
        $photo = $request->file('photo');
        $files = $request->file('images');


        $request->validate([
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        if($photo){
            $destinationPath = 'public/uploads/media';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1100, 420)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/media/'. $newFileName;

            $destinationPath = 'public/uploads/media/thumbnails';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(260, 305)->save($destinationPath.'/'.$newFileName);
            $input['thumbnail'] = 'uploads/media/thumbnails/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date']);
        $input['slug'] = $this->generateSlug($input['title']);

        if(isset($input['no_english'])){
            $input['no_english'] = 1;
            $input['title'] = $this->generateSlug($input['title_ar']);
            $input['slug'] = $this->generateSlug($input['title_ar']);
        }
        if(isset($input['no_arabic'])){
            $input['no_arabic'] = 1;
        }

        $post = Media::create($input);

        $captions = $request->input('captions');
        $destinationPath = 'public/uploads/media';

        if($files){
            foreach($files as $index=>$file){

                if($file['slide']){
                    $photo = $file['slide'];

                    $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();
                    Image::make($photo->getRealPath())->fit(1080, 600)->save($destinationPath.'/'.$newFileName);

                    $newphoto['source'] = 'uploads/media/'. $newFileName;
                    // $newphoto['caption'] = $captions[$index]['EN'];
                    // $newphoto['caption_ar'] = $captions[$index]['AR'];

                    $post->slides()->create($newphoto);
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);
        $types = MediaType::get();

        return view('admin.media.edit',compact('item','types'));
    }



    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($photo)
            $request->validate([
                'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
            ]);

        if($target){
            if($photo){
                $destinationPath = 'public/uploads/media';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1100, 420)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/media/'. $newFileName;

                $destinationPath = 'public/uploads/media/thumbnails';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(260, 305)->save($destinationPath.'/'.$newFileName);
                $input['thumbnail'] = 'uploads/media/thumbnails/'. $newFileName;
            }

            if($delete_photo){
                $input['thumbnail'] = null;
                $input['photo'] = null;
            }

            $input['date'] = Carbon::parse($input['date']);

            // if($target->title != $input['title'])
            //     $input['slug'] = $this->generateSlug($input['title']);

            if(isset($input['no_english']))
                $input['no_english'] = 1;
            else
                $input['no_english'] = 0;

            if(isset($input['no_arabic']))
                $input['no_arabic'] = 1;
            else 
                $input['no_arabic'] = 0;

            $target->update($input);

            $captions = $request->input('captions');
            $destinationPath = 'public/uploads/media';
            $files = $request->file('images');

            if($files){
                foreach($files as $index=>$file){

                    if($file['slide']){
                        $photo = $file['slide'];

                        $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();
                        Image::make($photo->getRealPath())->fit(1080, 600)->save($destinationPath.'/'.$newFileName);

                        $newphoto['source'] = 'uploads/media/'. $newFileName;
                        // $newphoto['caption'] = $captions[$index]['EN'];
                        // $newphoto['caption_ar'] = $captions[$index]['AR'];

                        $target->slides()->create($newphoto);
                    }
                }
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function deleteSlide($id){
        $page = MediaSlide::find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    // ----------------------------------------------------------  TYPES


    public function types(){
        $data = MediaType::orderBy('id','DESC')->get();

        return view('admin.media.types.index',compact('data'));
    }

    public function createTypes(){
        return view('admin.media.types.create');
    }

    public function storeTypes(Request $request){
        $input = $request->except('_token');

        $request->validate([
            'title' => 'required|max:255'
        ]);

        $input['slug'] = $this->generateSlug($input['title']);
        MediaType::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function editTypes($id){
        $item = MediaType::find($id);
        return view('admin.media.types.edit',compact('item'));
    }



    public function updateTypes(Request $request){

        $request->validate([
            'title' => 'required|max:255'
        ]);

        $input = $request->except('_token');
        $target = MediaType::find($request->input('id'));

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $target->update($input);

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function deleteTypes($id){
        $page = $this->model->find($id);

        if($page){
            if(MediaType::count()==1){
                Session::flash('error','There must be atleast one item.');
                return redirect()->back();
            }
            $page->delete();
        }

        return redirect()->back();
    }
}
