<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Category;
use App\Models\EventType;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;

class EventController extends Controller
{
    use CanCreateSlug;

    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Event::orderBy('id','DESC')->paginate(100);

        return view('admin.events.index',compact('data'));
    }

    public function create(){
        $types = EventType::get();
        return view('admin.events.create',compact('types'));
    }

    public function store(Request $request){
        $input = $request->except('_token','photo');
        $photo = $request->file('photo');

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        if($photo){
            $destinationPath = 'public/uploads/events';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1100, 420)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/events/'. $newFileName;

            $destinationPath = 'public/uploads/events/thumbnails';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(260, 305)->save($destinationPath.'/'.$newFileName);
            $input['thumbnail'] = 'uploads/events/thumbnails/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date']);
        $input['slug'] = $this->generateSlug($input['title']);

        Event::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);
        $types = EventType::get();
        return view('admin.events.edit',compact('item','types'));
    }



    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        if($target){
            if($photo){
                $destinationPath = 'public/uploads/events';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1100, 420)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/events/'. $newFileName;

                $destinationPath = 'public/uploads/events/thumbnails';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(260, 305)->save($destinationPath.'/'.$newFileName);
                $input['thumbnail'] = 'uploads/events/thumbnails/'. $newFileName;
            }

            if($delete_photo){
                $input['thumbnail'] = null;
                $input['photo'] = null;
            }

            $input['date'] = Carbon::parse($input['date']);

            if($target->title != $input['title'])
                $input['slug'] = $this->generateSlug($input['title']);

            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    // ----------------------------------------------------------  TYPES


    public function types(){
        $data = EventType::orderBy('id','DESC')->get();

        return view('admin.events.types.index',compact('data'));
    }

    public function createTypes(){
        return view('admin.events.types.create');
    }

    public function storeTypes(Request $request){
        $input = $request->except('_token');

        $request->validate([
            'title' => 'required|max:255'
        ]);

        $input['slug'] = $this->generateSlug($input['title']);
        EventType::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function editTypes($id){
        $item = EventType::find($id);
        return view('admin.events.types.edit',compact('item'));
    }



    public function updateTypes(Request $request){

        $request->validate([
            'title' => 'required|max:255'
        ]);

        $input = $request->except('_token');
        $target = EventType::find($request->input('id'));

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $target->update($input);

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function deleteTypes($id){
        $page = $this->model->find($id);

        if($page){
            if(EventType::count()==1){
                Session::flash('error','There must be atleast one item.');
                return redirect()->back();
            }
            $page->delete();
        }

        return redirect()->back();
    }
}
