<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PartnerController extends Controller
{

    public function __construct(Sponsor $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Sponsor::paginate(100);

        return view('admin.partners.index',compact('data'));
    }

    public function create(){
        return view('admin.partners.create');
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        $input = $request->except('_token');
        $photo = $request->file('photo');

        if($photo){
            $destinationPath = 'public/uploads/partners';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/partners/'. $newFileName;
        }

        Sponsor::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);

        if(!$item)
            return redirect()->back();

        return view('admin.partners.edit',compact('item'));
    }



    public function update(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($target){

            if($photo){
                $destinationPath = 'public/uploads/partners';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/partners/'. $newFileName;
            }

            if($delete_photo)
                $input['photo'] = null;

            $target->update($input);

            Session::flash('success','Item updated successfully.');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
