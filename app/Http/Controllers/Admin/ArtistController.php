<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\ArtistCollection;
use App\Models\Category;
use App\Models\MonthArtist;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ArtistController extends Controller
{
    use CanCreateSlug;

    public function __construct(Artist $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Artist::paginate(100);

        return view('admin.artists.index',compact('data'));
    }

    public function create(){
        return view('admin.artists.create');
    }

    public function store(Request $request){
        $input = $request->except('_token');
        $photo = $request->file('photo');
        $banner = $request->file('banner');
        $collections = $request->all('collection');

//        if(Artist::where('email',$input['email'])->count()){
//            Session::flash('error','This email is already registered.');
//            return redirect()->back();
//        }

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        if($photo){
            $destinationPath = 'public/uploads/artists';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/artists/'. $newFileName;
        }

        if($banner){
            $destinationPath = 'public/uploads/artists';
            $newFileName = Str::random(32).'.'.$banner->getClientOriginalExtension();

            Image::make($banner->getRealPath())->fit(1110, 220)->save($destinationPath.'/'.$newFileName);
            $input['banner'] = 'uploads/artists/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['name']);

        $artist = Artist::create($input);

        if(isset($collections['collection'])){
            foreach ($collections['collection'] as $collection){
                if($collection){
                    $destinationPath = 'public/uploads/artists/collections';
                    $newFileName = Str::random(32).'.'.$collection->getClientOriginalExtension();

                    if(
                        $collection->getClientOriginalExtension() == 'jpg' ||
                        $collection->getClientOriginalExtension() == 'png' ||
                        $collection->getClientOriginalExtension() == 'gif'
                    ) {
                        Image::make($collection->getRealPath())->fit(1080)->save($destinationPath.'/'.$newFileName);
                        $item['path'] = 'uploads/artists/collections/'. $newFileName;
                        $item['type'] = 'photo';
                    } elseif (
                        $collection->getClientOriginalExtension() == 'mp4' ||
                        $collection->getClientOriginalExtension() == 'mov' ||
                        $collection->getClientOriginalExtension() == 'wav'
                    ) {
                        $collection->move($destinationPath, $newFileName);
                        $item['path'] = 'uploads/artists/collections/'. $newFileName;
                        $item['type'] = 'video';
                    }

                    $artist->collections()->create($item);
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);

        if(!$item)
            return redirect()->back();

        return view('admin.artists.edit',compact('item'));
    }



    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $banner = $request->file('banner');
        $target = $this->model->find($request->input('id'));
        $collections = $request->all('collection');

        $request->validate([
            'name' => 'required|max:255',
            'photo' => 'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        ]);

        if($target){

            if($photo){
                $img = Image::make($photo);
                $img->resize(520, 520);

                $destinationPath = 'public/uploads/artists';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/artists/'. $newFileName;
            }

            if($banner){

                $destinationPath = 'public/uploads/artists';
                $newFileName = Str::random(32).'.'.$banner->getClientOriginalExtension();

                Image::make($banner->getRealPath())->fit(1110, 220)->save($destinationPath.'/'.$newFileName);
                $input['banner'] = 'uploads/artists/'. $newFileName;
            }

            if($delete_photo)
                $input['photo'] = null;

            $target->update($input);

            if(isset($collections['collection'])){
                foreach ($collections['collection'] as $collection){
                    if($collection){
                        $destinationPath = 'public/uploads/artists/collections';
                        $newFileName = Str::random(32).'.'.$collection->getClientOriginalExtension();

                        if(
                            $collection->getClientOriginalExtension() == 'jpg' ||
                            $collection->getClientOriginalExtension() == 'png' ||
                            $collection->getClientOriginalExtension() == 'gif'
                        ) {
                            Image::make($collection->getRealPath())->fit(1080)->save($destinationPath.'/'.$newFileName);
                            $item['path'] = 'uploads/artists/collections/'. $newFileName;
                            $item['type'] = 'photo';
                        } elseif (
                            $collection->getClientOriginalExtension() == 'mp4' ||
                            $collection->getClientOriginalExtension() == 'mov' ||
                            $collection->getClientOriginalExtension() == 'wav'
                        ) {
                            $collection->move($destinationPath, $newFileName);
                            $item['path'] = 'uploads/artists/collections/'. $newFileName;
                            $item['type'] = 'video';
                        }

                        $target->collections()->create($item);
                    }
                }
            }

            Session::flash('success','Item updated successfully.');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function deleteCollection($id){
        $page = ArtistCollection::find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
