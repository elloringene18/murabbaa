<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FestivalItem;
use App\Models\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ArtFestivalController extends Controller
{

    public function index(){
        $data = PageContent::where('page','al-murabba-festival')->get();
        $festivalItems = FestivalItem::get();
        $brochure = PageContent::where('page','al-murabba-festival')->where('section','pdf-file')->first();
        return view('admin.art-festival.index',compact('data','festivalItems','brochure'));
    }

    public function update(Request $request){

        $input = $request->except('_token','items','brochure');

        foreach($input['section'] as $id=>$item){
            $target = PageContent::find($id);

            if($target){
                $target->update($item);
            }
        }

        $items = $request->all('items');

        foreach ($items['items'] as $id=>$item){
            $target = FestivalItem::find($id);

            if($target){

                if(isset($item['thumbnail'])){

                    $image = $item['thumbnail'];

                    $ext = $image->getClientOriginalExtension();
                    $allowed = ['JPG','jpg','jpeg','JPEG','PNG','GIF'];

                    if(in_array($ext,$allowed)){
                        $destinationPath = 'public/uploads/art-festival';
                        $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                        Image::make($image->getRealPath())->fit(255)->crop(255,255)->save($destinationPath.'/'.$newFileName);
                        $item['thumbnail'] = 'uploads/art-festival/'.$newFileName;
                    }


                }

                $target->update($item);
            }
        }

        $newItem = $request->all('newitem')['newitem'];

        if($newItem['title']){
            if(isset($newItem['thumbnail'])){
                $image = $newItem['thumbnail'];

                $ext = $image->getClientOriginalExtension();
                $allowed = ['JPG','jpg','jpeg','JPEG','PNG','GIF'];

                if(in_array($ext,$allowed)){

                    $destinationPath = 'public/uploads/art-festival';
                    $newFileName = Str::random(32) . '.' . $image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->fit(255)->crop(255, 255)->save($destinationPath . '/' . $newFileName);
                    $newItem['thumbnail'] = 'uploads/art-festival/' . $newFileName;
                }
            }
            FestivalItem::create($newItem);
        }


        $file = $request->file('brochure');

        if($file){

            $ext = $file->getClientOriginalExtension();
            $allowed = ['PDF'];

            if(in_array($ext,$allowed)){

                $destinationPath = 'public/uploads/art-festival';
                $newFileName = Str::random(32) . '.' . $file->getClientOriginalExtension();
                $file->move($destinationPath, $newFileName);

                $brochure = PageContent::where('page', 'al-murabba-festival')->where('section', 'pdf-file')->first();

                $brochure->update([
                    'content' => 'uploads/art-festival/' . $newFileName,
                    'content_ar' => 'uploads/art-festival/' . $newFileName
                ]);
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $target = FestivalItem::find($id);

        if($target)
            $target->delete();

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }
}
