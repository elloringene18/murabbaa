<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FileController extends Controller
{
   
    public function __construct(File $model)
    {
        $this->model = $model;
    }

    public function index(){
        $posts = $this->model->get();
        return view('admin.files.index', compact('posts'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/files/');
    }

    public function create(){
        return view('admin.files.create');
    }

    public function store(Request $request){
        $file = $request->file('file');
        //Move Uploaded File
        $destinationPath = 'uploads/files';
        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();

        $upload['path'] = $destinationPath;
        $upload['original_name'] = $file->getClientOriginalName();
        $upload['file_name'] = $newFileName;
        $upload['mime_type'] = $file->getClientOriginalExtension();

        $file->move($destinationPath,$newFileName);

        $this->model->create($upload);

        Session::flash('success','Item uploaded successfully');
        return redirect('admin/files');
    }
}
