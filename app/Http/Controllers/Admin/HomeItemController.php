<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Category;
use App\Models\EventType;
use App\Models\HomeCategory;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;

class HomeItemController extends Controller
{
    use CanCreateSlug;

    public function __construct(HomeCategory $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = HomeCategory::with('category')->orderBy('id','DESC')->get();
        return view('admin.settings.home-items.index',compact('data'));
    }

    public function create(){
        $homeCats = HomeCategory::get()->pluck('category_id');

        $categories = Category::whereNotIn('id',$homeCats)->whereDoesntHave('parent')->get();
        return view('admin.settings.home-items.create',compact('categories'));
    }

    public function store(Request $request){
        $input = $request->except('_token');
        HomeCategory::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);

        if(!$item)
            return redirect()->back();

        $homeCats = HomeCategory::where('id','!=',$id)->get()->pluck('category_id');
        $categories = Category::whereNotIn('id',$homeCats)->whereDoesntHave('parent')->get();

        return view('admin.settings.home-items.edit',compact('item','categories'));
    }



    public function update(Request $request){

        $input = $request->except('_token');
        $target = $this->model->find($request->input('id'));

        if($target){
            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    // ----------------------------------------------------------  TYPES


    public function types(){
        $data = EventType::orderBy('id','DESC')->get();

        return view('admin.events.types.index',compact('data'));
    }

    public function createTypes(){
        return view('admin.events.types.create');
    }

    public function storeTypes(Request $request){
        $input = $request->except('_token');

        $input['slug'] = $this->generateSlug($input['title']);
        EventType::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function editTypes($id){
        $item = EventType::find($id);
        return view('admin.events.types.edit',compact('item'));
    }



    public function updateTypes(Request $request){

        $input = $request->except('_token');
        $target = EventType::find($request->input('id'));

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $target->update($input);

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function deleteTypes($id){
        $page = $this->model->find($id);

        if($page){
            if(EventType::count()==1){
                Session::flash('error','There must be atleast one item.');
                return redirect()->back();
            }
            $page->delete();
        }

        return redirect()->back();
    }
}
