<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Category;
use App\Models\MonthArtist;
use App\Models\Upload;
use App\Traits\CanCreateSlug;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ArtistMonthController extends Controller
{
    use CanCreateSlug;

    public function __construct(MonthArtist $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = MonthArtist::with('artist')->orderBy('date_to','DESC')->paginate(100);

        return view('admin.artist-month.index',compact('data'));
    }

    public function create(){
        $artists = Artist::get();
        return view('admin.artist-month.create',compact('artists'));
    }

    public function store(Request $request){
        $input = $request->except('_token','month','year','products');
        $products = $request->input('products');

        $date = Carbon::parse($request->input('month') .' '.$request->input('year'));
        $input['date_from'] = $date->format('Y-m-d');

        $date = Carbon::parse($request->input('month') .' '.$request->input('year'));
        $input['date_to'] = $date->endOfMonth()->format('Y-m-d');

        $check = MonthArtist::where('date_from',$input['date_from'])->first();

        if($check){
            Session::flash('error','There is already an artist assigned to this month.');
            return redirect()->back();
        }

        $month = MonthArtist::create($input);
        $month->products()->sync($products);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);

        if(!$item)
            return redirect()->back();

        $artists = Artist::get();
        $seclectedartwork = $item->product_id;
        $seclectedartworks = $item->products()->get()->pluck('id');
        $artworks = $item->artist->products;

        return view('admin.artist-month.edit',compact('item','artists','seclectedartwork','artworks','seclectedartworks'));
    }

    public function update(Request $request){
        $input = $request->except('_token','id','month','year','products');
        $products = $request->input('products');
        $target = $this->model->find($request->input('id'));

        $date = Carbon::parse($request->input('month') .' '.$request->input('year'));
        $input['date_from'] = $date->format('Y-m-d');

        $date = Carbon::parse($request->input('month') .' '.$request->input('year'));
        $input['date_to'] = $date->endOfMonth()->format('Y-m-d');

        if($input['date_from'] != $target->date_from){
            $check = MonthArtist::where('date_from',$input['date_from'])->first();

            if($check){
                Session::flash('error','There is already an artist assigned to this month.');
                return redirect()->back();
            }
        }

        $target->update($input);
        $target->products()->sync($products);

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page){
            if($this->model->count()==1){
                Session::flash('error','There must be atleast one item.');
                return redirect()->back();
            }

            $page->delete();
        }

        return redirect()->back();
    }


}
