<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Category;
use App\Models\Product;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    use CanCreateSlug;

    public function __construct(Product $model, ProductImagesUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function index(){
        $data = Product::with('category')->paginate(100);

        return view('admin.products.index',compact('data'));
    }

    public function editCategories(){
        $categories = Category::get();
        $data = Artist::with('products.category')->get();
        return view('admin.products.categories',compact('data','categories'));
    }

    public function updateCategories(Request $request){
        $data = $request->input('product');

        foreach($data as $id=>$item){
            $target = Product::find($id);

            if($target){
                $target->category_id = $item['category_id'];
                $target->save();
            }
        }

        Session::flash('success','Items successfully updated.');
        return redirect()->back();
    }

    public function create(){
        $categories = Category::get();
        $artists = Artist::get();
        return view('admin.products.create',compact('categories','artists'));
    }

    public function show(){
        $pages = $this->model->get();
        return view('admin.products.show',compact('pages'));
    }

    public function store(Request $request){

        $request->validate([
            'title' => 'required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ]);

        $image = $request->file('image');
        $input = $request->except('artist_id','_token','image','attributes');
        $attributes = $request->input('attributes');

        if($image){
            $img = Image::make($image);

            $destinationPath = 'public/uploads/products';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/products/'. $newFileName;

            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            if ($img->height()>$img->width())
                Image::make($image->getRealPath())->resize(null,1080, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$newFileName);
            else
                Image::make($image->getRealPath())->resize(1080,null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$newFileName);


            $input['photo_full'] = 'uploads/products/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['title']);
        $product = Product::create($input);

        $product->artist()->sync($request->input('artist_id'));

        $product->attributes()->sync($attributes);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){

        $categories = Category::get();
        $artists = Artist::get();
        $item = $this->model->with('category.attributes.values')->find($id);

        if(!$item)
            return redirect()->back();

        $currentAttributes = [];

        foreach($item->attributes as $attribute){
            $currentAttributes[] = $attribute->id;
        }

        $currentAttributes = json_encode($currentAttributes);

        return view('admin.products.edit',compact('categories','artists','item','currentAttributes'));
    }



    public function update(Request $request){

        $request->validate([
            'title' => 'required|max:255',
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        ]);

        $image = $request->file('image');
        $input = $request->except('artist_id','_token','image','attributes','id');
        $attributes = $request->input('attributes');

        $target = $this->model->find($request->input('id'));

        if($target){
            if($image){
                $img = Image::make($image);

                $destinationPath = 'public/uploads/products';
                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(360)->crop(360,360)->save($destinationPath.'/'.$newFileName);
                $target->photo = 'uploads/products/'. $newFileName;

                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                if ($img->height()>$img->width())
                    Image::make($image->getRealPath())->resize(null,1080, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$newFileName);
                else
                    Image::make($image->getRealPath())->resize(1080,null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$newFileName);

                $target->photo_full = 'uploads/products/'. $newFileName;
            }

            if($input['title']!=$target->title)
                $target->slug = $this->generateSlug($input['title']);

            $target->update($input);
            $target->artist()->sync($request->input('artist_id'));
            $target->attributes()->sync($attributes);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $resize;
    }

}
