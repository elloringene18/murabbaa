<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Event;
use App\Models\Media;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    public function search(Request $request){
        $lang = $request->input('lang');
        $keyword = $request->input('keyword');

        $data['products'] = (new Search())
            ->registerModel(Product::class, ['title','title','description','description'])
            ->perform($keyword);

        $data['events'] = (new Search())
            ->registerModel(Event::class, ['title','title','content','content_ar'])
            ->perform($keyword);

        $data['artists'] = (new Search())
            ->registerModel(Artist::class, ['name','name_ar'])
            ->perform($keyword);

        $data['media'] = (new Search())
            ->registerModel(Media::class, ['title','title_ar','content','content_ar'])
            ->perform($keyword);

        return view('search', compact('data','lang','keyword'));

    }
}
