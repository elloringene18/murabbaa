<?php
namespace App\Exports;

use App\Models\ContactEntry;
use App\Models\Subscriber;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OpenCallExports implements FromView
{
    use Exportable;

    public function view(): View
    {
        $results = ContactEntry::where('source','open-call')->with('items')->get();

        foreach ($results as $id=>$result){
            foreach ($result->items as $valId=>$item)
                if($item->key=='portfolio')
                    $data[$id]['items'][$item->key] = asset('public/').'/'.$item->value;
                else
                    $data[$id]['items'][$item->key] = $item->value;


            $data[$id]['items']['date'] = Carbon::parse($result->created_at)->format('d-m-Y');
            $data[$id]['id'] = $result->id;
            $data[$id]['date'] = $result->created_at;
        }

        return view('exports.contacts', [
            'data' => $data
        ]);
    }
}
