<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('admin/login', function () {
    return view('auth.login');
})->middleware('cors');

Route::get('user/profile', function () {
    return view('404');
});

Route::get('register', function () { return redirect('/'); });
Route::get('dashboard', function () { return redirect('/'); });
// Route::get('getfiles','ArtistController@getFiles');

Route::group(['prefix' => 'admin','middleware' => ['auth']], function() {
    Route::get('/', function () {
        return redirect('admin/products');
    });

    Route::group(['prefix' => 'products'], function() {
        Route::get('/','Admin\ProductController@index');
        Route::get('/create','Admin\ProductController@create');
        Route::post('/store','Admin\ProductController@store');
        Route::post('/update','Admin\ProductController@update');
        Route::get('/delete/{id}','Admin\ProductController@delete');
        Route::get('/update-categories','Admin\ProductController@editCategories');
        Route::post('/update-categories','Admin\ProductController@updateCategories');
        Route::get('/{id}','Admin\ProductController@edit');

    });

    // Route::group(['prefix' => 'files','middleware' => 'auth'], function() {
    //     Route::get('/', 'Admin\FileController@index');
    //     Route::get('/edit/{id}', 'Admin\FileController@edit');
    //     Route::post('/update', 'Admin\FileController@update');
    //     Route::get('/create/', 'Admin\FileController@create');
    //     Route::post('/store/', 'Admin\FileController@store');
    //     Route::get('/delete/{id}', 'Admin\FileController@delete');
    // });

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/','Admin\CategoryController@index');
        Route::post('/store','Admin\CategoryController@store');
        Route::get('/create','Admin\CategoryController@create');
        Route::post('/update','Admin\CategoryController@update');
        Route::get('/delete/{id}','Admin\CategoryController@delete');
        Route::get('/attribute/delete/{id}','Admin\CategoryController@deleteAttribute');
        Route::get('/value/delete/{id}','Admin\CategoryController@deleteValue');
        Route::get('/{id}','Admin\CategoryController@edit');
    });

    Route::group(['prefix' => 'artists'], function() {
        Route::get('/','Admin\ArtistController@index');
        Route::post('/store','Admin\ArtistController@store');
        Route::get('/create','Admin\ArtistController@create');
        Route::post('/update','Admin\ArtistController@update');
        Route::get('/{id}/delete','Admin\ArtistController@delete');
        Route::get('/delete-collection/{id}','Admin\ArtistController@deleteCollection');
        Route::get('/{id}','Admin\ArtistController@edit');
    });

    Route::group(['prefix' => 'artist-of-the-month'], function() {
        Route::get('/','Admin\ArtistMonthController@index');
        Route::post('/store','Admin\ArtistMonthController@store');
        Route::get('/create','Admin\ArtistMonthController@create');
        Route::post('/update','Admin\ArtistMonthController@update');
        Route::get('/delete/{id}','Admin\ArtistMonthController@delete');
        Route::get('/{id}','Admin\ArtistMonthController@edit');
    });

    Route::group(['prefix' => 'events'], function() {

        Route::group(['prefix' => 'types'], function() {
            Route::get('/','Admin\EventController@types');
            Route::post('/store','Admin\EventController@storeTypes');
            Route::get('/create','Admin\EventController@createTypes');
            Route::post('/update','Admin\EventController@updateTypes');
            Route::get('/{id}','Admin\EventController@editTypes');
            Route::get('/delete/{id}','Admin\EventController@deleteTypes');
        });


        Route::get('/','Admin\EventController@index');
        Route::post('/store','Admin\EventController@store');
        Route::get('/create','Admin\EventController@create');
        Route::post('/update','Admin\EventController@update');
        Route::get('/month','Admin\EventController@month');
        Route::get('/{id}','Admin\EventController@edit');
        Route::get('/delete/{id}','Admin\EventController@delete');
    });

    Route::group(['prefix' => 'media'], function() {

        Route::group(['prefix' => 'types'], function() {
            Route::get('/','Admin\MediaController@types');
            Route::post('/store','Admin\MediaController@storeTypes');
            Route::get('/create','Admin\MediaController@createTypes');
            Route::post('/update','Admin\MediaController@updateTypes');
            Route::get('/{id}','Admin\MediaController@editTypes');
            Route::get('/delete/{id}','Admin\MediaController@deleteTypes');
        });


        Route::get('/','Admin\MediaController@index');
        Route::post('/store','Admin\MediaController@store');
        Route::get('/create','Admin\MediaController@create');
        Route::post('/update','Admin\MediaController@update');
        Route::get('/month','Admin\MediaController@month');
        Route::get('/{id}','Admin\MediaController@edit');
        Route::get('/delete/{id}','Admin\MediaController@delete');
        Route::get('/slides/delete/{id}','Admin\MediaController@deleteSlide');
    });

    Route::group(['prefix' => 'administrators'], function() {
        Route::get('/','Admin\AdminController@index');
        Route::post('/store','Admin\AdminController@store');
        Route::get('/create','Admin\AdminController@create');
        Route::post('/update','Admin\AdminController@update');
        Route::get('/{id}','Admin\AdminController@edit');
        Route::get('/delete/{id}','Admin\AdminController@delete');
    });

    Route::group(['prefix' => 'partners'], function() {
        Route::get('/','Admin\PartnerController@index');
        Route::post('/store','Admin\PartnerController@store');
        Route::get('/create','Admin\PartnerController@create');
        Route::post('/update','Admin\PartnerController@update');
        Route::get('/{id}','Admin\PartnerController@edit');
        Route::get('/delete/{id}','Admin\PartnerController@delete');
    });

    Route::group(['prefix' => 'subscribers'], function() {
        Route::get('/','Admin\SubscriptionController@index');
        Route::get('/export','Admin\SubscriptionController@export');
        Route::get('/delete/{id}','Admin\SubscriptionController@delete');
    });

    Route::group(['prefix' => 'contacts'], function() {
        Route::get('/','Admin\ContactController@index');
        Route::get('/export','Admin\ContactController@export');
        Route::get('/view/{id}','Admin\ContactController@view');
        Route::get('/delete/{id}','Admin\ContactController@delete');
    });

    Route::group(['prefix' => 'open-call'], function() {
        Route::get('/','Admin\OpenCallController@index');
        Route::get('/export','Admin\OpenCallController@export');
        Route::get('/view/{id}','Admin\OpenCallController@view');
        Route::get('/delete/{id}','Admin\OpenCallController@delete');
    });

    Route::group(['prefix' => 'settings'], function() {

        Route::group(['prefix' => 'home-items'], function() {
            Route::get('/','Admin\HomeItemController@index');
            Route::post('/store','Admin\HomeItemController@store');
            Route::get('/create','Admin\HomeItemController@create');
            Route::post('/update','Admin\HomeItemController@update');
            Route::get('/{id}','Admin\HomeItemController@edit');
            Route::get('/delete/{id}','Admin\HomeItemController@delete');
        });

    });

    Route::group(['prefix' => 'page-contents'], function() {
        Route::post('/update','Admin\PageContentController@update');
        Route::get('/{page}','Admin\PageContentController@edit');
    });

    Route::group(['prefix' => 'al-murabba-festival'], function() {
        Route::get('/','Admin\ArtFestivalController@index');
        Route::get('/delete/{id}','Admin\ArtFestivalController@delete');
        Route::post('/update','Admin\ArtFestivalController@update');
    });

    Route::get('artist-interactions',function() {
        $data = \App\Models\ArtistInteraction::paginate(100);
        return view('admin.interactions', compact('data'));
    });

    Route::get('/{page}', function ($page) {
        return view('404');
    });
});

Route::get('/get-by-cat/{cat}','TestingController@getByCat');
Route::get('/get-by-attribute/{attribute}','TestingController@getByAttribute');

Route::get('/', function () {
    $lang = 'en';
    return view('home', compact('lang'));
});

Route::get('/programs', function () {
    $lang = 'en';
    return view('programs', compact('lang'));
});

Route::get('ar/programs', function () {
    $lang = 'ar';
    return view('programs', compact('lang'));
});

Route::get('/gallery','CatalogController@index');
Route::post('/gallery','CatalogController@filter');
Route::get('/gallery/{category}','CatalogController@index');

Route::get('/artists','ArtistController@index');
Route::get('/artists-archive','ArtistController@archive');
Route::get('/artists/{slug}','ArtistController@show');

Route::get('/ajman-touring',function (){
    $lang = 'en';
    return view('ajman-touring',compact('lang'));
});

Route::get('/calendar','CalendarController@index');
Route::post('/calendar','CalendarController@filter');
Route::get('/calendar/p/{slug}','CalendarController@show');
Route::get('/calendar/{category}','CalendarController@index');

Route::get('/media','MediaController@index');
Route::post('/media','MediaController@filter');
Route::get('/media/p/{slug}','MediaController@show');
Route::get('/media/{category}','MediaController@index');

Route::post('/subscribe','FormController@subscribe');
Route::post('/contact','FormController@contact');
Route::get('/open-call',function(){ return redirect('/'); });
Route::post('/send-artist-email','FormController@sendArtistEmail');
Route::post('/send-interest-email','FormController@sendInterestEmail');

Route::post('/search','SearchController@search');

Route::get('/artist-gallery/{artist}','CatalogController@artist');

Route::group(['prefix' => 'ar'], function() {

    Route::get('/calendar','CalendarController@indexAr');
    Route::get('/calendar/p/{slug}','CalendarController@showAr');
    Route::get('/calendar/{category}','CalendarController@indexAr');

    Route::get('/media','MediaController@indexAr');
    Route::get('/media/p/{slug}','MediaController@showAr');
    Route::get('/media/{category}','MediaController@indexAr');

    Route::get('/artists','ArtistController@indexAr');
    Route::get('/artists/{slug}','ArtistController@showAr');
    Route::get('/artists-archive','ArtistController@archiveAr');

    Route::get('/gallery','CatalogController@indexAr');
    Route::post('/gallery','CatalogController@filter');
    Route::get('/gallery/{category}','CatalogController@indexAr');

    Route::post('/subscribe','FormController@subscribe');
    Route::post('/contact','FormController@contact');

    Route::post('/search','SearchController@search');

    Route::get('/', function () {
        $lang = 'ar';
        return view('home', compact('lang'));
    });

    Route::get('{page}', function ($page) {
        $lang = 'ar';

        if(strpos($page, ".") !== false)
            return view('404');

        if(view()->exists($page))
            return view($page, compact('lang'));

        return view('404');
    });
});

Route::group(['prefix' => 'api'], function() {

    Route::group(['prefix' => 'products'], function() {
        Route::get('/get-single/{lang}/{id}','ProductController@getSingle');
        Route::get('/get-by-artist/{id}','API\ProductController@getFromArtist');
    });

//    Route::group(['prefix' => 'categories'], function() {
//        Route::get('/get-attributes/{id}','API\CategoryAPI@getAttributes')->middleware(['auth']);
//    });
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/{page}', function ($page) {
    $lang = 'en';

    if(strpos($page, ".") !== false)
        return view('404');

    if(view()->exists($page))
        return view($page, compact('lang'));

    return view('404');
});
