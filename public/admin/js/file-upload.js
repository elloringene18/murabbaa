$('#add-file').on('click',function(){
    index = $('#file-uploads .file-upload').length + 1;

    el = '\n' +
        '            <div class="file-upload card-box">\n' +
        '                <div class="row">\n' +
        '                <button class="remove-box" type="button">X</button>\n' +
        '                    <div class="col-md-12">\n' +
        '                        <div class="form-group">\n' +
        '                            <label>Image(1080x1080):</label>\n' +
        '                            <input type="file" class="form-control" name="images['+index+'][slide]" placeholder="Upload Image">\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>';

    $('#file-uploads').append(el);


    removeBoxBtEvent();
});

function removeBoxBtEvent(){
    $('.remove-box').on('click',function(){
        $(this).closest('.file-upload').remove();
    });
}

$(document).ready(function(){
    removeBoxBtEvent();
});
