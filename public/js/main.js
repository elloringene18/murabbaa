$('#featured-nav').on('click',function(e){
    e.preventDefault();
    
    $(this).toggleClass('active');
    if($('#featured-menu').hasClass('active'))
        $('#featured-menu .cols').hide();
    else 
        $('#featured-menu .cols').show();
        
    $('#featured-menu').toggleClass('active');
});

$('#contactArtist').on('click',function(e){
    e.preventDefault();
    $(this).closest('.artwork-details').hide();
    $(this).closest('.details').find('.artwork-contact').fadeIn();
});

$('#sendArtistForm').on('submit',function(e){
    e.preventDefault();
    e.stopPropagation();

    data = $(this).serialize();
    url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(response){
            $('#formMessage').html(response).show();
            $('#sendArtistForm').trigger('reset');
            $('#sendArtistMessage').hide();
        }
    });
});

setTimeout(function(){
$(window).scroll();
},300);

$('.productLink').on('click',function (e) {
    e.preventDefault();
    id = $(this).attr('data-id');
    getProductModal(id);
});

$('#subscribeForm').on('submit',function (e) {
    e.preventDefault();
    e.stopPropagation();

    data = $(this).serialize();
    url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(response){
            $('#subscribeSuccess').html(response).show();
            $('#subscribeForm').trigger('reset');
        }
    });

});

function getProductModal(id){
    $.ajax({
        type: "GET",
        url: baseUrl+'/api/products/get-single/'+siteLang+'/'+id,
        success: function(response){

            $('#PM-artist-id').val(response.artist.id);
            $('#PM-photo').attr('src',response.photo);
            $('#PM-artist').html(response.artist_name);
            $('#PM-title').html(response.title);
            $('#artName').html(response.title);
            $('#PM-details').html(response.description);
            $('#PM-artistlink').attr('href',baseUrl+'/artists/'+response.artist.slug);


            // if(response.artist.email){
            //     setTimeout(function () {
            //         $('#sendArtistForm').show();
            //     },100);
            // }
            // else {
            //     setTimeout(function () {
            //         $('#sendArtistForm').hide();
            //     },300);
            // }
            //
            // if(response.artist.mobile!=null){
            //     setTimeout(function () {
            //         $('#artist-whatsapp').show();
            //         $('#PM-artist-contact').attr('href','https://wa.me/'+response.artist.mobile);
            //     },300);
            // }
            // else {
            //     setTimeout(function () {
            //         $('#artist-whatsapp').hide();
            //     },300);
            // }

            if(response.for_sale){
                $('#contactArtist').show();
                $('#notForSale').hide();
            }
            else {
                $('#contactArtist').hide();
                $('#notForSale').show();
            }

            attribs = '';
            if(Object.keys(response.attributes).length){
                attributes = response.attributes;

                Object.keys(attributes).map(function(objectKey, index) {
                    var values = attributes[objectKey];
                    attribs += '<p><strong>'+objectKey+': </strong> ';

                    Object.keys(values).map(function(objectKeyIn, index) {
                        for(x=0;x<values[objectKeyIn].length;x++){
                            if(objectKey=="Color")
                                attribs += '<span class="modalcolor" style="background-color:'+values[objectKeyIn][x]+'"></span>';
                            else
                                attribs += values[objectKeyIn][x]+ " ";
                        }
                    });
                });
            }

            // Object.keys(attributes).forEach(function(key) {
            //     key = Object.keys(attributes[key]);
            //
            //     attribs += '<p><strong></strong>'+key;
            //
            //     console.log(Object.values(key);
            //
            //     attribs += '</p>'
            // });

            $('#PM-attributes').html(attribs);
            $('#sendArtistMessage').show();


            $('#artModal .artwork-details').show();
            $('#artModal .details .artwork-contact').hide();

            $('#artModal').modal('show');
        },
        statusCode: {
            // 401: function() {
            //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
            // }
        },
        complete : function (event,error){
            hideLoader();
        }
    });
}

setTimeout(function () {
    // $('#featured-menu').css('max-height',$('#featured-menu').height());
},1000);


var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    ll = selElmnt.length;
    /* For each element, create a new DIV that will act as the selected item: */
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 0; j < ll; j++) {
        /* For each option in the original select element,
        create a new DIV that will act as an option item: */
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /* When an item is clicked, update the original select box,
            and the selected item: */
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    yl = y.length;
                    for (k = 0; k < yl; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box: */
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

