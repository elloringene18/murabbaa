$('.dropcategories .expand').on('click',function(e){
    e.preventDefault();

    if(!$(this).hasClass('active'))
        $('.dropcategories .active').removeClass('active');

    $(this).closest('li').find('.sub-cats').toggleClass('active');
    $(this).toggleClass('active');
});

$('.taglist .cat').on('click',function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $(this).parent().find('ul').toggleClass('active');
});

$('.taglist .tag').on('click',function(){
    $(this).toggleClass('active');
});

$('.taglist .color').on('click',function(e){
    $(this).toggleClass('active');
});
