// $(window).on('load',function(){
//     setTimeout(function(){
//         plotHotspots();
//     },300);
// });
// //
// $(window).on('resize',function(){
//     plotHotspots();
// });

// var hotspots = [];
//
// function plotHotspots(){
//     $('#panorama .hotspot').each(function(){
//         x  = $(this).attr('data-x');
//         y  = $(this).attr('data-y');
//         mh = $('#panorama img').first().height();
//         mw = $('#panorama img').first().width();
//
//         tX = mw * (x/100);
//         tY = mh * (y/100);
//
//         $(this).css({
//             top : tY,
//             left : tX,
//         });
//
//         hotspots.push({
//             top : tY,
//             left : tX,
//         });
// //
// //        console.log({
// //            top : tY,
// //            left : tX,
// //        });
//     });
// }
// //

$('#mobileMenu').on('click',function(){
    if($('#mobileMenu').hasClass('active')){
        $('#nav ul').removeClass('hidden');
        setTimeout(function(){
        $('#mobileMenu').removeClass('active');
        },100);
    }
    else {
        $('#nav ul').addClass('hidden');
        $('#mobileMenu').addClass('active');
    }
});

var swiper = new Swiper('.swiper-container', {
    on: {
        slideChange: function (event) {
            $('#nav a.active').removeClass('active');
            $('#nav li a').eq(event.activeIndex).addClass('active');
        },
    },
    slidesPerView: 1,
    spaceBetween: 0,
    freeMode: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
    },
});

$('#nav li a').on('click',function(){
    $('#nav a.active').removeClass('active');
    
    $(this).addClass('active');
    
    if($(window).width()<600){
        $('#nav ul').toggleClass('hidden');
        $('#mobileMenu').toggleClass('active');
    }
    swiper.slideTo($(this).index('#nav li a'),1000);
});

$('#instructions').on('click',function () {
   $(this).fadeOut();
});
